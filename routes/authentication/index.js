/* eslint-disable no-undef */
var express = require('express');
const authController = require('../../controllers/authentication/index');
require('dotenv').config();

var router = express.Router();

/**
 * Function to check user session exists
 */
const checkSessionExist = (req, res, next) => {
  if (req.session.user) {
    switch (req.session.user.UserRoleID) {
      case 1:
        res.redirect('/eventDashbaord');
        break;
      case 2:
        res.redirect('/dashboard');
        break;
      case 3:
        res.redirect('/eventDashbaord');
        break;
    }
  } else {
    next();
  }
};

/* GET default page. */
router.get('/', checkSessionExist, (req, res) => {
  locals = { layout: 'default' };
  res.render(`authentication/login`, locals);
});

/**
 * get login page
 * GET METHOD
 */
router.get('/login', checkSessionExist, (req, res, next) => {
  let locals = {};
  locals = { layout: 'default' };
  res.render('authentication/login', locals);
});

/* Handle Login POST */
router.post('/login', (req, res, next) => {
  authController.validate(req).then((result) => {
    if (result.code == '200') {
      switch (req.session.user.UserRoleID) {
        case 1:
          res.json({ status: true, message: 'Successfully logged in!', url: '/eventDashbaord', data: result.data });
          break;
        case 2:
          res.json({ status: true, message: 'Successfully logged in!', url: '/dashboard', data: result.data });
          break;
        case 3:
          res.json({ status: true, message: 'Successfully logged in!', url: '/eventDashbaord', data: result.data });
          break;
        default:
          res.json({ status: true, message: 'Successfully logged in!', url: '/eventDashbaord', data: result.data });
      }
    } else {
      return res.send({ status: false, message: result.message });
    }
  }).catch((err) => {
    return res.send({ status: false, message: 'Internal Server Error' });
  })
});

/**
 * Check if user email exists in database
 * POST method
 */
router.post('/isEmailExists', (req, res) => {
  if (req.body.email) {
    authController.checkEmailExists(req.body.email).then((result) => {
      if (result.code === 200) {
        return res.send({ status: true, message: 'Email Already Exists!!' });
      } else if (result.code === 404) {
        return res.send({ status: false, message: 'Email not Exists!!' });
      } else {
        return res.send({ status: false, message: 'Please enter valid email.' });
      }
    }).catch((err) => {
      return res.send({ status: false, message: 'Internal Server Error' });
    });
  } else {
    return res.send({ status: false, message: 'Please enter email.' });
  }
});

/* Logout Function */
router.get('/logout', (req, res) => {
  if (req.session.user && req.session.user !== '') {
    req.session.destroy();
    res.redirect('/login');
  } else {
    res.redirect('/login');
  }
});

/**
 * @url '/changePassword'
 * This api is used to render change password page
 * @method 'GET'
 * @return load change password page
 */
router.get('/changePassword', (req, res) => {
  let token = req.query.token;
  let tokenName = 'ForgetPasswordToken';
  authController.validateForgotPasswordToken(token, tokenName).then((result) => {
    res.render('authentication/changePassword', { layout: 'default', userEmail: result[0].EmailAddress });
  }).catch((error) => {
    res.render('authentication/unauthorizedToken', { layout: 'default' })
  });
});

/**
 * @url '/changePassword'
 * This api is used to render change password page
 * @method 'POST'
 * @return reset user password
 */
router.post('/changePassword', (req, res) => {
  authController.resetPassword(req.body.email, req.body.confirmPassword).then(function (result) {
    return res.send({ status: result.code, message: result.message });
  }).catch(function (error) {
    return res.json({ status: result.status, message: result.message });
  });
});

/**
 * @url '/reset-password'
 * This api check the token to set password
 * @method 'GET'
 * @return load setPassword page
 */
router.get('/forgotPassword', function (req, res) {
  res.render('authentication/forgotPassword', { layout: 'default' });
});

/**
 * Function to render invalid token page
 */
router.get('/invalidToken', function (req, res) {
  res.render('authentication/unauthorizedToken', { layout: 'default' });
});

/**
 * Function to render unauthorized page
 */
router.get('/unauthorized', function (req, res) {
  res.render('authentication/unauthorized', { layout: 'default' });
});

/**
 * Function to activate user after registeration
 **/
router.get('/verifyUser', (req, res) => {
  let token = req.query.token;
  authController.validateRegisterToken(token).then((result) => {
    res.redirect('/login');
  }).catch((error) => {
    res.render('authentication/unauthorized', { layout: 'default' })
  });
});

/**
 * @url '/reset-password'
 * This api is to set the buyer password through the link send in mail
 * @method 'POST'
 * @return set buyer password
 */
router.post('/resetPassword', function (req, res, next) {
  authController.checkEmailExists(req.body.email).then((result) => {
    if (result.code === 200) {
      let userData = result.data;
      authController.forgotPassword(userData).then((resultSet) => {
        return res.send({ status: true, message: 'Email sent with set password link. Please check.' });
      }).catch((err) => {
        return res.send({ status: false, message: 'Internal Server Error' });
      });
    } else if (result.code === 404) {
      return res.send({ status: false, message: 'Email not Exists!!' });
    } else {
      return res.send({ status: false, message: 'Please enter valid email.' });
    }
  }).catch((error) => {
    return res.send({ status: false, message: 'Internal Server Error' });
  });
});

/**
 * creates new user
 * POST method
 */
router.post('/registerUser', function (req, res, next) {
  if (req.body.userCaptcha && req.body.userCaptcha !== req.session.captcha) {
    return res.status(500).send({ code: 500, status: false, message: "Please enter valid captcha" });
  } else {
    authController.addUserInDB(req.body).then((result) => {
      return res.status(result.code).send(result);
    }).catch((err) => {
      return res.status(500).send(err);
    });
  }
});

module.exports = router;


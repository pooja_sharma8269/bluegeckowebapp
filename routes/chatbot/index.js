var express = require('express');
var router = express.Router();
const chatbotController = require('../../controllers/chatbot/index');

/* GET chatbot locations. */
router.get('/locations', function(req, res) {
  chatbotController.getChatbotLocations().then((result) => {
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

/* GET visibility mode. */
router.get('/visibilityMode', function(req, res) {
  chatbotController.getVisibilityMode().then((result) => {
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

/* GET chatbot Id */
router.get('/getChatBotID', (req, res) => {
  chatbotController.getChatBotId( req.query.domainName ).then((result) => {
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

module.exports = router;
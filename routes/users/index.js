var express = require('express');
var userCtrl = require('../../controllers/users/index');
var router = express.Router();
require('dotenv').config();

/* GET users listing. */
router.get('/pluginDashboard', function(req, res, next) {
  if (req.session.user && req.session.user !== '' && req.session.user.UserRoleID === 2) {
    res.render('plugin/index', { title:'Blue Gecko' ,layout: 'main', user: req.session.user });
} else {
    res.redirect('/login');
}
});

/**
 * get user details from email
 * GET method
 */
router.get('/getUserFromEmail', function(req, res, next) {    
  userCtrl.getUserByEmail(req.query).then((result) => {
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

/**
 * get dialoguserId from email
 * GET method
 */
router.get('/getDialogUserIdByEmail', function(req, res, next) {
  userCtrl.getDialogUserIdByEmail(req.query.email).then((result) => {
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

module.exports = router;
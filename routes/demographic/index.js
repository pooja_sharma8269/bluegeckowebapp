/* eslint-disable no-undef */
var express = require('express');
var router = express.Router();
var utils = require('../../lib/utils');

/* GET demographic details. */
router.get('/:zipcode', (req, res) => {
  utils.getDemographicDetails(req.params.zipcode)
    .then((result) => {
      res.send({
        status: true,
        body: {
          data: result,
          message: `Records fetched successfully`
        }
      });
    })
    .catch((error) => {
      res.status(400).send({
        status: false,
        error: `${error}`
      });
    })
});

module.exports = router;

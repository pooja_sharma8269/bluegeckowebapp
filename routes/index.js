/* eslint-disable no-undef */
var express = require('express');
var svgCaptcha = require('svg-captcha');
const { spawn } = require('child_process');
// const authController = require('../../controllers/authentication/index');
const userController = require('../controllers/users/index');
const adminController = require('../controllers/admin/index');
const witnessController = require('../controllers/witness/index');
const webScrapController = require('../controllers/webScraping/index.js');

require('dotenv').config();
const mailHelper = require('../lib/mailHelper');

var router = express.Router();
const options = {
  size: 5, // size of random string
  ignoreChars: '0o1i', // filter out some characters like 0o1i
  noise: 2, // number of noise lines
  color: true, // characters will have distinct colors instead of grey, true if background option is set
  background: '#D3D3D3'
}


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Blue Gecko' });
});

/**
 * GET admin dashboard page
 */
router.get('/dashboard', (req, res, next) => {
  if (req.session.user && req.session.user !== '' && (req.session.user.UserRoleID === 1 || req.session.user.UserRoleID === 2)) {
    res.render('admin/dashboard', { layout: 'main', user: req.session.user });
  } else {
    res.redirect(`/login`);
  }
});

/**
* GET intent setting page
*/
router.get('/settings', (req, res, next) => {
  if (req.session.user && req.session.user !== '' && req.session.user.UserRoleID === 1 || req.session.user.UserRoleID === 2) {
    res.render('admin/settings', { layout: 'main', user: req.session.user });
  } else {
    res.redirect(`/login`);
  }
});

/**
* GET intent management page
*/
router.get('/intentManagement', (req, res, next) => {
  if (req.session.user && req.session.user !== '' && (req.session.user.UserRoleID === 1 || req.session.user.UserRoleID === 2 || req.session.user.UserRoleID === 3)) {
    res.render('admin/intentManagement', { layout: 'main', user: req.session.user });
  } else {
    res.redirect(`/login`);
  }
});

/**
* GET create new user page
*/
router.get('/addUser', (req, res, next) => {
  if (req.session.user && req.session.user !== '' && req.session.user.UserRoleID === 1) {
    res.render('admin/createUser', { layout: 'main', user: req.session.user });
  } else {
    res.redirect(`/login`);
  }
});

/**
* GET Captcha
*/
router.get('/captcha', function (req, res) {
  var captcha = svgCaptcha.create(options);
  req.session.captcha = captcha.text;
  res.status(200).send(captcha.data);
});


/* GET user role list. */
router.get('/getUserDropdownList', function (req, res, next) {
  userController.getUserDropdownList(req.session.user.UserID).then((result) => {
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});


/* GET users list page. */
router.get('/userLists', function (req, res, next) {
  if (req.session.user && req.session.user !== '' && req.session.user.UserRoleID === 1) {
    res.render('admin/userList', { layout: 'main', user: req.session.user });
  } else {
    res.redirect(`/login`);
  }
});

/* GET users list. */
router.get('/userListing', function (req, res, next) {
  userController.getUserList(req.session.user.EmailAddress).then((result) => {
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

/* GET user by email. */
router.get('/editUser/:email', function (req, res, next) {
  userController.getUserByEmail(req.params).then((result) => {
    result.userData[0].editUser = 1;
    return res.render('admin/createUser', {
      layout: 'main',
      user: req.session.user,
      body: result.userData[0]
    });
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

/* GET update user's state of active/deactive */
router.get('/activeDeactiveUser', function (req, res, next) {
  userController.updateUserActiveState(req.query).then((result) => {
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

/* GET Intent Analytics. */
router.get('/intentAnalytics', function (req, res, next) {
  adminController.getIntentAnalytics(req.session.user.UserID, req.query.lastDate, req.query.currentDate,req.query.type).then((result) => {
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

/* GET Intent Tx History. */
router.get('/intentTxHistory', function (req, res, next) {
  adminController.getIntentTxHistory(req.session.user.EmailAddress, req.query.IntentID, req.query.lastDate, req.query.currentDate).then((result) => { //
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

/* GET event state maintenancne page. */
router.get('/eventStateMaintenance/:intentName', function (req, res, next) {
  if (req.session.user && req.session.user !== '' && (req.session.user.UserRoleID === 3 || req.session.user.UserRoleID === 1 || req.session.user.UserRoleID === 2)) {
    res.render('witness/eventStateMaintenance', { layout: 'main', user: req.session.user, intentName: req.params.intentName });
  } else {
    res.redirect(`/login`);
  }
});

/* GET event dahsboard page. */
router.get('/eventDashbaord', function (req, res, next) {
  if (req.session.user && req.session.user !== '' && (req.session.user.UserRoleID === 3 || req.session.user.UserRoleID === 1)) {
    res.render('witness/eventDashboard', { layout: 'main', user: req.session.user });
  } else {
    res.redirect(`/login`);
  }
});

/* GET event state information. */
router.get('/eventInformation', function (req, res, next) {
  //add userID from session;
  witnessController.getEventInformation(req.session.user.UserID, req.query.IntentID).then((result) => { //req.session.user.UserID
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

/* GET Intent List For User*/
router.get('/getIntentList', function (req, res, next) {
  witnessController.getIntentList(req.query.type,req.session.user.UserID).then((result) => {
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

/* GET Intent QA List */
router.get('/getIntentQA', function (req, res, next) {
  witnessController.getIntentQA(req.query.translateLanguageVal).then((result) => {
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

/* POST update intent information*/
router.post('/updateEventInformation', function (req, res, next) {
  witnessController.updateEventInfo(req.session.user.UserID, req.body).then((result) => { //req.session.user.UserID
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});


/* GET event state dahsboard information. */
router.get('/eventDashboard', function (req, res, next) {
  witnessController.getEventDashbaordInfo(req.session.user.UserID).then((result) => { //req.session.user.UserID
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});


/* GET last known reset */
router.get('/getLastKnownReset', function (req, res, next) {
  let type = req.query.type;
  witnessController.getLastKnownReset(req.session.user.UserID, type).then((result) => { //req.session.user.UserID
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

/* GET message response */
router.get('/getMessageResponse', function (req, res, next) {
  witnessController.getMessageResponse(req.session.user.UserID, req.query).then((result) => { //req.session.user.UserID
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

// Pooja Sharma Web scraping code
// /* GET webScraper page*/
router.get('/webScraper', function (req, res, next) {
  if (req.session.user && req.session.user !== '' && req.session.user.UserRoleID === 1) {
    res.render('admin/webScraper', { layout: 'main', user: req.session.user });
  } else {
    res.redirect(`/login`);
  }
});

/**
 * Child Processed
 */
router.get('/getKey', express.json(), (req, response) => {
  const ls = spawn('gcloud', ['auth', 'application-default', 'print-access-token']);
  ls.stdout.on('data', (data) => {
    console.log(`${data}`);
    const token = data.toString().trim();
    console.log('token:- ' + token);
    response.send(token);
  });

  ls.stderr.on('data', (data) => {
    console.log(`stderr: ${data}`);
  });

  ls.on('close', (code) => {
    console.log(`child process exited with code ${code}`);
  });
});

/* POST update intent information from web scraping*/
router.post('/updateWebScrapedDataToDatabase', function (req, res) {
  webScrapController.updateURLResponse(JSON.parse(req.body.data)).then((result) => {
    req.setTimeout(0);
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

/* POST sitemap scraped data to database */
router.post('/updateSiteMapScrapedDataToDatabase', function (req, res) {
  webScrapController.updateSiteMapScrapedResponse(req.body.type).then((result) => {
    req.setTimeout(0);
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

/* POST update question answer translated data for a url */
router.post('/updateTranslatedData', function (req, res) {
  webScrapController.updateTranslatedResponse(req.body).then((result) => {
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

/* GET web scraped complete data from response */
router.get('/getCompleteWebScrapedResponse', function (req, res) {
  webScrapController.getCompleteWebScrapedResponse().then((result) => {
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

/* Translate web scraped data from database responses */
router.post('/translateCompleteWebScrapedResponse', function (req, res) {
  webScrapController.translateCompleteWebScrapedResponse(req.body).then((result) => {
    req.setTimeout(0);
    return res.send(result);
  }).catch((err) => {
    return res.status(400).send(err);
  });
});

module.exports = router;

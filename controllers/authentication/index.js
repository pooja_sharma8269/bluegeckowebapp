/* eslint-disable no-undef */
const authModel = require('../../models/authentication/index.js');
const dbHelper = require('../../lib/dbHelper');
const utils = require('../../lib/utils');
const mailHelper = require('../../lib/mailHelper');
const random = require('random-string-generator');
const moment = require('moment');
const logger = require('../../lib/loggers');

/**
 * Function used for register/update user
 * @param {*} data
 */
const getSetUserProfile = (data) => new Promise((resolve, reject) => {
  let successObj;
  dbHelper.getSetUserProfileStoredProcedure(data).then((result) => {
    if (result.returnValue == 0) {
      successObj = {
        code: 200,
        message: 'User Registered Successfully.',
        data: result
      };
      logger.info('User Registered Successfully. ===> ');
      resolve(successObj);
    } else if (result.returnValue == 1) {
      successObj = {
        code: 201,
        message: 'User Updated Successfully.',
        data: result
      };
      logger.info('User Updated Successfully. ===> ');
      resolve(successObj);
    } else if (result.returnValue == -1) {
      successObj = {
        code: 201,
        message: 'EmailID, FirstName and LastName is required.',
        data: result
      };
      logger.error('EmailID, FirstName and LastName is required. ===> Error in Registration ');
      resolve(successObj);
    } else {
      successObj = {
        code: 404,
        message: 'User Not Registered.',
        data: result
      };
      logger.error('User Not Registered. ===> Error in Registration ');
      resolve(successObj);
    }
  }).catch((err) => {
    reject({code: 500,message: `${err}`});
  });
})

/**
 * This function stores user data to the database
 * @param {*} body
 */
const register = (body) => new Promise((resolve, reject) => {
  let data;
  if (!('ChatbotLocationID' in body)) { body.ChatbotLocationID = '2';}
  if(body.PostalCode){
    utils.getDemographicDetails(body.PostalCode).then((zipCodeData) => {
      data = { ...body, ...zipCodeData[0] };
      getSetUserProfile(data).then((result) => {
        resolve(result);
      }).catch((err) => {
        reject(err);
      });
    }).catch((error) => {
      reject({code: 500, message: `${error}`});
    });
  }else{
    getSetUserProfile(body).then((result) => {
      resolve(result);
    }).catch((err) => {
      reject(err);
    });
  }
});

/**
 * function check valid email format
 * @param {*} userEmail
 */
const checkValidEmail = (userEmail) => {
  let emailFilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if (!emailFilter.test(userEmail)) {
    return false;
  } else {
    return true;
  }
}

/**
 * Function checks email exists in database.
 * @param {*} email
 */
const checkEmailExists = (email) => new Promise((resolve, reject) => {
  if (!checkValidEmail(email) == false) {
    authModel.checkEmailExists(email).then((result) => {
      resolve(result);
    }).catch((err) => {
      reject(err);
    })
  } else {
    resolve({ status: false, message: 'Please check the email.' });
  }
});

/**
 * This function helps to validate user credentials at login
 * @param {*} body
 */
const validate = (req) => new Promise((resolve, reject) => {
  authModel.login(req.body).then((result) => {
    req.session.user = result.data;
    resolve(result);
  }).catch((err) => {
    reject(err);
  });
});

/**
 * forgot password function
 * @param {*} req
 */
const forgotPassword = data =>new Promise((resolve, reject) => {
  let forgetPasswordToken = random('lower');
  let userObject = {};
  userObject.isVerified = 1;
  userObject.EmailAddress = data.EmailAddress;
	userObject.UserPassword = data.UserPassword;
	userObject.ForgetPasswordToken = forgetPasswordToken;
  data.ForgetPasswordToken = forgetPasswordToken;
  dbHelper.forgotPasswordStoredProcedure(userObject).then((result) => {
    if(result.returnValue == 1){
      mailHelper.sendForgotPasswordMail(data,1).then((result) => {
        resolve(result);
      }).catch((err) => {
        reject(err);
     });
    }
  }).catch((err) => {
    reject({code: 500,message: `${err}`});
  });
});

/**
 * validate forgot password token
 * @param {*} token
 */
const validateForgotPasswordToken = (token,tokenName) => new Promise(((resolve, reject) => {
 authModel.getUserByToken(token.trim(), tokenName).then((result) => {
    let endDate = moment.utc(result[0].PermittedAccessEndDateTime).format();
    if (result == null) {
      reject('401');
    } else if (endDate <= moment.utc().format()) {
      reject('401');
    } else {
      resolve(result);
    }
  }).catch((error) => {
    reject(error);
  });
}));




/**
 * verify user after registration
 * @param {*} token
 */
const activateUser = (token,isVerified) => new Promise((resolve,reject) => {
  dbHelper.getUserTokenStoredProcedure(token,isVerified).then((res) => {
    resolve(res);
  }).catch((err) => {
      reject(err);
  });
});

/**
 * validate user register function
 * @param {*} token
 */
const validateRegisterToken = token => new Promise((resolve,reject) => {
  let isVerified = 0;
  authModel.getUserByRegisterToken(token.trim(), isVerified).then((result) => {
    if (result[0]) {
      isVerified = 1;
      activateUser(token.trim(),isVerified).then((res) => {
        if(res){
          resolve(res);
        }else{
          let errObj = {
            code: 500,
            status: false,
            message: 'User unable to verify. Please try again.'
          }
          reject(errObj);
        }
      }).catch((err) => {
        reject(err);
      });
    }else{
      let errObj = {
        code: 500,
        status: false,
        message: 'User unable to verify. Please try again.'
      }
      reject(errObj);
    }
  }).catch((error) => {
    reject(error);
  });
});

/**
 * This function resets user password
 * @param {STRING} userid
 * @param {STRING} password
 */
const resetPassword = (email, password) => new Promise(((resolve, reject) => {
  let data = {};
  data.EmailAddress = email;
  data.UserPassword = password;
  data.isVerified = 1;
  authModel.setUserPassword(data).then((result) => {
    logger.info(`Password updated successfully for user ===> ${email}`);
    resolve(result);
  }).catch((error) => {
    logger.error(`Password update failed for user ===> ${email}`);
    reject(error);
  });
}));

/**
 * This function is used to save user details in database.
 * @param {*} body
*/
const addUserInDB = (body) => new Promise((resolve,reject) => {
  // let APIKey;
  let registerToken;
  if(body.pageValue === '2'){
    // APIKey = random('alphanumeric');
    registerToken = random('lowernumeric');
    // body.WordPressAPIKey = APIKey;
    body.ForgetPasswordToken = registerToken;
    body.isVerified = 0;
  }
  register(body).then((result) => {
    if (result.code == 200){
      let userObject = {};
      userObject.isVerified = 0;
      userObject.EmailAddress = body.EmailAddress;
      userObject.UserPassword = '';
      userObject.ForgetPasswordToken = registerToken;
      dbHelper.forgotPasswordStoredProcedure(userObject).then((result) => {
        if(result.returnValue == 1){
          mailHelper.sendForgotPasswordMail(body,2).then((result) => {
            resolve({ status: true, code: 200,  message: result.message, url: '/userLists'}); //, APIKey: APIKey
          }).catch((err) => {
            reject(err);
         });
        }
      }).catch((err) => {
        reject({code: 500,message: `${err}`});
      });
    }else if(result.code == 201){
      resolve({ status: true , code: result.code, message: result.message});
    }else{
      reject({ status: false, code: result.code,  message: result.message, result: {} });
    }
  }).catch((err) => {
      reject({ status: false, code: 400,  message: err.message });
  });
});

// eslint-disable-next-line no-undef
module.exports = {
  register,
  validate,
  validateForgotPasswordToken,
  resetPassword,
  checkEmailExists,
  addUserInDB,
  forgotPassword,
  validateRegisterToken,
  activateUser
}
/* eslint-disable no-undef */
const userModel = require('../../models/users/index.js');

/**
 * This function helps to fetch user list 
 */
const getUserList = (emailID) => new Promise ((resolve,reject) =>{
    userModel.getUserList(emailID).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

/**
 * This function helps to fetch user data by email
 * @param {user} user  
 */
const getUserByEmail = (user) => new Promise ((resolve,reject) =>{
    userModel.getUserByEmail(user).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

/**
 * This function helps to fetch user data by email
 * @param {userEmail} user email 
 */
const getDialogUserIdByEmail = (userEmail) => new Promise ((resolve,reject) =>{
    userModel.getDialogUserIdByEmail(userEmail).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

/**
 * This function helps to update user active state
 * @param {query} user query 
 */
const updateUserActiveState = (query) => new Promise ((resolve,reject) =>{
    userModel.updateUserActiveState(query).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

/**
 * This function helps to fetch user role list
 * @param {userId} user userId 
 */
const getUserDropdownList = (userId) => new Promise ((resolve,reject) =>{
    userModel.getUserDropdownList(userId).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

module.exports = {
    getUserList,
    getUserByEmail,
    getDialogUserIdByEmail,
    updateUserActiveState,
    getUserDropdownList
}
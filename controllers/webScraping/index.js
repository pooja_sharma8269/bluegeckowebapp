/* eslint-disable no-undef */
const webScrapModel = require('../../models/webScraping/index');
const translationScrapDataModel = require('../../models/webScrapingTranslation/index');
const siteMapScrapModel = require('../../models/siteMapScraping/index')

const updateURLResponse = (urlValues) => new Promise ((resolve,reject) =>{
    webScrapModel.updateURLResponse(urlValues).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

const updateSiteMapScrapedResponse = (type) => new Promise ((resolve,reject) =>{
    siteMapScrapModel.updateSiteMapScrapedResponse(type).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

const updateTranslatedResponse = (data) => new Promise ((resolve,reject) =>{
    webScrapModel.updateTranslatedResponse(data).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
})

const getCompleteWebScrapedResponse = () => new Promise ((resolve,reject) =>{
    webScrapModel.getCompleteWebScrapedResponse().then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

const translateCompleteWebScrapedResponse = (body) => new Promise ((resolve,reject) =>{
    translationScrapDataModel.translateCompleteWebScrapedResponse(JSON.parse(body.data),JSON.parse(body.languageValueFiltered)).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});
module.exports = {
    updateURLResponse,
    updateTranslatedResponse,
    getCompleteWebScrapedResponse,
    translateCompleteWebScrapedResponse,
    updateSiteMapScrapedResponse
}
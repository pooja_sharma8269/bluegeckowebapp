const witnessModel = require('../../models/witness/index.js');

/**
 * This function helps to fetch Event State Information
 * @param {userID} userID
 * @param {intentID} intentID
 */
const getEventInformation = (userID, intentId) => new Promise ((resolve,reject) =>{
    witnessModel.getEventInformation(userID, intentId).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

/**
 * This function helps to fetch intent list for the user
 */
const getIntentList = (type,UserID) => new Promise ((resolve,reject) =>{
    witnessModel.getIntentList(type,UserID).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

/**
 * This function helps to fetch intent QA
 */
const getIntentQA = (languages) => new Promise ((resolve,reject) =>{
    witnessModel.getIntentQA(languages).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

/**
 * This function helps to update event informatio
 * @param { userId } userId
 * @param { eventInfo} eventInfo
 */
const updateEventInfo = (userId,eventInfo) => new Promise ((resolve,reject) =>{
    witnessModel.updateEventInfo(userId,eventInfo).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

/**
 * This function helps to get event dashborad information
 * @param { userId } userId
 */
const getEventDashbaordInfo = (userId) => new Promise ((resolve,reject) =>{
    witnessModel.getEventDashbaordInfo(userId).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

/**
 * This function helps to get last known reset
 * @param { userId } userId
 * @param { type } type
 */
const getLastKnownReset = (userId,type) => new Promise ((resolve,reject) =>{
    witnessModel.getLastKnownReset(userId,type).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

/**
 * This function helps to get message response
 * @param { userId } userId
 * @param { query } query
 */
const getMessageResponse = (userId, query) => new Promise ((resolve,reject) =>{
    witnessModel.getMessageResponse(userId , query).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

module.exports = {
    getEventInformation,
    getIntentList,
    getIntentQA,
    updateEventInfo,
    getEventDashbaordInfo,
    getLastKnownReset,
    getMessageResponse
}
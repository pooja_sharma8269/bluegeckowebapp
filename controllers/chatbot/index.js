/* eslint-disable no-undef */
const chatbotModel = require('../../models/chatbot/index.js');

/**
 * This function helps to fetch chatbot locations 
 */
const getChatbotLocations = () => new Promise ((resolve,reject) =>{
    chatbotModel.getChatbotLocations().then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

/**
 * This function helps to fetch visibility mode 
 */
const getVisibilityMode = () => new Promise ((resolve,reject) =>{
    chatbotModel.getVisibilityMode().then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

/**
 * This function helps to fetch visibility mode 
 */
const getChatBotId = (wordpressdomain) => new Promise ((resolve,reject) =>{
    chatbotModel.getChatBotId(wordpressdomain).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

/**
 * This function helps to fetch chatbot locations and visibility mode 
 */
const getChatbotLocationAndVisibilityMode = () => new Promise ((resolve,reject) =>{
    chatbotModel.getChatbotLocationAndVisibilityMode().then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

module.exports = {
    getChatbotLocations,
    getVisibilityMode,
    getChatBotId,
    getChatbotLocationAndVisibilityMode
}
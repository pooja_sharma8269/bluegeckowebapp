/* eslint-disable no-undef */
const adminModel = require('../../models/admin/index.js');

/**
 * This function helps to Intent Analytics
 * @param {userID} userID
 */
const getIntentAnalytics = (userID,lastDate,currentDate,type) => new Promise ((resolve,reject) =>{
    adminModel.getIntentAnalytics(userID,lastDate,currentDate,type).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

/**
 * This function helps to get Intent Transaction History
 * @param {userEmail} userEmail
 * @param {intentID} intentID
 */
const getIntentTxHistory = (userEmail,intentID,lastDate,currentDate) => new Promise ((resolve,reject) =>{
    adminModel.getIntentTxHistory(userEmail,parseInt(intentID),lastDate,currentDate).then((result) => {
        resolve(result);
    }).catch((err) => {
        reject(err);
    });
});

module.exports = {
    getIntentAnalytics,
    getIntentTxHistory
}
/* eslint-disable consistent-return */
/* eslint no-underscore-dangle: ["error", { "allow": ["_switch_value_"] }] */
module.exports = {

    switch(value, options) {
      this._switch_value_ = value;
      const html = options.fn(this); // Process the body of the switch block
      delete this._switch_value_;
      return html;
    },
    case(value, options) {
      if (value == this._switch_value_) {
        return options.fn(this);
      }
    },
    ifCond(v1, operator, v2, options) {
      switch (operator) {
        case '==':
          return (v1 == v2) ? options.fn(this) : options.inverse(this);
        case '===':
          return (v1 === v2) ? options.fn(this) : options.inverse(this);
        case '!=':
          return (v1 != v2) ? options.fn(this) : options.inverse(this);
        case '!==':
          return (v1 !== v2) ? options.fn(this) : options.inverse(this);
        case '<':
          return (v1 < v2) ? options.fn(this) : options.inverse(this);
        case '<=':
          return (v1 <= v2) ? options.fn(this) : options.inverse(this);
        case '>':
          return (v1 > v2) ? options.fn(this) : options.inverse(this);
        case '>=':
          return (v1 >= v2) ? options.fn(this) : options.inverse(this);
        case '&&':
          return (v1 && v2) ? options.fn(this) : options.inverse(this);
        case '||':
          return (v1 || v2) ? options.fn(this) : options.inverse(this);
        default:
          return options.inverse(this);
      }
    },
    verification() {
      if (this.registrationStatus && this.registrationStatus.isVerified) {
        return '<div class="check-circle"><i class="check circle outline icon"></i></div><p>Thank you for creating your account. </br><strong>Your email address is confirmed.</strong></p>';
      }
      return '<div class="envelope-open"><i class="envelope open outline icon"></i></div><p>We need to confirm your email address. </br><strong>Please check your email for a confirmation message.</strong></p><span>Be sure to check your spam box if it does not arrive within </br> a few minutes.</span></br><a class="underline-on-hover" id="sendVerifyEmail">Resend e-mail</a>';
    },
  };
  
function getToasterHtml(type,body) {
  let divContent = type === 'error' ? `<div id='toast' style="background-color:#b50000;">
  <i class="fas fa-exclamation-circle"></i>
  <div id='desc'>${body}</div>
  </div>` : `<div id='toast' style="background-color:#0c8f78;">
  <i class="fas fa-check-circle"></i>
  <div id='desc'>${body}</div>
  </div>`;
  return divContent;
}
  
function showToaster(type, body, timeout) {
    if(document.getElementById("toast")){
      $('#toast').remove();
    }
    const divContent = getToasterHtml(type,body);
    const latestNotification = $(divContent).appendTo('#showToasters');
    let x = document.getElementById("toast");
    x.className = "show";
    setTimeout(function(){ 
      x.className = x.className.replace("show", "hide");
    }, timeout);
}

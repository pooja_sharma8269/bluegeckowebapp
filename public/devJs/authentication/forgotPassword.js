(function() {
    "use strict";
    window.addEventListener(
      "load",
      function() {
        var forms = document.getElementsByClassName("needs-validation");
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener(
            "submit",
            function(event) {
              if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
              }
              form.classList.add("was-validated");
            },
            false
          );
        });
      },
      false
    );
  })();

  function sendAjaxRequest () {
    showFullPageLoader();
    let email = $("#inputEmail").val();
    let data = {email};
        $.ajax({
          url: `/resetPassword`,
          type: 'POST',
          data,
          cache: false,
          headers: { 'cache-control': 'no-cache' },
          success(result) {
              hideFullPageLoader();
              if (result.status === true) {
                showToaster("success", result.message, 5000);
                $('#resetPasswordForm').hide();
                $('#forgotPasswordMessage').show();
              } else {
                showToaster("error", result.message, 5000);
              }
             
          },
          error(error) {
            hideFullPageLoader();
            showToaster("error", error.message, 5000);
            //showError(error.message, 5000);
          },
        });
  }

  $(document).ready(() => {
    applyFloatingLabels();
      $('#resetPassword').click((e) => {
        $('#resetPasswordForm').submit(false); 
       sendAjaxRequest();
    }); 
  });
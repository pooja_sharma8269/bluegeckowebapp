$(function () {
  $("form[name='loginForm']").validate({
    errorClass: 'invalid-feedback',
    //validClass: 'valid-feedback',
    // Specify validation rules
    rules: {
      email: {
        required: true,
        email: true
      },
      password: {
        required: true
      }
    },
    highlight: function (element) {
      $(element).removeClass('is-valid');
    },
    unhighlight: function (element) {
    },
    success: function (label, element) {
      $(element).addClass('is-valid');
    },    // Specify validation error messages
    messages: {
      password: {
        required: "Please provide a password"
      },
      email: "Please enter a valid email address"
    },
    submitHandler: function (form) {
      addCookies();
      sendAuthenticationRequest();
    }
  });
  applyFloatingLabels();
});

const addCookies = () => {
  let expires_day = 1;
  let pm = [];
  if ($('#remember-me').is(':checked')) {
    $.cookie('pm[email]', $('#inputUsername').val(), { expires: expires_day });
    $.cookie('pm[password]', $('#inputPassword').val(), { expires: expires_day });
    $.cookie('pm[remember]', true, { expires: expires_day });
  }
  else {
    // reset cookies.
    $.cookie('pm[email]', '');
    $.cookie('pm[password]', '');
    $.cookie('pm[remember]', false);
  }
  return true;
};

function sendAuthenticationRequest() {
    showFullPageLoader();
    const data = $('#loginForm').serialize();
    $.ajax({
      url: `/login`,
      type: 'POST',
      data,
      cache: false,
      headers: { 'cache-control': 'no-cache' },
      success(result) {
        hideFullPageLoader();
        if (result.status === true) {
          window.location.href = result.url;
        } else {
          showToaster("error", result.message, 5000);
        }
       },
      error(error) {
        hideFullPageLoader();
        showToaster("error", error.message, 5000);
      },
    });
  }

$(document).ready(function () {
  let remember = $.cookie('pm[remember]');
  if (remember) {
    $('#inputUsername').val($.cookie('pm[email]'));
    $('#inputPassword').val($.cookie('pm[password]'));
    if (remember === "true" || remember === true) {
      $('#remember-me').attr("checked", true);
      $('.form-control-placeholder').addClass('freeze');
    }
  }
// Disabled right click
   document.addEventListener("contextmenu", function(e){
      e.preventDefault();
    }, false);
    document.addEventListener("keydown", function(e) {
     if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
        disabledEvent(e);
      }
      if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
        disabledEvent(e);
      }
      if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
        disabledEvent(e);
      }
      if (e.ctrlKey && e.keyCode == 85) {
        disabledEvent(e);
      }
      if (event.keyCode == 123) {
        disabledEvent(e);
      }
    }, false);
    function disabledEvent(e){
      if (e.stopPropagation){
        e.stopPropagation();
      } else if (window.event){
        window.event.cancelBubble = true;
      }
      e.preventDefault();
      return false;
    }
});

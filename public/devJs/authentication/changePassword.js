$(document).ready(() => {

  $.validator.addMethod("passwordRegex", function(value, element) {
    let val = value.trim();
    return this.optional(element) || /^[a-zA-Z0-9@#%&*.-]/i.test(val);
  }, "Address is invalid: Special characters like <,>,' are not allowed.");

  $("form[name='changePasswordForm']").validate({
    errorClass:'invalid-feedback',
    rules: {
      newPassword: {
        required: true,
        minlength: 5
      },
      confirmPassword: {
        required: true,
        passwordRegex: true,
        equalTo : "#inputPassword"
      }
    },
    highlight: function (element) {
    },
    unhighlight:function (element) {
    },
    success: function (label,element) {
    },    
    messages: {
      newPassword: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      confirmPassword: {
        required: "Please provide a password",
        equalTo:'Please enter the same password.'
      }
    },
    submitHandler: function(form) {
      sendAjaxForChangePassword();
    }
  });
  applyFloatingLabels();
});


function sendAjaxForChangePassword() {
    showFullPageLoader();
    const formData = $('#changePassowordForm').serialize();
    $.ajax({
      url: '/changePassword',
      type: 'POST',
      data: formData,
      success(result) {
        hideFullPageLoader();
        $('#changePassowordForm').trigger("reset");
        if (result.status == '200') {
            $('#sucessMsgDiv').css('display','block');
            showToaster("success", result.message, 5000);
        } else {
            showToaster("error", result.message, 5000);
        }
      },
      error(data) {
        hideFullPageLoader();
        $('#changePassowordForm').trigger("reset");
        showError(data.responseJSON.message, 5000);
      },
    });
}


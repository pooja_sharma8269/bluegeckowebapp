window.onload = function() {
    document.addEventListener("contextmenu", function(e){
      e.preventDefault();
    }, false);
    document.addEventListener("keydown", function(e) {
     if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
        disabledEvent(e);
      }
      if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
        disabledEvent(e);
      }
      if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
        disabledEvent(e);
      }
      if (e.ctrlKey && e.keyCode == 85) {
        disabledEvent(e);
      }
      if (event.keyCode == 123) {
        disabledEvent(e);
      }
    }, false);
    function disabledEvent(e){
      if (e.stopPropagation){
        e.stopPropagation();
      } else if (window.event){
        window.event.cancelBubble = true;
      }
      e.preventDefault();
      return false;
    }
  };



$(document).on('ready', function () {
    var nav_offset_top = $('header').height();
// Navbar Fixed
function navbarFixed() {
    if ($('.main-header-area').length) {
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll >= nav_offset_top) {
                $(".main-header-area").addClass("navbar-fixed");
            } else {
                $(".main-header-area").removeClass("navbar-fixed");
            }
        });
    }
}
navbarFixed();

//Search
$('.icon_search, .ti-close').on('click', function () {
    if ($(".search_area").hasClass('open')) {
        $(".search_area").removeClass('open')
    }
    else {
        $(".search_area").addClass('open')
    }
    return false
});

//Navigation
if ($.fn.navigation) {
    $("#navigation").navigation();
    $("#always-hidden-nav").navigation({
        hidden: true
    });
}
    // slider
    $slick_slider = $('.slider');
    settings_slider = {
    dots: true,
    arrows: false,
    centerMode: false,
    rows: 2,
    autoplay: true,
    autoplaySpeed: 3000,
    slidesToShow: 1,
    variableWidth: true,
    centerPadding: '15px',
    }

    slick_on_mobile($slick_slider, settings_slider);

    // slick on mobile
    function slick_on_mobile(slider, settings) {

        $(window).on('load resize', function () {
            if ($(window).width() > 767) {
                if (slider.hasClass('slick-initialized')) {
                    slider.slick('unslick');

                }
                return
            }
            if (!slider.hasClass('slick-initialized')) {
                return slider.slick(settings);
            }
        });
    }

    //Full Height
    var elements = document.getElementsByClassName("login1");
    var windowheight = window.innerHeight + "px";

    fullheight(elements);
    function fullheight(elements) {
    for (let el in elements) {
        if (elements.hasOwnProperty(el)) {
        elements[el].style.height = windowheight;
        }
    }
    }

    window.onresize = function(event) {
    fullheight(elements);
    };
});

function applyFloatingLabels(){
    var formFields = $('.form-group');
    formFields.each(function() {
      var field = $(this);
      var input = field.find('.form-control');
      var label = field.find('.form-control-placeholder');
      checkInput(input,label);
      input.change(function() {
        checkInput(input,label)
      })
    });
  }

  function checkInput(input,label) {
    var valueLength = input.val() ? input.val().length : 0;
    if (valueLength > 0 ) {
      label.addClass('freeze')
    } else {
      label.removeClass('freeze')
    }
  }

function activeHeaderTab(id) {
    $('#allHeaderTabs li').removeClass('active');
    setTimeout(function (){
      $("#" + id).addClass('active');
    }, 300);
}

// Loaders on all ajax call start

function showFullPageLoader() {
    //$(document.getElementsByTagName('body')[0]).find('.ui.page.dimmer.fullPageLoader').addClass('loader');
    $('.fullPageLoader').show();
}

  function hideFullPageLoader() {
    //$(document.getElementsByTagName('body')[0]).find('.ui.page.dimmer.fullPageLoader').removeClass('loader');
    $('.fullPageLoader').hide();
  }

  let generateCircle = (circleColor, percent, htmlElementId, label) => {

    var ratio = percent / 100;

    var w = 200, h = 200;

    var outerRadius = (w / 3) -10;  // size of circle
    var innerRadius = 63;

    var color = ['#f2503f', '#ea0859', '#e2e2e2'];
    var svg = d3.select(htmlElementId)
        .append("svg")
        .attr("width", w)
        .attr("height", h).append('g')
        .attr("transform", 'translate(' + w / 2 + ',' + h / 2 + ')');

    var pie = d3.pie()
        .value(function (d) { return d })
        .sort(null);

    // createGradient(svg, 'gradient', color[0], color[1]);
    var arc = d3.arc()
        .innerRadius(innerRadius)
        .outerRadius(outerRadius)
        .startAngle(0)
        .endAngle(2 * Math.PI);

    var arcLine = d3.arc()
        .innerRadius(innerRadius)
        .outerRadius(outerRadius)
        .startAngle(0);

    svg.append('path')
        .attr("d", arc)
        .style("fill", color[2]); // circle line color


    var pathChart = svg.append('path')
        .datum({ endAngle: 0 })
        .attr("d", arcLine)
        .style("fill", circleColor);  // % completion color, exmple :55%

    var middleCount = svg.append('text')
        .text(function (d) {
            return d;
        })
        .attr("class", 'middleText')
        .attr('text-anchor', 'middle')
        .attr("dx", 0)
        .attr("dy", 10)
        .style("fill", '#5b5b5b')
        .style('font-size', '35px'); // percent font size

    svg.append('text')
        .attr("class", 'percent')
        .attr('text-anchor', 'middle')
        .attr("dx", 50)
        .attr("dy", -5)
        .style("fill", color[1])
        .style('font-size', '25px');
    textX = -45;
    textY = 90;
    if(label === 'Media Response Audio'){
        textX = -75;
        textY = 95;
    }
    svg.append("text")
        .attr("text-anchor", "bottom")
        .attr('font-size', '15px')
        .attr('x', textX)
        .attr('y', textY)
        .text(label);

    var arcTween = function (transition, newAngle) {
        transition.attrTween("d", function (d) {
            var interpolate = d3.interpolate(d.endAngle, newAngle);
            var interpolateCount = d3.interpolate(0, percent);
            return function (t) {
                d.endAngle = interpolate(t);
                middleCount.text(Math.floor(interpolateCount(t)) + "%");
                return arcLine(d);
            };
        });
    };

    var animate = function () {
        pathChart.transition()
            .duration(750)
            .ease(d3.easeCubic)
            .call(arcTween, ((2 * Math.PI)) * ratio);
    };
    setTimeout(animate, 0);
};
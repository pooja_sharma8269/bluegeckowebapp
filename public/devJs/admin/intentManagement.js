var intentData = [];
activeHeaderTab('adminSettings');
$(document).ready(() => {
  getIntentListForUser();
});

function getIntentListForUser() {
  showFullPageLoader();
  $.ajax({
    url: '/getIntentList',
    data: { type: 1 },
    type: 'GET',
    success(result) {
      if (result.code == 200) {
        intentData = result.intentList[0];
        showIntentList(intentData);
      } else if ((result.code == 404)) {
        showIntentList([]);
      }
    },
    error(err) {
      hideFullPageLoader();
      showToaster("error", err.message, 5000);
    }
  });
}


function showIntentList(intentList) {
 var rows = ``;
  if (intentList.length > 0) {
    $.each(intentList, function () {
      rows += `<tr><td>` + this.Inputs_Intent + `</td><td class="text-center">` + this.TotalVolume + `</td><td class="text-center">` + this.PlatformName + `</td>`;
      if(this.isCompatible){
      rows += `<td class="text-center"><i class="fas fa-check-circle text-success"></i></td>`;
     }else{
      rows += `<td class="text-center"><i class="fas fa-times-circle text-danger"></i></td>`;
     }
     if(this.isTranslated){
      rows += `<td class="text-center"><i class="fas fa-check-circle text-success"></i></td>`;
     }else{
      rows += `<td class="text-center"><i class="fas fa-times-circle text-danger"></i></td>`;
     }
      if (this.IsStatic) {
        rows += `<td class="text-center"><i class="fas fa-check-circle text-success"></i></td>`;
      } else {
        rows += `<td class="text-center"><i class="fas fa-times-circle text-danger"></i></td>`;
      }
      if (this.IsUserControlled) {
        rows += `<td class="text-center"><i class="fas fa-check-circle text-success"></i></td>`;
      } else {
        rows += `<td class="text-center"><i class="fas fa-times-circle text-danger"></i></td>`;
      }
      if (this.IsActive) {
        rows += `<td class="text-center"><i class="fas fa-check-circle text-success"></i></td><td>` + moment(this.ValidFromDate).format("MM/DD/YYYY HH:mm A") + `</td><td>` + moment(this.ValidToDate).format("MM/DD/YYYY HH:mm A") + `</td><td class="intent-actions text-center"><a href="/eventStateMaintenance/` + this.Inputs_Intent + `" title="Edit" class="text-primary"><i class="fas fa-pencil-alt"></i></a></td></tr>`;
      } else {
        rows += `<td class="text-center"><i class="fas fa-times-circle text-danger"></i></td><td>` + moment(this.ValidFromDate).format("MM/DD/YYYY HH:mm A") + `</td><td>` + moment(this.ValidToDate).format("MM/DD/YYYY HH:mm A") + `</td><td class="intent-actions text-center"><a href="/eventStateMaintenance/` + this.Inputs_Intent + `" title="Edit" class="text-primary"><i class="fas fa-pencil-alt"></i></a></td></tr>`;
      }
    });
    $(rows).appendTo("#intentManagmentDashboard tbody");
    const userTable = $('#intentManagmentDashboard').DataTable({
      paging: true,
      pageLength: 15,
      pagingType: "full_numbers",
      info: false,
      ordering: true,
      order: [[1, "desc"]],
      destroy: true,
      bInfo: false,
      lengthChange: false,
      language: {
        paginate: {
          previous: '<span aria-hidden="true">«</span>',
          next: '<span aria-hidden="true">»</span>'
        }
      },
      ordering: true,
      dom: '<bottam>p',
      initComplete() {
        $('.dataTables_empty').html(`<div class="ui center emptyBox" id="intentManagmentDashboard">
          <i class="font50 unordered list icon ml10 mr10"></i>
          <p>No intents available</p>
        </div>`);
      },
      processing: true,
    });
    $('#searchIntent').on('keyup', function () {
      userTable.search($(this).val()).draw();
    });
    hideFullPageLoader();
  } else {
    $(rows).appendTo("#intentManagmentDashboard tbody");
    const userTable = $('#intentManagmentDashboard').DataTable({
      paging: false,
      dom: '<bottam>p',
      initComplete() {
        $('.dataTables_empty').html(`<div class="ui center emptyBox" id="intentManagmentDashboard">
            <i class="font50 unordered list icon ml10 mr10"></i>
            <p>No intents available</p>
          </div>`);
      },
      processing: true,
    });
    $('#searchIntent').on('keyup', function () {
      userTable.search($(this).val()).draw();
    });
    hideFullPageLoader();
  }
}
let userEmail = '';
$(document).ready(() => {
  getUserList();
});
activeHeaderTab('adminUsers');

function getUserList() {
  showFullPageLoader();
  $.ajax({
    url: '/userListing',
    type: 'GET',
    success(result) {
      if (result.code == 200) {
        showUserTable(result.userList);
      } else if (result.code == 404) {
        showUserTable([]);
      }
    },
    error(err) {
      hideFullPageLoader();
      showToaster("error", err.responseJSON.message, 5000);
    }
  });
}

function showUserTable(userList) {
  var rows = ``;
  if (userList.length > 0) {
    $.each(userList, function () {
      var role = '';
      if (this.UserRoleID == 1) {
        role = "Admin User";
      } else if (this.UserRoleID == 2) {
        role = "Plugin User";
      } else {
        role = "Witness User";
      }
      if (this.IsActive) {
        rows += `<tr><td>` + this.FirstName + ` ` + this.LastName + `</td><td>` + this.EmailAddress + `</td><td>` + role + `</td><td>` + this.OrganizationName + `</td><td>` + this.City + `</td><td>` + this.Country + `</td><td class="actionicon"> <a href="/editUser/` + this.EncryptedEmailAddress + `" title="Edit" class="mr-3 editIcon"><i class="fas fa-pencil-alt"></i></a><span onclick="userDeleteConfirmation('` + this.EmailAddress + `')"><a href="javascript:void(0)" class="mr-3 highlighter-ng text-danger" title="Delete"><i class="far fa-trash-alt "></i></a></span><a href="javascript:void(0)" onclick="activeDeactiveUser(0,'` + this.EmailAddress + `')" class="text-success" title="Deactivate User"><i class="fas fa-user-check"></i></a></td></tr>`;
      } else {
        rows += `<tr><td>` + this.FirstName + ` ` + this.LastName + `</td><td>` + this.EmailAddress + `</td><td>` + role + `</td><td>` + this.OrganizationName + `</td><td>` + this.City + `</td><td>` + this.Country + `</td><td class="actionicon"> <a href="/editUser/` + this.EncryptedEmailAddress + `" title="Edit" class="mr-3 editIcon"><i class="fas fa-pencil-alt"></i></a><span onclick="userDeleteConfirmation('` + this.EmailAddress + `')"><a href="javascript:void(0)" class="mr-3 highlighter-ng text-danger" title="Delete"><i class="far fa-trash-alt "></i></a></span><a href="javascript:void(0)" onclick="activeDeactiveUser(1,'` + this.EmailAddress + `')" class="text-danger" title="Activate User"><i class="fas fa-user-alt-slash"></i></a></td></tr>`;
      }
    });
    $(rows).appendTo("#userList tbody");
    const userTable = $('#userList').DataTable({
      paging: true,
      pageLength: 15,
      pagingType: "full_numbers",
      info: false,
      destroy: true,
      bInfo: false,
      lengthChange: false,
      language: {
        paginate: {
          previous: '<span aria-hidden="true">«</span>',
          next: '<span aria-hidden="true">»</span>'
        }
      },
      ordering: true,
      dom: '<bottam>p',
      initComplete() {
        $('.dataTables_empty').html(`<div class="ui center emptyBox" id="userList">
              <i class="font50 unordered list icon ml10 mr10"></i>
              <p>No users available</p>
            </div>`);
      },
      processing: true,
    });
    $('#searchUser').on('keyup', function () {
      userTable.search($(this).val()).draw();
    });
    hideFullPageLoader();
  } else {
    $(rows).appendTo("#userList tbody");
    const userTable = $('#userList').DataTable({
      paging: false,
      dom: '<bottam>p',
      initComplete() {
        $('.dataTables_empty').html(`<div class="ui center emptyBox" id="userList">
              <i class="font50 unordered list icon ml10 mr10"></i>
              <p>No users available</p>
            </div>`);
      },
      processing: true,
    });
    $('#searchUser').on('keyup', function () {
      userTable.search($(this).val()).draw();
    });
    hideFullPageLoader();
  }
}

function activeDeactiveUser(type, EmailAddress) {
    showFullPageLoader();
    $.ajax({
      url: '/activeDeactiveUser',
      type: 'GET',
      data: { EmailAddress, type },
      success(result) {
        showToaster("success", result.message, 5000);
        setTimeout(() => {
          hideFullPageLoader();
          location.reload(true);
        }, 5000);
      },
      error(err) {
        showToaster("error", err.responseJSON.message, 5000);
        setTimeout(() => {
          hideFullPageLoader();
          location.reload(true);
        }, 5000);
      }
    });
}

function userDeleteConfirmation(EmailAddress) {
  userEmail = EmailAddress;
  $("#deleteUser").modal('show');
}

$('#confirmDelete').click((e) => {
  $("#deleteUser").modal('hide');
  activeDeactiveUser(2,userEmail);
  userEmail = '';
});
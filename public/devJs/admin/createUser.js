$(document).ready(() => {
  $.validator.addMethod("addressRegex", function (value, element) {
    let val = value.trim();
    return this.optional(element) || /^[a-zA-Z0-9 ().-]/i.test(val);
  }, "Address is invalid: Special characters like <,>,' are not allowed.");

  $.validator.addMethod("organizationNameRegex", function (value, element) {
    let val = value.trim();
    return this.optional(element) || /^[a-zA-Z0-9 ().-]/i.test(val);
  }, "Organization Name is invalid: Special characters like <,>,' are not allowed.");

  $.validator.addMethod("passwordRegex", function (value, element) {
    let val = value.trim();
    return this.optional(element) || /^[a-zA-Z0-9@#%&*.-]/i.test(val);
  }, "Password is invalid: Special characters like <,>,' are not allowed.");

$.validator.addMethod("publicScreenRegex", function(value, element) {
    let val = value.trim();
    return this.optional(element) || /^[a-zA-Z0-9\s]+$/i.test(val);  
 }, "Special characters are not allowed.");

  $('form[name=\'createUserProfile\']').validate({
    errorClass: 'invalid-feedback',
    //validClass: 'valid-feedback',
    rules: {
      EmailAddress: {
        required: true,
        email: true,
        remote: {
          url: "/isEmailExists",
          type: "post",
          data: {
            email: function () {
              return $("#email").val();
            }
          },
          dataFilter: function (data) {
            let objectdata = JSON.parse(data);
            if (objectdata.status === true) {
              return false;
            } else {
              return true;
            }
          },
          complete: function (xhr) {
            let objectdata = JSON.parse(xhr.responseText);
            if (objectdata.status === true) {
              return false;
            } else {
              return true;
            }
          }
        }
      },
      UserPassword: {
        required: true,
        minlength: 5
      },
      FirstName: {
        required: true,
        publicScreenRegex: true
      },
      LastName: {
        required: true,
        publicScreenRegex: true
      },
      PublicScreenTag: {
        required: true,
        publicScreenRegex: true
      },
      PostalCode: {
        required: true,
        number: true,
      },
      OrganizationName: {
        required: true,
        organizationNameRegex: true
      },
      AddressLine1: {
        required: true,
        addressRegex: true
      },
      AddressLine2: {
        required: true,
        addressRegex: true
      },
      cUserPassword: {
        required: true,
        passwordRegex: true,
        equalTo: "#password"
      },
      userCaptcha: {
        required: true
      }
    },
    highlight: function (element) {
      //$(element).removeClass('is-valid');
    },
    unhighlight: function (element) {
    },
    success: function (label, element) {
      //$(element).addClass('freeze');
    },
    messages: {
      EmailAddress: {
        required: 'Please provide a valid Email.',
        email: 'Your email address must be in the format of name@domain.com',
        remote: 'Email Address already exists.'
      },
      UserPassword: { required: 'Please provide a valid Password.' },
      FirstName: {
        required: 'Please provide a valid FirstName.',
      },
      LastName: {
        required: 'Please provide a valid LastName.',
      },
      PublicScreenTag: {
        required: 'Please provide a valid display name.',
      },
      PostalCode: {
        required: 'Please provide a valid Postal Code.',
        number: 'Please enter only numbers.'
      },
      OrganizationName: { required: 'Please provide a valid organization name.' },
      AddressLine1: { required: 'Please provide a valid Address.' },
      AddressLine2: { required: 'Please provide a valid Address.' },
      cUserPassword: {
        required: 'Please provide a valid Confirm Password.',
        equalTo: 'Please enter the same password again.'
      },
      userCaptcha: { required: 'Please provide a valid Captcha.' }
    },
    submitHandler: function (form) {
      sendAuthenticationRequest();
    }
  });
  getCaptcha();
  getUserDropdownList();
  applyFloatingLabels();
});

function getCaptcha() {
  showFullPageLoader();
  $.ajax({
    url: '/captcha',
    type: 'GET',
    success(result) {
      $("#captcha").append(`<div id="captchaRemove">` + result + `</div></div>`);
      hideFullPageLoader();
    },
  });
}

function sendAuthenticationRequest() {
  showFullPageLoader();
  let pageValue = $('#hiddenDiv').text()
  const data = $('#createUserProfile').serialize() + '&pageValue=' + pageValue;
  $.ajax({
    url: `/registerUser`,
    type: 'POST',
    data,
    cache: false,
    headers: { 'cache-control': 'no-cache' },
    success(result) {
      if (result.status === true) {
        setTimeout(function () {
          if (result.code === 200 || result.code === 201) {
            showToaster("success", result.message, 5000);
            $("form")[0].reset();
            $("#captchaRemove").remove();
            getCaptcha();
            //window.location.href = result.url;
            location.reload(true);
          }
        }, 7000)
      } else {
        showToaster("error", result.message, 5000);
      }
    },
    error(error) {
      hideFullPageLoader();
      $("#captchaRemove").remove();
      getCaptcha();
      showToaster("error", error.responseJSON.message, 5000);
    },
  });
}

$('#pincode').change(function () {
  showFullPageLoader();
  var zipcode = $('#pincode').val();
  $.ajax({
    method: "GET",
    url: '/demographics/' + zipcode,
  })
    .done(function (response) {
      hideFullPageLoader();
      const city = response.body.data[0].City;
      const state = response.body.data[0].State;
      const country = response.body.data[0].Country;
      const county = response.body.data[0].County;
      const utctimezone = response.body.data[0].UTCTimeZone + ' ' + response.body.data[0].UTCTimeOffset;
      $('#city').val(city);
      $('#state').val(state);
      $('#country').val(country);
      $('#utctimezone').val(utctimezone);
      $('#county').val(county);
      applyFloatingLabels();
    })
    .fail(function (response) {
      hideFullPageLoader();
      $('#city').val('');
      $('#state').val('');
      $('#country').val('');
      $('#utctimezone').val('');
      showToaster("error", "Please enter valid postal code.", 5000);
    });
});

function getUserDropdownList() {
  showFullPageLoader();
  $.ajax({
    url: '/getUserDropdownList',
    type: 'GET',
    success(result) {
      showCreateUserList(result.userRoleList);
    },
    error(err) {
      hideFullPageLoader();
      showCreateUserList([]);
    }
  });
}

function showCreateUserList(userRoleList) {
  try {
    var roleList = document.getElementById("createUserRole");
    if (userRoleList.length > 0) {
      $.each(userRoleList, function () {
        roleList.add(new Option(this.ROLEDESCRIPTION, this.USERROLEID));
      });
      $("#createUserRole").val("3").attr("selected", "selected");
    } else {
      roleList.add(new Option("No User Role Found", null));
      $("#createUserRole").attr("disabled", true);
      hideFullPageLoader();
    }
  } catch (error) {
    hideFullPageLoader();
  }
}


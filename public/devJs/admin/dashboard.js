var intentData = [];
var lastDate, currentDate, userTable, selectedIntent = '';
activeHeaderTab('adminDashboard');
$(document).ready(() => {
  showDateData();
  getIntentAnalyticsData();
  getIntentListForUser();
})

function getCurrentDate() {
  var currentDate = new Date();
  var dd = String(currentDate.getDate()).padStart(2, '0');
  var mm = String(currentDate.getMonth() + 1).padStart(2, '0');
  var yyyy = currentDate.getFullYear();
  currentDate = mm + '/' + dd + '/' + yyyy;
  return currentDate
}

function getLastDate() {
  var today = new Date();
  var lastDate = new Date(new Date().setDate(today.getDate() - 30));
  var dd = String(lastDate.getDate()).padStart(2, '0');
  var mm = String(lastDate.getMonth() + 1).padStart(2, '0');
  var yyyy = lastDate.getFullYear();
  lastDate = mm + '/' + dd + '/' + yyyy;
  return lastDate;
}

function showDateData() {
  $('#id_4').datepicker("destroy");
  $('#id_5').datepicker("destroy");
  lastDate = getLastDate();
  currentDate = getCurrentDate();

  $("#oprationDashboardLastDate").datepicker('setDate', lastDate);
  $("#id_4").datepicker('setEndDate', currentDate);
  let tempLastDate = $('#oprationDashboardLastDate').val();
  if (tempLastDate) {
    $("#id_4").on("changeDate", function () {
      $('#intentTXHistory').DataTable().destroy();
      $('#intentTXHistory tbody').empty();
      lastDate = $('#oprationDashboardLastDate').val();
      $("#id_5").datepicker('setStartDate', lastDate);
      selectedIntent = $('#intent').val();
      getIntentTxHistory(selectedIntent);
      getIntentAnalyticsData();
      $('.datepicker').hide();
    });
  }

  $("#oprationDashboardCurrentDate").datepicker('setDate', currentDate);
  $("#id_5").datepicker('setEndDate', currentDate);
  let tempCurrentDate = $('#oprationDashboardCurrentDate').val();
  if (tempCurrentDate) {
    $("#id_5").on("changeDate", function () {
      $('#intentTXHistory').DataTable().destroy();
      $('#intentTXHistory tbody').empty();
      currentDate = $('#oprationDashboardCurrentDate').val();
      $("#id_4").datepicker('setEndDate', currentDate);
      if ((Date.parse(currentDate) < Date.parse(lastDate))) {
        currentDate = getCurrentDate();
        showFullPageLoader();
        $("#id_5").datepicker('setDate', currentDate);
        $("#date-alert").html('Please select from date first...');
        $("#date-alert").addClass("d-block");
        setTimeout(function () {
          $("#date-alert").removeClass("d-block");
          hideFullPageLoader();
        }, 2000);
      } else {
        selectedIntent = $('#intent').val();
        getIntentTxHistory(selectedIntent);
        getIntentAnalyticsData();
      }
      $('.datepicker').hide();
    });
  }
}

//Tooltip
$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});

$('#id_4,#id_5').datepicker({
  dateFormat: 'mm-dd-yy',
  changeMonth: true,
  fautoclose: true,
  todayHighlight: true,
});

function getIntentListForUser() {
  showFullPageLoader();
  $.ajax({
    url: '/getIntentList',
    data: { type: 1 },
    type: 'GET',
    success(result) {
      if (result.code == 200) {
        intentData = result.intentList[0];
        showIntentList(intentData);
        getIntentTxHistory(intentData[0].IntentID);
      } else if (result.code == 404) {
        showIntentList([]);
      }
    },
    error(err) {
      hideFullPageLoader();
      showToaster("error", err.responseJSON.message, 5000);
    }
  });
}

function getIntentTxHistory(IntentID) {
  showFullPageLoader();
  $.ajax({
    url: '/intentTxHistory',
    type: 'GET',
    data: { IntentID: IntentID, lastDate: lastDate, currentDate: currentDate },
    success(result) {
      if (result.code == 200) {
        intentData = result.intentList[0];
        showIntentTxHitsory(intentData);
      } else if (result.code == 404) {
        showIntentTxHitsory([]);
      }
    },
    error(err) {
      hideFullPageLoader();
      showToaster("error", err.responseJSON.message, 5000);
    }
  });
}


function getIntentAnalyticsData(tempCheck) {
  showFullPageLoader();
  $.ajax({
    url: '/intentAnalytics',
    type: 'GET',
    data: { lastDate: lastDate, currentDate: currentDate,type : 1},
    success(result) {
      if (result.code == 200) {
        showIntentAnalyticsData(result.data, tempCheck);
      } else if (result.code == 404) {
        showIntentAnalyticsData([]);
      }
    },
    error(err) {
      hideFullPageLoader();
      showToaster("error", err.responseJSON.message, 5000);
    }
  });
}

function compare(a, b) {
  let intentOne = parseInt(a.IntentID);
  let sortedIntent = parseInt(b.IntentID);

  let comparison = 0;
  if (intentOne > sortedIntent) {
    comparison = 1;
  } else if (intentOne < sortedIntent) {
    comparison = -1;
  }
  return comparison;
}

function showIntentList(intentData) {
  var intent = document.getElementById("intent");
  if (intentData.length > 0) {
    intentData.sort(compare);
    $.each(intentData, function () {
      intent.add(new Option(this.Inputs_Intent, this.IntentID));
    });
  } else {
    intent.add(new Option("No Intent Found", null));
    $("#intent").attr("disabled", true);
    hideFullPageLoader();
  }
}

function showIntentTxHitsory(intentData) {
  var rows = ``;
  if (intentData.length > 0) {
    $.each(intentData, function () {
     // var time = this.TimeInterval.split(' ');
      rows += `<tr><td class="text-center">` + moment(this.TimeStamp).format("MM/DD/YYYY HH:mm A") + `<td class="text-center">` + this.ServerInstanceName + `</td><td class="text-center">` + this.Language + `</td><td class="text-center">` + '--' + `</td><td class="text-center">` + this.NewUser + `</td></tr>`;

   // rows += `<tr><td>` + moment(this.TimeStamp).format("MM/DD/YYYY HH:mm A") + `<td>` + this.ServerInstanceName + `</td><td>` + this.Language + `</td><td>` + this.Country + `</td><td>` + this.NewUser + `</td><td class="text-center">` + time[0] + `</td><td class="text-center">` + time[2] + `</td><td class="text-center">` + time[4] + `</td><td class="text-center">` + time[6] + `</td></tr>`;
    });
    $(rows).appendTo("#intentTXHistory tbody");
    userTable = $('#intentTXHistory').DataTable({
      paging: true,
      autoWidth:false,
      pageLength: 15,
      pagingType: "full_numbers",
      info: true,
      destroy: true,
      language: {
        paginate: {
          previous: '<span aria-hidden="true">«</span>',
          next: '<span aria-hidden="true">»</span>'
        }
      },
      dom: '<bottam>p',
      initComplete() {
        $('.dataTables_empty').html(`<div class="ui center emptyBox" id="intentListDashboard">
          <i class="font50 unordered list icon ml10 mr10"></i>
          <p>No intents available</p>
        </div>`);
      },
      processing: true,
    });
    $('#searchIntentTx').on('keyup', function () {
      userTable.search($(this).val()).draw();
    });
    hideFullPageLoader();
  } else {
    $(rows).appendTo("#intentTXHistory tbody");
    let userTable = $('#intentTXHistory').DataTable({
      paging: false,
      autoWidth:false,
      destroy: true,
      dom: '<bottam>p',
      initComplete() {
        $('.dataTables_empty').html(`<div class="ui center emptyBox" id="intentTXHistory">
             <i class="font50 unordered list icon ml10 mr10"></i>
             <p>No intents available</p>
           </div>`);
      },
      processing: true,
    });
    $('#searchIntentTx').on('keyup', function () {
      userTable.search($(this).val()).draw();
    });
    hideFullPageLoader();
  }
}

$("#intent").on('change', function () {
  showFullPageLoader();
  $('#intentTXHistory').dataTable().fnClearTable();
  $('#intentTXHistory').DataTable().destroy();
  $('#intentTXHistory tbody').empty();
  getIntentTxHistory(this.value);
});

function showIntentAnalyticsData(intentAnalyticsData) {
  if (intentAnalyticsData.length > 0) {
    if (intentAnalyticsData[3].length >= 0) {
      $('#intentsMatched').text(intentAnalyticsData[3][0].IntentsMatched);
    } else {
      $('#intentsMatched').text("--");
    }
    if (intentAnalyticsData[4].length >= 0) {
      $('#followupRequests').text(intentAnalyticsData[4][0].FOLLOWUPREQUESTS);
    } else {
      $('#followupRequests').text("--");
    }
    if (intentAnalyticsData[5].length >= 0) {
      $('#intentsPerMin').text(intentAnalyticsData[5][0].IntentsPerMin);
    } else {
      $('#intentsPerMin').text("--");
    }
  } else {
    $('#intentsMatched').text("--");
    $('#followupRequests').text("--");
    $('#intentsPerMin').text("--");
  }
}
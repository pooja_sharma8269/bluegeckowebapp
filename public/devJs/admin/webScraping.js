activeHeaderTab('adminSettings');
let intentQA = [];
let urlData = [];
let filteredURLs = [], translateLanguageVal = [], intentInformation = [];
let currentIntent;
var current_progress = 0;
const chunk = 100; // Chunk size for web scraping
const translateChunk = 15; // Chunk size for translation


//Tooltip
$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});

$(document).ready(() => {
  getURLList();
});

function getURLList() {
  showFullPageLoader();
  $.ajax({
    url: '/getIntentList',
    data: { type: 2 },
    type: 'GET',
    success(result) {
      if (result.code == 200) {
        filteredURLs = urlData = result.intentList[1];
        intentInformation = result.intentList
        // intentQA = result.intentList[2];
        translateLanguageVal = result.intentList[0];
        let uniqueFrequency = [...new Set(urlData.map(item => item.ChangeFrequency))];
        showFrequencyDropdown(uniqueFrequency);
        showLanguageDatafortranslation(translateLanguageVal)
        showUrlList(filteredURLs);
        showScrapedIntentDataInformation(intentInformation)
        currentIntent = urlData[0].Inputs_Intent;
        $('#languageIntent').empty();
        // showLanguageDropdown(currentIntent);
        // showQA(currentIntent, "en-US");
        getIntentQA(translateLanguageVal);
      } else if (result.code == 404) {
        showUrlList([]);
      }
    },
    error(err) {
      hideFullPageLoader();
      showToaster("error", err.responseJSON.message, 5000);
    }
  });
}

function getIntentQA(translateLanguageVal) {
  showFullPageLoader();
  $.ajax({
    url: '/getIntentQA',
    data: { translateLanguageVal },
    type: 'GET',
    success(result) {
      if (result.code == 200) {
        intentQA = result.intentList;
        showLanguageDropdown(currentIntent);
        showQA(currentIntent, "en-US");
        hideFullPageLoader();
      } else if (result.code == 404) {
        //
      }
    },
    error(err) {
      hideFullPageLoader();
      showToaster("error", err.responseJSON.message, 5000);
    }
  });
}

// Scrap sitemap.xml
$('#getSitemapData').click((e) => {
  scrapSiteMap();
});

let scrapSiteMap = async () => {
  $("#siteMapWebscrapNotificationMessage").html('Please wait while scraping data from sitemap...');
  await $.ajax({
    url: '/updateSiteMapScrapedDataToDatabase',
    type: 'POST',
    data: { type: 1 },
    success(result) {
      if (result.code == 200) {
        $("#sitemapSrcapSuccess-alert").addClass("d-block");
        setInterval(() => {
          window.location.reload();
        }, 3000);
      } else if (result.code == 404) {
        hideFullPageLoader();
      }
    },
    error(err) {
      hideFullPageLoader();
    }
  });

}


function showUrlList(urlList) {
  var rows = ``;
  if (urlList.length > 0) {
    $.each(urlList, function () {
      rows += `<tr><td class="text-uppercase">` + this.Inputs_Intent + `</td><td id="DOCUMENTURL"><a href=${this.DocumentURL} target="_blank">` + this.DocumentURL + `</a></td><td>` + moment(this.DateLastChanged).format("MM/DD/YYYY HH:mm A") + `</td><td class="capitalize">` + this.ChangeFrequency + `</td><td class="text-center"><i class="fas fa-check-circle text-success"</i></td></tr>`;
    });
    hideFullPageLoader();

    $(rows).appendTo("#urlTable tbody");
    const urlDataTable = $('#urlTable').DataTable({
      paging: true,
      pageLength: 15,
      pagingType: "full_numbers",
      info: false,
      destroy: true,
      bInfo: false,
      lengthChange: false,
      language: {
        paginate: {
          previous: '<span aria-hidden="true">«</span>',
          next: '<span aria-hidden="true">»</span>'
        }
      },
      ordering: true,
      dom: '<bottam>p',
      initComplete() {
        $('.dataTables_empty').html(`<div class="ui center emptyBox" id="urlTable">
            <i class="font50 unordered list icon ml10 mr10"></i>
            <p>No records available</p>
          </div>`);
      },
      processing: true,
    });

    $('#urlSearch').on('keyup', function () {
      urlDataTable.search($(this).val()).draw();
    });

    hideFullPageLoader();
  } else {
    $(rows).appendTo("#urlTable tbody");
    const urlDataTable = $('#urlTable').DataTable({
      paging: false,
      dom: '<bottam>p',
      initComplete() {
        $('.dataTables_empty').html(`<div class="ui center emptyBox" id="urlTable">
            <i class="font50 unordered list icon ml10 mr10"></i>
            <p>No records available</p>
          </div>`);
      },
      processing: true,
    });
    $('#urlSearch').on('keyup', function () {
      urlDataTable.search($(this).val()).draw();
    });

    hideFullPageLoader();
  }
}

function showScrapedIntentDataInformation(intentInformation) {
  if ($("#intentInfo").length) {
    $("#intentInfo").empty();
  }
  if (intentInformation.length > 0) {
    if (intentInformation[2].length >= 0) {
      $("#intentInfo").append(`<li>Total Active Intent Count : ${intentInformation[2][0].TotalActiveIntentCount}</li>`);
    } else {
      $("#intentInfo").append(`<li>Total Active Intent Count :  --- </li>`);
    }
    if (intentInformation[3].length >= 0) {
      $("#intentInfo").append(`<li>Total NonCompatible Count : ${intentInformation[3][0].TotalNonCompatibleCount}</li>`);
    } else {
      $("#intentInfo").append(`<li>Total NonCompatible Count :  --- </li>`);
    }
    if (intentInformation[4].length >= 0) {
      $("#intentInfo").append(`<li>Total Scraped Count : ${intentInformation[4][0].TotalScrapedCount}</li>`);
    } else {
      $("#intentInfo").append(`<li>Total Scraped Count : --- </li>`);
    }
    if (intentInformation[5].length >= 0) {
      $("#intentInfo").append(`<li>Newly Added Intent Count : ${intentInformation[5][0].NewlyAddedIntentCount}</li>`);
    } else {
      $("#intentInfo").append(`<li>Newly Added Intent Count : --- </li>`);
    }

  } else {
    $("#intentInfo").append(`<li>Total Active Intent Count :  --- </li>`);
    $("#intentInfo").append(`<li>Total NonCompatible Count :  --- </li>`);
    $("#intentInfo").append(`<li>Total Scraped Count : --- </li>`);
    $("#intentInfo").append(`<li>Newly Added Intent Count : --- </li>`);
  }

}

getFormattedDateDiff = function (date1, date2) {
  var Difference_In_Time = date2 - date1;
  var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
  return parseInt(Difference_In_Days);
};

const getDate = () => {
  var currentDate = new Date();
  return currentDate
}

//Update data to database
$('#updateURLData').click((e) => {
  const languageValue = $('#Translate-Language').val();
  $("#notificationMessage").html('Please wait while scraping data...');
  if (languageValue == 'en-US') {
    forScrapingURL(filteredURLs)
  }
});


$('#updateTranslateData').click((e) => {
  const languageValue = $('#Translate-Language').val();
  $("#notificationMessage").html('Please wait while translating data...');
  if (languageValue != 'en-US') {
    forTranslationUpdate(languageValue);
  }
});

let forScrapingURL = (filteredURLs) => {
  var data = [];
  for (var i in filteredURLs) {
    if (filteredURLs[i].IsScraped == 0) {
      const obj = {
        GQLIBRARYID: filteredURLs[i].GQLibraryID,
        DOCUMENTURL: filteredURLs[i].DocumentURL
      }
      data.push(obj);
    } else if (filteredURLs[i].ChangeFrequency == "monthly") {
      const scrabStartDate = filteredURLs[i].DateLastChanged;
      const scrabEndDate = getDate();
      const frequencyDiff = getFormattedDateDiff(moment(scrabStartDate), scrabEndDate);
      if (frequencyDiff >= 30) {
        const obj = {
          GQLIBRARYID: filteredURLs[i].GQLibraryID,
          DOCUMENTURL: filteredURLs[i].DocumentURL
        }
        data.push(obj);
      }
    } else if (filteredURLs[i].ChangeFrequency == "weekly") {
      const scrabStartDate = filteredURLs[i].DateLastChanged;
      const scrabEndDate = getDate();
      const frequencyDiff = getFormattedDateDiff(moment(scrabStartDate), scrabEndDate);
      if (frequencyDiff >= 7) {
        const obj = {
          GQLIBRARYID: filteredURLs[i].GQLibraryID,
          DOCUMENTURL: filteredURLs[i].DocumentURL
        }
        data.push(obj);
      }
    } else if (filteredURLs[i].ChangeFrequency == "daily") {
      const scrabStartDate = filteredURLs[i].DateLastChanged;
      const scrabEndDate = getDate();
      const frequencyDiff = getFormattedDateDiff(moment(scrabStartDate), scrabEndDate);
      if (frequencyDiff >= 1) {
        const obj = {
          GQLIBRARYID: filteredURLs[i].GQLibraryID,
          DOCUMENTURL: filteredURLs[i].DocumentURL
        }
        data.push(obj);
      }
    }
    if (Number(i) == filteredURLs.length - 1) {
      if (data.length > 0) {
        forXMLUpdate(data);
      } else {
        const chunkPerPercent = Number(((chunk * 100) / data.length).toFixed(2));
        updateProgress(chunkPerPercent);
      }
    }
  }
}

let forXMLUpdate = (data) => {
  var tempdata = [];
  const chunkPerPercent = Number(((chunk * 100) / data.length).toFixed(2));
  for (var i = 0; i < data.length; i += chunk) {
    tempdata.push(data.slice(i, i + chunk));
    if ((i + chunk) >= data.length) {
      xmlUpdate(tempdata, chunkPerPercent);
    }
  }
}

let xmlUpdate = async (tempArray, chunkPerPercent) => {
  for (var i in tempArray) {
    await $.ajax({
      url: '/updateWebScrapedDataToDatabase',
      type: 'POST',
      data: { data: JSON.stringify(tempArray[i]) },
      ajaxI: i,
      success(result) {
        if (result.returnValue == 0) {
          updateProgress(chunkPerPercent);
        } else {
          updateProgress(chunkPerPercent);
        }
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        if (XMLHttpRequest.readyState == 4) {
          updateProgress(chunkPerPercent);
        }
        else if (XMLHttpRequest.readyState == 0) {
          updateProgress(chunkPerPercent);
        }
        else {
          updateProgress(chunkPerPercent);
        }
      }
    });
  }
}

function forTranslationUpdate(languageValue) {
  var translateddata = [], translatedTempdata = [], urlTempData = [], tempData = [];
  var languageValueFiltered = translateLanguageVal.find(function (languageDetails) {
    return ((languageDetails.LanguageTag === languageValue));
  });

  let langSpecificQA = intentQA.filter(function (intents) {
    return intents.LANGUAGETAG === languageValueFiltered.LanguageTag;
  });

  let langSpecificToEngQA = intentQA.filter(function (intents) {
    return intents.LANGUAGETAG === 'en-US';
  });

  $.each(urlData, function () {
    const obj = {
      GQLIBRARYID: this.GQLibraryID,
    }
    urlTempData.push(obj);
  });

  //Find values that are in urlTempData but not in langSpecificQA
  tempData = urlTempData.filter(function (url) {
    return !langSpecificQA.some(function (intent) {
      return url.GQLIBRARYID == intent.GQLIBRARYID;
    });
  });

  //Find values that are in tempData and in langSpecificToEngQA
  translateddata = langSpecificToEngQA.filter(function (intent) {
    return tempData.some(function (url) {
      return url.GQLIBRARYID == intent.GQLIBRARYID;
    });
  });

  if (translateddata.length > 0) {
    const chunkPerPercent = Number(((translateChunk * 100) / translateddata.length).toFixed(2));
    for (var i = 0; i < translateddata.length; i += translateChunk) {
      translatedTempdata.push(translateddata.slice(i, i + translateChunk));
      if ((i + translateChunk) >= translateddata.length) {
        translatScrapedResponse(translatedTempdata, languageValueFiltered, chunkPerPercent);
      }
    }
  } else {
    const chunkPerPercent = Number(((chunk * 100) / translateddata.length).toFixed(2));
    OnTranslationupdateProgress(chunkPerPercent);
  }
}


let translatScrapedResponse = async (translatedTempdata, languageValueFiltered, chunkPerPercent) => {
  for (var i in translatedTempdata) {
    await $.ajax({
      url: '/translateCompleteWebScrapedResponse',
      type: 'POST',
      data: { data: JSON.stringify(translatedTempdata[i]), languageValueFiltered: JSON.stringify(languageValueFiltered) },
      ajaxI: i,
      success(result) {
        if (result.returnValue == 0) {
          OnTranslationupdateProgress(chunkPerPercent);
        } else {
          OnTranslationupdateProgress(chunkPerPercent);
        }
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        if (XMLHttpRequest.readyState == 4) {
          OnTranslationupdateProgress(chunkPerPercent)
        }
        else if (XMLHttpRequest.readyState == 0) {
          OnTranslationupdateProgress(chunkPerPercent)
        }
        else {
          OnTranslationupdateProgress(chunkPerPercent)
        }
      }
    });
  }
}

function OnTranslationupdateProgress(chunkPerPercent) {
  current_progress = Number((current_progress += chunkPerPercent).toFixed(2));
  if (current_progress > 100) {
    let progress = 100;
    $("#dynamic").css("width", progress + "%").attr("aria-valuenow", progress).text(progress + "% Complete");
    $("#translationSuccess-alert").addClass("d-block");
    setInterval(() => {
      window.location.reload();
    }, 5000);
  } else if (current_progress === 100) {
    $("#dynamic")
      .css("width", current_progress + "%")
      .attr("aria-valuenow", current_progress)
      .text(current_progress + "% Complete");
    $("#translationSuccess-alert").addClass("d-block");
    setInterval(() => {
      window.location.reload();
    }, 5000);
  } else {
    $("#dynamic")
      .css("width", current_progress + "%")
      .attr("aria-valuenow", current_progress)
      .text(current_progress + "% Complete");
  }
}

function showLanguageDatafortranslation(translatedLanguageVal) {
  var translatedLanguageValues = document.getElementById("Translate-Language");
  if (translatedLanguageVal.length > 0) {
    $.each(translatedLanguageVal, function () {
      translatedLanguageValues.add(new Option(this.LanguageName, this.LanguageTag));
    });
    $("#Translate-Language").val("en-US").attr("selected", "selected");
  } else {
    translatedLanguageValues.add(new Option("No Language Found", null));
  }
}


function showFrequencyDropdown(frequencyData) {
  var frequency = document.getElementById("Frequency-intent");
  frequency.add(new Option("All", null));
  if (frequencyData.length > 0) {
    $.each(frequencyData, function () {
      frequency.add(new Option(this, this));
    });
  } else {
    frequency.add(new Option("No Frequency Found", null));
  }
}

$("#Frequency-intent").on('change', function () {
  if (!(this.value == "null")) {
    filteredURLs = urlData.filter(obj => obj.ChangeFrequency == this.value);
    $('#urlTable').dataTable().fnClearTable();
    $('#urlTable').dataTable().fnDestroy();
    $('#urlTable tbody').empty();
    showUrlList(filteredURLs);
    currentIntent = filteredURLs[0].Inputs_Intent;
    $('#languageIntent').empty();
    showLanguageDropdown(currentIntent);
    showQA(currentIntent, "en-US");
  } else {
    filteredURLs = urlData;
    $('#urlTable').dataTable().fnClearTable();
    $('#urlTable').dataTable().fnDestroy();
    $('#urlTable tbody').empty();
    showUrlList(filteredURLs);
    currentIntent = filteredURLs[0].Inputs_Intent;
    $('#languageIntent').empty();
    showLanguageDropdown(currentIntent);
    showQA(currentIntent, "en-US");
  }
});


$("#Translate-Language").on('change', function () {
  if (this.value != "en-US") {
    $("#updateTranslateData").attr("disabled", false);
    $("#updateURLData").attr("disabled", true);
    $("#getSitemapData").attr("disabled", true);
  } else {
    $("#updateURLData").attr("disabled", false);
    $("#getSitemapData").attr("disabled", false);
    $("#updateTranslateData").attr("disabled", true);
  }
});

function updateProgress(chunkPerPercent) {
  current_progress = Number((current_progress += chunkPerPercent).toFixed(2));
  if (current_progress > 100) {
    let progress = 100;
    $("#dynamic").css("width", progress + "%").attr("aria-valuenow", progress).text(progress + "% Complete");
    $("#success-alert").addClass("d-block");
    setInterval(() => {
      window.location.reload();
    }, 5000);
  } else if (current_progress === 100) {
    $("#dynamic")
      .css("width", current_progress + "%")
      .attr("aria-valuenow", current_progress)
      .text(current_progress + "% Complete");
    $("#success-alert").addClass("d-block");
    setInterval(() => {
      window.location.reload();
    }, 5000);
  } else {
    $("#dynamic")
      .css("width", current_progress + "%")
      .attr("aria-valuenow", current_progress)
      .text(current_progress + "% Complete");
  }
}

$("#languageIntent").on('change', function () {
  if (!(this.value == "null")) {
    showQA(currentIntent, this.value);
  } else {
    document.getElementById("saveURLData").disabled = true;
    document.getElementById("Answer").readOnly = true;
  }
});

function showQA(intent, language) {
  var QA_Data = []
  if (intentQA.length > 0) {
    QA_Data = intentQA.find(function (intents) {
      return (intents.INPUTS_INTENT === intent) && (intents.LANGUAGETAG === language)
    });
    if (!QA_Data) {
      questionAnswerNotFound();
    } else {
      if (language == "en-US") {
        document.getElementById("saveURLData").disabled = true;
        document.getElementById("Answer").readOnly = true;
      } else {
        if (!QA_Data.ISOVERRIDE) {
          document.getElementById("saveURLData").disabled = false;
          document.getElementById("Answer").readOnly = false;
        } else {
          document.getElementById("saveURLData").disabled = true;
          document.getElementById("Answer").readOnly = true;
        }
      }

      if (QA_Data.QUESTION) {
        $("#Question").html(QA_Data.QUESTION);
      } else {
        $("#Question").html("Question is not available");
      }

      if (QA_Data.ANSWER) {
        $("#Answer").val(QA_Data.ANSWER);
      } else {
        $("#Answer").val("Answer is not available");
      }

      if (QA_Data.ADDITIONALRESOURCES) {
        $("#AddResources").html(QA_Data.ADDITIONALRESOURCES);
      } else {
        $("#AddResources").html("Additional resource is not available");
      }

      if (QA_Data.RESOURCEURL) {
        var ResourcesURL = QA_Data.RESOURCEURL.replace(/and/g, '&')
        if (QA_Data.RESOURCEURL == "undefined") {
          $("#ResourcesURL").html("Resource URL is not available").removeClass("urlColor");
        } else {
          $("#ResourcesURL").html(ResourcesURL).addClass("urlColor");
        }
      } else {
        $("#ResourcesURL").html("Resource URL is not available").removeClass("urlColor");
      }

      if (QA_Data.FOLLOWUPVIDEOURI) {
        if (QA_Data.FOLLOWUPVIDEOURI == "null") {
          $("#VideoURL").html("Video URL is not available").removeClass("urlColor");
        } else {
          $("#VideoURL").html(QA_Data.FOLLOWUPVIDEOURI).addClass("urlColor");
        }
      } else {
        $("#VideoURL").html("Video URL is not available").removeClass("urlColor");
      }

      $('#languageIntent').val(language).attr('selected', 'selected');
    }

  } else {
    questionAnswerNotFound();
  }
}


const questionAnswerNotFound = () => {
  $("#Question").html("Question is not available");
  $("#Answer").val("Answer is not available");
  $("#AddResources").html("Additional resource is not available");
  $("#ResourcesURL").html("Resource URL is not available").removeClass("urlColor");
  $("#VideoURL").html("Video URL is not available").removeClass("urlColor");
}

function showLanguageDropdown(intent) {
  document.getElementById("Answer").readOnly = true;
  let intentLanguages = intentQA.filter(function (intents) {
    return intents.INPUTS_INTENT === intent;
  });
  var language = document.getElementById("languageIntent");
  //language.add(new Option("Select Language", null));
  if (intentLanguages.length > 0) {
    $.each(intentLanguages, function () {
      language.add(new Option(this.LANGUAGENAME, this.LANGUAGETAG));
    });
    $('#languageIntent').val("en-US").attr('selected', 'selected');

  } else {
    language.add(new Option("No Language Found", null));
  }
}

function directResourcesURL() {
  if (($("#ResourcesURL").html()) == "Resource URL is not available") {
    $("#ResourcesURL").html("Resource URL is not available").removeClass("urlColor");
  } else
    var myResourcesURLWindow = window.open($("#ResourcesURL").html(), 'myResourcesURLWindow', 'width=900,height=700,left=550,right=0,top=150,status=yes,menubar=yes');
  myResourcesURLWindow.focus();
}

function directVideoURL() {
  if (($("#VideoURL").html()) == "Video URL is not available") {
    $("#VideoURL").html("Video URL is not available").removeClass("urlColor");
  } else
    var myVideoURLWindow = window.open($("#VideoURL").html(), 'myVideoURLWindow', 'width=900,height=700,left=550,right=0,top=150,status=yes,menubar=yes');
  myVideoURLWindow.focus();
}


$('#urlTable tbody').on('click', 'tr', function () {
  currentIntent = $('#urlTable').DataTable().row(this).data()[0];
  $('#languageIntent').empty();
  showLanguageDropdown(currentIntent);
  showQA(currentIntent, "en-US");
  if ($(this).hasClass('active')) {
    $(this).removeClass('active');
  }
  else {
    $('#urlTable').DataTable().$('tr.active').removeClass('active');
    $(this).addClass('active');
  }
});

$('#saveURLData').click((e) => {
  showFullPageLoader();
  const languageValue = $('#languageIntent').val();
  var languageValueFiltered = translateLanguageVal.find(function (languageDetails) {
    return ((languageDetails.LanguageTag === languageValue));
  });
  const LanguageID = languageValueFiltered.LanguageID;
  var intentFiltered = urlData.find(function (data) {
    return ((data.Inputs_Intent === currentIntent));
  });
  const gqID = intentFiltered.GQLibraryID;
  const answer = $("#Answer").val();

  const question = $("#Question").html();
  const shortDiscription = $("#AddResources").html();
  const ResourceURL = $("#ResourcesURL").html();
  const videoURL = $("#VideoURL").html();
  if (answer) {
    $.ajax({
      url: '/updateTranslatedData',
      type: 'POST',
      data: { LanguageID, gqID, answer, question, shortDiscription, ResourceURL, videoURL },
      success(result) {
        if (result.code == 200) {
          getURLList();
        }
      },
      error(err) {
        hideFullPageLoader();
        showToaster("error", err.responseJSON.message, 5000);
      }
    });
  } else {
    showToaster("error", "Answer text box is empty", 5000);
  }
});

function alphaOnlyInAnswer(event) {
  var keyCode = event.which ? event.which : event.keyCode;
  if (parseInt(keyCode) == 39 || parseInt(keyCode) == 44 || parseInt(keyCode) == 60 || parseInt(keyCode) == 61 || parseInt(keyCode) == 62) {
    $("#alert-danger").fadeIn(1000);
    $("#alert-danger").fadeOut(5000);
    return false;
  }
}

function alphaOnlyInSearch(event) {
  var keyCode = event.which ? event.which : event.keyCode;
  if (parseInt(keyCode) == 39 || parseInt(keyCode) == 44 || parseInt(keyCode) == 60 || parseInt(keyCode) == 61 || parseInt(keyCode) == 62)
    return false;
}

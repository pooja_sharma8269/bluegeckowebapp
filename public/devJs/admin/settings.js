activeHeaderTab('adminSettings');
var lastDate, currentDate = '';
let dashBoardInputIntent, dashBoardTotalVolume, dashBoardAzure, dashBoardRequest, dashBoardResponse = "";
var found = false;
var myChart;
$(document).ready(() => {
  showDateData();
  getIntentAnalytics();
});

function getCurrentDate() {
  var currentDate = new Date();
  var dd = String(currentDate.getDate()).padStart(2, '0');
  var mm = String(currentDate.getMonth() + 1).padStart(2, '0');
  var yyyy = currentDate.getFullYear();
  currentDate = mm + '/' + dd + '/' + yyyy;
  return currentDate
}

function getLastDate() {
  var today = new Date();
  var lastDate = new Date(new Date().setDate(today.getDate() - 30));
  var dd = String(lastDate.getDate()).padStart(2, '0');
  var mm = String(lastDate.getMonth() + 1).padStart(2, '0');
  var yyyy = lastDate.getFullYear();
  lastDate = mm + '/' + dd + '/' + yyyy;
  return lastDate;
}

function showDateData() {
  $('#id_4').datepicker("destroy");
  $('#id_5').datepicker("destroy");
  lastDate = getLastDate();
  currentDate = getCurrentDate();

  $("#dashboardLastDate").datepicker('setDate', lastDate);
  $("#id_4").datepicker('setEndDate', currentDate);
  let tempLastDate = $('#dashboardLastDate').val();
  if (tempLastDate) {
    $("#id_4").on("changeDate", function () {
      lastDate = $('#dashboardLastDate').val();
      $("#id_5").datepicker('setStartDate', lastDate);
      getIntentAnalytics();
      $('.datepicker').hide();
    });
  }

  $("#dashboardCurrentDate").datepicker('setDate', currentDate);
  $("#id_5").datepicker('setEndDate', currentDate);
  let tempCurrentDate = $('#dashboardCurrentDate').val();
  if (tempCurrentDate) {
    $("#id_5").on("changeDate", function () {
      currentDate = $('#dashboardCurrentDate').val();
      $("#id_4").datepicker('setEndDate', currentDate);
      if ((Date.parse(currentDate) < Date.parse(lastDate))) {
        currentDate = getCurrentDate();
        showFullPageLoader();
        $("#id_5").datepicker('setDate', currentDate);
        $("#date-alert").html('Please select from date first...');
        $("#date-alert").addClass("d-block");
        setTimeout(function () {
          $("#date-alert").removeClass("d-block");
          hideFullPageLoader();
        }, 2000);
      } else {
        getIntentAnalytics();
      }
      $('.datepicker').hide();
    });
  }
}

//Tooltip
$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});

$('#id_4,#id_5').datepicker({
  dateFormat: 'mm-dd-yy',
  changeMonth: true,
  fautoclose: true,
  todayHighlight: true,
});

function getIntentAnalytics() {
  showFullPageLoader();
  $.ajax({
    url: '/intentAnalytics',
    type: 'GET',
    data: { lastDate: lastDate, currentDate: currentDate ,type : 2},
    success(result) {
      if (result.code == 200) {
        showIntentAnalytics(result.data[0]);
        showIntentAnalyticsDashBoard(result.rankingData);
        formBarChart(result.data[1])
        formLineChart(result.data[2]);
      } else if (result.code == 404) {
        showIntentAnalytics([]);
      }
    },
    error(err) {
      hideFullPageLoader();
      showToaster("error", err.responseJSON.message, 5000);
    }
  });
}

function showIntentAnalytics(intentData) {
  var rows = ``;
  if (intentData.length > 0) {
    $.each(intentData, function () {
      rows += `<tr><td>` + this.IntentName + `</td><td class="text-center">` + this.TotalVolume + `</td><td class="text-center">` + this.TotalAzureSearch + `</td><td class="text-center">` + this.TotalIntentRequest + `</td><td class="text-center">` + this.TotalIntentResponse + ``; //this.SuccessRate,this.Clicks
    });
    $(rows).appendTo("#intentList tbody");
    let userTable = $('#intentList').DataTable({
      paging: true,
      pageLength: 10,
      pagingType: "full_numbers",
      info: false,
      destroy: true,
      bInfo: false,
      lengthChange: false,
      language: {
        paginate: {
          previous: '<span aria-hidden="true">«</span>',
          next: '<span aria-hidden="true">»</span>'
        }
      },
      ordering: true,
      order: [[1, "desc"]],
      dom: '<bottam>p',
      initComplete() {
        $('.dataTables_empty').html(`<div class="ui center emptyBox" id="intentList">
          <i class="font50 unordered list icon ml10 mr10"></i>
          <p>No intents available</p>
        </div>`);
      },
      processing: true,
    });
    $('#searchIntent').on('keyup', function () {
      userTable.search($(this).val()).draw();
    });
    hideFullPageLoader();
  } else {
    $(rows).appendTo("#intentList tbody");
    const userTable = $('#intentList').DataTable({
      paging: false,
      dom: '<bottam>p',
      initComplete() {
        $('.dataTables_empty').html(`<div class="ui center emptyBox" id="intentList">
          <i class="font50 unordered list icon ml10 mr10"></i>
          <p>No intents available</p>
        </div>`);
      },
      processing: true,
    });
    $('#searchIntent').on('keyup', function () {
      userTable.search($(this).val()).draw();
    });
    hideFullPageLoader();
  }
}

function showIntentAnalyticsDashBoard(intentDashboardData) {
  var rows = ``;
  if (intentDashboardData.length > 0) {
    $.each(intentDashboardData.sort(), function () {
       if (this.ChangeInCount == '0') {
          dashBoardChangeInCount = '-'
        } else {
          dashBoardChangeInCount = this.ChangeInCount
        }
        if (this.ChangeInCountPercent == '0') {
          dashBoardChangeInCountPercent = "-"
        } else {
          dashBoardChangeInCountPercent = this.ChangeInCountPercent
        }
        if (this.IntentCountLP == '0') {
          dashBoardIntentCountLP = "-"
        } else {
          dashBoardIntentCountLP = this.IntentCountLP
        }
        if (this.IntentCountTP == '0') {
          dashBoardIntentCountTP = "-"
        } else {
          dashBoardIntentCountTP = this.IntentCountTP
        }
        if (this.TrendIndicator == "Up  ") {
          dashBoardTrendIndicator = `<i class="fas fa-lg fa-arrow-circle-up text-success"></i>`
        } else {
          dashBoardTrendIndicator = `<i class="fas fa-lg fa-arrow-circle-down text-danger"></i>`
        }
        rows += `<tr><td>` + this.inputs_intent + `</td><td class="text-center">` + dashBoardChangeInCount + `</td><td class="text-center">` + dashBoardChangeInCountPercent + `</td><td class="text-center">` + dashBoardIntentCountLP + `</td><td class="text-center">` + dashBoardIntentCountTP + `</td><td class="text-center">`+ dashBoardTrendIndicator +`</td>`;
    });
    $(rows).appendTo("#intentListDashboard tbody");
    let userTable = $('#intentListDashboard').DataTable({
      paging: true,
      pageLength: 10,
      pagingType: "full_numbers",
      info: false,
      destroy: true,
      bInfo: false,
      lengthChange: false,
      ordering: true,
      order: [[1, "desc"]],
      dom: '<bottam>p',
      language: {
        paginate: {
          previous: '<span aria-hidden="true">«</span>',
          next: '<span aria-hidden="true">»</span>'
        }
      },
      initComplete() {
        $('.dataTables_empty').html(`<div class="ui center emptyBox" id="intentListDashboard">
          <i class="font50 unordered list icon ml10 mr10"></i>
          <p>No intents available</p>
        </div>`);
      },
      processing: true,
    });
    $('#searchIntentDashboard').on('keyup', function () {
      userTable.search($(this).val()).draw();
    });
    hideFullPageLoader();
  } else {
    $(rows).appendTo("#intentListDashboard tbody");
    const userTable = $('#intentListDashboard').DataTable({
      destroy: true,
      paging: false,
      dom: '<bottam>p',
      initComplete() {
        $('.dataTables_empty').html(`<div class="ui center emptyBox" id="intentListDashboard">
          <i class="font50 unordered list icon ml10 mr10"></i>
          <p>No intents available</p>
        </div>`);
      },
      processing: true,
    });
    $('#searchIntentDashboard').on('keyup', function () {
      userTable.search($(this).val()).draw();
    });
    hideFullPageLoader();
  }
}

function convertDate(EventDate) {
  function pad(s) { return (s < 10) ? '0' + s : s; }
  var d = new Date(EventDate)
  return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/')
}

function getDataforLineChart(linechartData) {
  var xAxis = []
  var yAxis = []
  linechartData.forEach(function (data) {
    var date = convertDate(data.EventDate)
    xAxis.push(date)
    yAxis.push(data.TotalVolume)
  });
  return { xAxis, yAxis };
}

function getDataforBarChart(barchartData) {
  var xAxisIntent = []
  var yAxisAzure = [];
  var yAxisRequests = [];
  var yAxisResponse = [];
  var yAxisTotalVolume = [];

  barchartData.forEach(function (data) {
    if (data.Azure == '0' && data.Requests == '0' && data.Response == '0' && data.TotalVolume == '0') {
      return false;
    } else {
      xAxisIntent.push(data.inputs_intent);
      yAxisAzure.push(data.Azure);
      yAxisRequests.push(data.Requests);
      yAxisResponse.push(data.Response);
      yAxisTotalVolume.push(data.TotalVolume);
    }

  });
  return { xAxisIntent, yAxisAzure, yAxisRequests, yAxisResponse, yAxisTotalVolume };
}

async function formBarChart(barchartData) {
  var data = await getDataforBarChart(barchartData);
  const ctx = document.getElementById('intentInputChart').getContext('2d');
  const myChart = new Chart(ctx, {
    plugins: [{
      beforeInit: function (chart, options) {
        chart.legend.afterFit = function () {
          this.height = this.height + 10;
        };
      }
    }],
    type: 'bar',
    data: {
      labels: data.xAxisIntent,

      datasets: [
        {
          label: 'Azure',
          barPercentage: 0.8,
          data: data.yAxisAzure,
          backgroundColor: '#3f4e61 ',

        },
        {
          label: 'Request',
          barPercentage: 0.8,
          data: data.yAxisRequests,
          backgroundColor: '#e05d6f'
        },
        {
          label: 'Response',
          barPercentage: 0.8,
          data: data.yAxisResponse,
          backgroundColor: '#17a086'
        },
        {
          label: 'Total Volume',
          barPercentage: 0.8,
          data: data.yAxisTotalVolume,
          backgroundColor: 'rgb(65,105,225)'
        }
      ]
    },
    options: {
      legend: {
        display: true,
        position: "top",
        labels: {
          fontColor: "black",
          fontSize: 14,
          fontFamily: "sans-serif"
        },
        onHover: (event, chartElement) => {
          event.target.style.cursor = chartElement[0] ? 'pointer' : 'default';
        }
      },
      hover: {
        onHover: function (e) {
          var point = this.getElementAtEvent(e);
          if (point.length) e.target.style.cursor = 'pointer';
          else e.target.style.cursor = 'pointer';
        }
      },
      tooltips: {
        mode: 'index',
        intersect: false
      },
      responsive: true,
      scales: {
        xAxes: [{
          gridLines: { display: false },
          stacked: true,
          ticks: {
            fontColor: "black",
            fontSize: 11,
            fontFamily: "sans-serif",
          },
        }],
        yAxes: [{
          gridLines: { display: true },
          stacked: true,
          ticks: {
            beginAtZero: true,
            fontColor: "black",
            fontSize: 11,
            fontFamily: "sans-serif",
            padding: 15,
          },
        }],
      }
    }
  });
}

async function formLineChart(linechartData) {
  var data = await getDataforLineChart(linechartData);
  const ctx = document.getElementById('volumeChart').getContext('2d');
  if (myChart) {
    myChart.destroy();
  }
  myChart = new Chart(ctx, {
    plugins: [{
      beforeInit: function (chart, options) {
        chart.legend.afterFit = function () {
          this.height = this.height + 5;
        };
      }
    }],
    type: 'line',
    data: {
      labels: data.xAxis,
      datasets: [
        {
          label: 'Total Volume',
          data: data.yAxis,
          fill: false,
          backgroundColor: "rgb(65,105,225)",
          borderColor: "rgb(65,105,225)",
          borderWidth: 1
        }]
    },
    options: {
      legend: {
        labels: {
          fontColor: "black",
          fontSize: 14,
          fontFamily: "sans-serif"
        },
        onHover: function (e) {
          e.target.style.cursor = 'pointer';
        }
      },
      hover: {
        onHover: function (e) {
          var point = this.getElementAtEvent(e);
          if (point.length) e.target.style.cursor = 'pointer';
          else e.target.style.cursor = 'pointer';
        }
      },
      scales: {
        tooltips: {
          mode: 'index',
          intersect: false
        },
        xAxes: [{
          gridLines: {
            display: false
          },
          ticks: {
            fontColor: "black",
            fontSize: 12,
            fontFamily: "sans-serif",
            min : 0
          },
        }],
        yAxes: [
          {
            gridLines: {
              display: true,
            },
            ticks: {
              fontColor: "black",
              fontSize: 12,
              fontFamily: "sans-serif",
              min : 0
            },
          }
        ]
      }
    }
  });
}

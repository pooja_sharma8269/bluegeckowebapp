var intentData = [];
var currentIntent, userControlledVal = '';

$(document).ready(() => {
	getIntentListForUser();
});
activeHeaderTab('adminSettings');


$('#fromDate, #toDate').datetimepicker({
	"allowInputToggle": true,
	"showClose": true,
	"showClear": true,
	"showTodayButton": true,
	"format": "MM/DD/YYYY",
});

function getIntentListForUser() {
let intentName = $('#intentName').val();
	$.ajax({
		url: '/getIntentList',
		data: { type: 1 },
		type: 'GET',
		success(result) {
			if (result.code == 200) {
				showFullPageLoader();
				intentData = result.intentList;
				$('#message').val("");
				$('input[type=checkbox]').prop('checked',false);
				$.each(intentData[0], function () {
					if (intentName == this.Inputs_Intent) {
						$('#fromRange').val(moment(this.ValidFromDate).format("MM/DD/YYYY"));
						$('#toRange').val(moment(this.ValidToDate).format("MM/DD/YYYY"));
						currentIntent = this;
						showEventInformation(currentIntent.IntentID[0]);
						if (this.IsStatic) {
							$('#state').val("0").attr('selected', 'selected');
							$("#state").attr("disabled", true);
						} else {
							$('#state').val("0").attr('selected', 'selected');
							$("#state").attr("disabled", false);
						}
						if (this.IsUserControlled) {
							$("#intentStateDashboardCheckBoxValue").css("display", "block");
							$('#userControl').val("1").attr('selected', 'selected');
							$("#userControl").attr("disabled", true);
						} else {
							$("#intentStateDashboardCheckBoxValue").css("display", "none");
							$('#userControl').val("0").attr('selected', 'selected');
							$("#userControl").attr("disabled", true);
						}
						if (this.IsActive) {
							$('#active').val("1").attr('selected', 'selected');
							$("#active").attr("disabled", true);
						} else {
							$('#active').val("0").attr('selected', 'selected');
							$("#active").attr("disabled", true);
						}
						showLanguageList(intentData[1]);
						getMessageResponse(currentIntent.IntentID[0], $('#state').val(), "en-US")
					}
				});
			}
		},
		error(err) {
			hideFullPageLoader();
			showToaster("error", err.message, 5000);
		}
	});
}

function getMessageResponse(IntentID, state, LanguageTag) {
	$.ajax({
		url: '/getMessageResponse',
		type: 'GET',
		data: { IntentID, state, LanguageTag },
		success(result) {
			if (result.code == 200) {
				$('#message').val(result.data[0].RESPONSE);
			} else if ((result.code == 404)) {
				$('#message').val('--');
			}
			hideFullPageLoader();
		},
		error(err) {
			hideFullPageLoader();
			showToaster("error", err.message, 5000);
		}
	});
}

$('input[type="checkbox"]').click(function () {
	var dashboardCheckBoxValue = '';
	var messageResponse = $("#message").val();
	if (messageResponse != "") {
		var messageLength = (messageResponse.lastIndexOf('.') === messageResponse.length - 1) ? messageResponse.length - 1 : messageResponse.length;
		messageResponse = messageResponse.substring(0, messageLength);
		if ($(this).prop("checked") == true) {
			$(this).attr('id', function (index, checkBoxCurrentvalue) {
				let initialValue = '';
				let selectedValues = [];
				if (messageResponse.indexOf('%%%') >= 0) {
					initialValue = messageResponse.substring(0, messageResponse.indexOf('%%%'));
					selectedValues = messageResponse.substring(messageResponse.indexOf('%%%')).split(", ");
					selectedValues.push('%%%' + $(`#${checkBoxCurrentvalue}`).val() + '%%%');
				}
				else {
					initialValue = messageResponse.substring(0);
					selectedValues.push('%%%' + $(`#${checkBoxCurrentvalue}`).val() + '%%%');
				}
				$("#message").val(`${initialValue}${selectedValues.join(', ')}.`);
			})
		}
		else if ($(this).prop("checked") == false) {
			$(this).attr('id', function (index, checkBoxCurrentvalue) {
				dashboardCheckBoxValue = $(`#${checkBoxCurrentvalue}`).val();
				dashboardCheckBoxValue = `%%%${dashboardCheckBoxValue}%%%`;
				if (messageResponse.indexOf('%%%') >= 0) {
					const initialValue = messageResponse.substring(0, messageResponse.indexOf('%%%'));
					const selectedValues = messageResponse.substring(messageResponse.indexOf('%%%')).split(", ");
					selectedValues.splice(selectedValues.indexOf(dashboardCheckBoxValue), 1);
					$("#message").val(`${initialValue}${selectedValues.join(', ')}.`);
					if ($("#message").val() == ".") {
						$('#message').val("");
					}
				}
			})
		}
	} else {
		if ($(this).prop("checked") == true) {
			$(this).attr('id', function (index, checkBoxCurrentvalue) {
				let selectedValues = [];
				selectedValues.push('%%%' + $(`#${checkBoxCurrentvalue}`).val() + '%%%');
				$("#message").val(`${selectedValues.join(', ')}.`);
			})
		}
	}
})

function showLanguageList(languageData) {
	var language = document.getElementById("language");
	language.add(new Option("Select Language", null));
	if (languageData.length > 0) {
		$.each(languageData, function () {
			language.add(new Option(this.LanguageName, this.LanguageTag));
		});
		$("#language").val("en-US").attr("selected", "selected");
	} else {
		language.add(new Option("No Language Found", null));
	}
}

function showEventInformation(IntentID) {
	$.ajax({
		url: '/eventInformation',
		type: 'GET',
		data: { IntentID: IntentID },
		success(result) {
			$('#LastKnownEventState').text(moment(result.data[0].LastKnownResetDateTime).format("MM/DD/YYYY HH:mm A"));
			$('#CurrentEventExpiryDateTime').text(moment(result.data[0].CurrentEventExpiryDateTime).format("MM/DD/YYYY HH:mm A"));
			$('#CurrentDateTime').text(moment(result.data[0].CurrentDateTime).format("MM/DD/YYYY HH:mm A"));
			if (result.data[0].IsWithinPolicy) {
				$('#IsWithinPolicy').text("1");
			} else {
				$('#IsWithinPolicy').text("0");
			}
			//hideFullPageLoader();
		},
		error(err) {
			hideFullPageLoader();
			showToaster("error", err.message, 5000);
		}
	});
}

$("#language").on('change', function () {
	showFullPageLoader();
	if (!(this.value == "null")) {
		getMessageResponse(currentIntent.IntentID[0], $('#state').val(), this.value);
	} else {
		getIntentListForUser();
	}
});

$("#state").on('change', function () {
	showFullPageLoader();
	if (!(this.value == "null")) {
		getMessageResponse(currentIntent.IntentID[0], this.value, $('#language').val());
	} else {
		getIntentListForUser();
	}
});

$('#updateIntent').click((e) => {
	$('#eventMaintenance').submit(false);
	updateIntentInformation();
});

function updateIntentInformation() {
	const intent = currentIntent.IntentID[0];
	const state = $('#state').val();
	const message = $('#message').val();
	const language = $('#language').val();
	const fromValue = moment($('#fromRange').val()).toISOString();
	const toValue = moment($('#toRange').val()).toISOString();
	const data = { intent, state, message, language, fromValue, toValue };

	showFullPageLoader(data);
	$.ajax({
		url: `/updateEventInformation`,
		type: 'POST',
		data,
		cache: false,
		headers: { 'cache-control': 'no-cache' },
		success(result) {
			if (result.code === 200) {
				hideFullPageLoader();
				showToaster("success", result.message,1000);
				$('#language').empty();
				getIntentListForUser();
			} else if (result.code === 404) {
				hideFullPageLoader();
				showToaster("success", result.message, 5000);
				$('#language').empty();
				getIntentListForUser();
			}
		},
		error(err) {
			hideFullPageLoader();
			showToaster("error", err.responseJSON.message, 5000);
			$('#intent').empty();
			$('#language').empty();
			getIntentListForUser();
		},
	});
}

function alphaonly(event) {
	var keyCode = event.which ? event.which : event.keyCode;
	if (parseInt(keyCode) == 39 || parseInt(keyCode) == 44 || parseInt(keyCode) == 60 || parseInt(keyCode) == 61 || parseInt(keyCode) == 62) {
		$("#alert-danger").fadeIn(1000);
		$("#alert-danger").fadeOut(5000);
		return false;
	}
}
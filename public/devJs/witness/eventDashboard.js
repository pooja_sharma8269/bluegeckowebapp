activeHeaderTab('eventDashboard');

$(document).ready(() => {
    getEventDashboardInfo(0);
 });

 function getEventDashboardInfo(type) {
	showFullPageLoader();
	$.ajax({
		url: '/eventDashboard',
		type: 'GET',
		success(result) {
      if(result.code == 200){
        $('#lastKnownReset').text(moment(result.date.CriticalResponseTimer).format("MM/DD/YYYY HH:mm A"));
        showEventListingTable(result.data);
        countDownTimer(result.date);
        updateLastKnownResetStatus(type);
        hideFullPageLoader();
      }else if(result.code == 404){
        $('#lastKnownReset').text('--');
        updateLastKnownResetStatus(type);
        showEventListingTable([]);
        hideFullPageLoader();
      }
		},
		error(err){
			hideFullPageLoader();
			//showToaster("error", err.message, 5000);
		}
	});
}

function showEventListingTable(evnetList) {
    $('#iconColor').addClass('text-success');
    var rows = ``;
    if(evnetList.length > 0){
      $.each(evnetList, function(){
        var blockArray= '';
        for(var i =0; i < this.sliceArray.length; i++){
          if(this.sliceArray[i]){
            blockArray += '<span class="timer-block time-active"></span>';
          }else{
            blockArray += '<span class="timer-block"></span>';
          }
        }
        if(this.LASTKNOWNRESET === '' || this.LASTKNOWNRESET === null){
          this.LASTKNOWNRESET = 0;
        }
        rows += `<tr><td>` + moment(this.TIMESTAMP).format("MM/DD/YYYY HH:mm A") + `</td><td>` + this.PUBLICSCREENTAG + `</td><td>` + this.UTCTIMEZONE +`</td><td class="text-center">` + this.UTCOFFSET + `</td><td class="text-center">117</td><td>` + this.LASTKNOWNRESET +`</td><td class="text-left monthly-timer">`+ blockArray +`</td></tr>`;
    });
    $(rows).appendTo( "#eventList tbody" );
    const userTable = $('#eventList').DataTable({
        paging:   true,
        pageLength : 15,
        pagingType: "full_numbers",
        info:     false,
        destroy: true,
        bInfo: false,
        lengthChange: false,
        language: {
          paginate: {
            previous: '<span aria-hidden="true">«</span>',
            next: '<span aria-hidden="true">»</span>'
          }
        },
        ordering: true,
        dom: '<bottam>p',
        initComplete() {
          $('.dataTables_empty').html(`<div class="ui center emptyBox" id="eventList">
            <i class="font50 unordered list icon ml10 mr10"></i>
            <p>No event available</p>
          </div>`);
        },
        processing: true,
      });
      $('#searchEvent').on('keyup', function () {
        userTable.search($(this).val()).draw();
      });
    }else{
      $(rows).appendTo( "#eventList tbody" );
    const userTable = $('#eventList').DataTable({
      paging:  false,
        dom: '<bottam>p',
        initComplete() {
          $('.dataTables_empty').html(`<div class="ui center emptyBox" id="eventList"><i class="font50 unordered list icon ml10 mr10"></i>
            <p>No event available</p>
          </div>`);
        },
        processing: true,
      });
      $('#searchEvent').on('keyup', function () {
        userTable.search($(this).val()).draw();
      });
    }
}

function updateLastKnownResetStatus(type) {
    $.ajax({
      url: '/getLastKnownReset',
      type: 'GET',
      data: { type : type},
      success(result) {
          if(type == 1){
              location.reload(true);
          }
      },
      error(err){
        $('#lastKnownReset').text('--');
        showToaster("error", err.responseJSON.message, 5000);
      }
    });
}

$('#resetLastKnown').click((e) => {
  getEventDashboardInfo(1)
});

function countDownTimer(timerDate){

  var dateLimit = new Date(moment(timerDate.CriticalResponseTimer)  );
  dateLimit.setDate(dateLimit.getDate() + Math.floor(timerDate.PolicyHours/24));
  var compareDate = dateLimit.getTime();

  // Update the count down every 1 second
  var countdownfunction = setInterval(function() {

    // Get todays date and time
    var countDownDate = new Date().getTime();

    // Find the difference between now an the count down date
    var difference = compareDate - countDownDate;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(difference / (1000 * 60 * 60 * 24));
    var hours = Math.floor((difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((difference % (1000 * 60)) / 1000);

    if (difference > (1000 * 60 * 60 * timerDate.YellowThresholdHours)){
      $('#iconColor').removeClass('text-danger');
      $('#iconColor').removeClass('text-warning');
      $('#iconColor').addClass('text-success');

      // Output the result in an element with id="demo"
      document.getElementById("demo").innerHTML = days + " Days " + hours + " Hours " + minutes + " Minutes " + seconds + " Seconds ";
    }else if(difference < (1000 * 60 * 60 * timerDate.YellowThresholdHours) && difference > (1000 * 60 * 60 * timerDate.RedThresholdHours)){
      $('#iconColor').removeClass('text-danger');
      $('#iconColor').removeClass('text-success');
      $('#iconColor').addClass('text-warning');

      // Output the result in an element with id="demo"
      document.getElementById("demo").innerHTML = days + " Days " + hours + " Hours " + minutes + " Minutes " + seconds + " Seconds ";

    }else if((1000 * 60 * 60 * timerDate.RedThresholdHours) > difference && difference > 0){
      $('#iconColor').removeClass('text-warning');
      $('#iconColor').removeClass('text-success');
      $('#iconColor').addClass('text-danger');

      // Output the result in an element with id="demo"
      document.getElementById("demo").innerHTML = days + " Days " + hours + " Hours " + minutes + " Minutes " + seconds + " Seconds ";
    }else if(difference <= 0){
      $('#iconColor').removeClass('text-warning');
      $('#iconColor').removeClass('text-success');
      $('#iconColor').addClass('text-danger');
      // Output the result in an element with id="demo"
      document.getElementById("demo").innerHTML = "Exipred!"
    }
  }, 1000);
}
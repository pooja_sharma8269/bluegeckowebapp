var queryInput, resultDiv, accessTokenInput, dynVarCount = 0;

function getFullText(e, t, s) {
    var n = document.getElementById(e.id),
        l = document.getElementById(t.id),
        a = document.getElementById(s.id);
    "none" === n.style.display ? (n.style.display = "inline", a.innerHTML = "Read more", l.style.display = "none") : (n.style.display = "none", a.innerHTML = "Read less", l.style.display = "inline")
}

function getButtonValue(e, t) {
    var s = e;
    createQueryNode(s), s = "OnButtonClick " + s;
    var n = createResponseNode();
    sendText(s).then(function(e) {
        var t;
        try {
            console.log(": ", e), e.result.fulfillment.messages.length > 1 ? e.result.fulfillment.messages[1].hasOwnProperty("replies") ? t = e.result.fulfillment.messages[1].replies.length > 1 ? e : 1 == e.result.fulfillment.messages[0].hasOwnProperty("subtitle") ? e.result.fulfillment.messages[0].subtitle : e.result.fulfillment.messages[0].speech : (e.result.fulfillment.messages.length, t = e) : t = 1 == e.result.fulfillment.messages[0].hasOwnProperty("subtitle") ? e : e.result.fulfillment.messages[0].speech
        } catch (e) {
            t = ""
        }
        setResponseJSON(e), setResponseOnNodeCardClick(t, n)
    }).catch(function(e) {
        setResponseJSON(e), setResponseOnNodeCardClick("Something goes wrong", n)
    })
    document.getElementById("result").scrollTop = 1000000;
}

function createQueryNode(e) {
    var t = document.createElement("div");
    t.className = "user-msg", resultDiv.appendChild(t);
    var s = document.createElement("img");
    s.setAttribute("src", "../images/chatbot/userIcon.png"), s.className = "user-img", t.appendChild(s);
    var n = document.createElement("div");
    n.className = "clearfix right card-panel", n.innerHTML = e, t.appendChild(n)
}

function createResponseNode() {
    var e = document.createElement("div");
    e.className = "bot-msg", resultDiv.appendChild(e);
    var t = document.createElement("img");
    t.className = "bot-img", t.setAttribute("src", "../images/chatbot/botIcon.png"), e.appendChild(t);
    var s = document.createElement("div");
    return s.className = "clearfix left card-panel", s.innerHTML = "...", e.appendChild(s), s
}

function setResponseOnNodeCardClick(e, t) {
    var s = "",
        n = "",
        l = "dotSpan" + dynVarCount,
        a = "moreSpan" + dynVarCount,
        i = "btnReadMore" + dynVarCount;
    if (dynVarCount++, e.hasOwnProperty("result"))
        if (1 == e.result.fulfillment.messages.length) {
            var r = "",
                o = "";
            r = "", r += "<div class='card-1'>", 1 == (C = (T = e.result.fulfillment.messages[0]).hasOwnProperty("buttons")) ? (r += '<iframe  class="card-img" src=https://www.youtube.com/embed/' + getId(T.buttons[0].postback) + "></iframe>", r += "<span class=card-msg>" + (f = (k = T.subtitle).length > 58 ? k.substring(0, 58) : k) + " <span id=" + l + ">...</span> <span id=" + a + " style=display:none>" + (n = k.substring(145, k.length)) + "</span><button class=btn-link onclick=getFullText(" + l + "," + a + "," + i + ") id=" + i + ">Read more</button> </span>", t.innerHTML = r, t.className = "clearfix left card-panel card-1", t.setAttribute("data-actual-response", f)) : (f = (k = T.subtitle).length > 58 ? k.substring(0, 58) : k, n = k.substring(145, k.length), r += "<span class=card-msg>" + (f = k.substring(0, 145)) + " <span id=" + l + ">...</span> <span id=" + a + " style=display:none>" + (n = k.substring(145, k.length)) + "</span><button class=btn-link onclick=getFullText(" + l + "," + a + "," + i + ") id=" + i + ">Read more</button> </span>", t.innerHTML = r, t.className = "clearfix left card-panel card-1", t.setAttribute("data-actual-response", f))
        } else if (e.result.fulfillment.messages[1].hasOwnProperty("replies"))
        if ((u = e.result.fulfillment.messages[1].replies.length) > 1) {
            var u = e.result.fulfillment.messages[1].replies.length,
                c = "",
                d = "",
                p = e.result.fulfillment.messages[1].replies,
                m = 0,
                g = (r = "", "Suggestion");
            for (m = 0; m < u; m++) {
                var b = p[m];
                console.log("ISQUESTION CHECK : ", p[m].includes("IsQuestion"), d, b.includes("IsQuestion")), b.includes("IsQuestion") ? (c = (d = b.split("IsQuestion"))[0], d = d[1]) : (c = p[m], d = p[m]), r += '<input type="button" id=btnSuggestion' + m + ' value="' + c + '"   class="btn" onclick="getButtonValue(\'' + d + "','" + g + "')\"/>"
            }
            t.innerHTML = "<p>" + e.result.fulfillment.messages[0].speech + "</p>" + r
        } else e.length > 145 ? (s = e.substring(0, 145), n = e.substring(145, e.length), t.innerHTML = "<p>" + s + "<span id=" + l + ">...</span><span id=" + a + " style=display:none>" + n + "</span></p><button class=btn-link onclick=getFullText(" + l + "," + a + "," + i + ") id=" + i + ">Read more</button>", t.setAttribute("data-actual-response", s)) : (t.innerHTML = "<p>" + e + "</p>", t.setAttribute("data-actual-response", e));
    else {
        o = "", r = "";
        var f, v = e.result.fulfillment.messages.length;
        console.log("INSIDE OF !CHECKREPLIES ", v);
        for (var h = 0; h < v; h++) {
            e.result.fulfillment.messages[h].type;
            var y = "btnSelect" + h;
            l = "dotSpan" + dynVarCount, a = "moreSpan" + dynVarCount, i = "btnReadMore" + dynVarCount, dynVarCount++;
            var T, w = (T = e.result.fulfillment.messages[h]).title,
                k = T.subtitle,
                I = T.imageUrl,
                C = T.hasOwnProperty("buttons");
            if (g = "Select", f = k.length > 58 ? k.substring(0, 58) + "..." : k, r += "<div class='card-1'>", r += 1 == C ? '<iframe  class="card-img" src=https://www.youtube.com/embed/' + getId(T.buttons[0].postback) + "></iframe>" : "<img src=" + I + ' class="card-img">', 1 == v ? r += "<span class=card-msg>" + (f = k.substring(0, 145)) + " <span id=" + l + ">...</span> <span id=" + a + " style=display:none>" + (n = k.substring(145, k.length)) + "</span><button class=btn-link onclick=getFullText(" + l + "," + a + "," + i + ") id=" + i + ">Read more</button> </span>" : (s = w.length > 28 ? w.substring(0, 28) + "..." : w, r += '<h2 id=orgTitle class="tooltip"  title="' + w + '"> ' + s + "</h2>", null == k && "" == k || (r += '<p id=orgSubTitle class="tooltip"  title="' + k + '" >' + f + "</p>"), r += "<button id=" + y + ' value="' + g + '"   class="sm btn" onclick="getButtonValue(\'' + w + "','" + g + "')\">" + g + " </button>", r += "</div>"), h == v - 1 && 1 != v) {
                var S = o + "<div class='myc-conversation-bubble myc-text-response myc-is-active myc-text-response card-slider'><div class='carousel'>";
                S += r, t.innerHTML = S + "</div></div></div>", t.className = "clearfix left", $(".carousel").not(".slick-initialized").slick({
                    infinite: !1,
                    autoplay: !1,
                    adaptiveHeight: !0,
                    prevArrow: "<img class='slick-prev' src='../images/chatbot/arrow-prev.svg'>",
                    nextArrow: "<img class='slick-next' src='../images/chatbot/arrow-next.svg'>"
                }), $(".tooltip").tipTop()
            } else 1 == v && (t.innerHTML = r, t.className = "clearfix left card-panel card-1", t.setAttribute("data-actual-response", f))
        }
    } else e.length > 145 ? (s = e.substring(0, 145), n = e.substring(145, e.length), t.innerHTML = "<p>" + s + "<span id=" + l + ">...</span><span id=" + a + " style=display:none>" + n + "</span></p><button class=btn-link onclick=getFullText(" + l + "," + a + "," + i + ") id=" + i + ">Read more</button>", t.setAttribute("data-actual-response", s)) : (t.innerHTML = "<p>" + e + "</p>", t.setAttribute("data-actual-response", e))
    document.getElementById("result").scrollTop = 1000000;
}

function setResponseJSON(e) {}

function formCardResponseForSubMenu(e) {
    var t = e.result.fulfillment.messages[1].replies.length,
        s = "",
        n = "",
        l = e.result.fulfillment.messages[1].replies,
        a = "";
    for (i = 0; i < t; i++) {
        var r = l[0];
        console.log("ISQUESTION CHECK : ", l[0].includes("IsQuestion"), n, r.includes("IsQuestion")), r.includes("IsQuestion") ? (s = (n = r.split("IsQuestion"))[0], n = n[1]) : (s = l[0], n = l[0]), a += '<input type="button" id=btnSuggestion0 value="' + s + '"   class="btn" onclick="getButtonValue(\'' + n + "','Suggestion')\"/>"
    }
    node.innerHTML = "<p>Below response</p>" + a, node.setAttribute("data-actual-response", a);
    document.getElementById("result").scrollTop = 1000000;
}

function getId(e) {
    var t = e.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/);
    return t && 11 == t[2].length ? t[2] : "error"
}

function openDownloadDocument(e) {
    window.open(e, "myWindow", "width=900,height=700,left=550,right=0,top=150,status=yes,menubar=yes").focus()
}! function() {
    "use strict";
    var e = 13;

    function t(t) {
        if (t.which === e) {
            var a = queryInput.value;
            queryInput.value = "",
                function(e) {
                    var t = document.createElement("div");
                    t.className = "user-msg", resultDiv.appendChild(t);
                    var s = document.createElement("img");
                    s.setAttribute("src", "../images/chatbot/userIcon.png"), s.className = "user-img", t.appendChild(s);
                    var n = document.createElement("div");
                    n.className = "clearfix right card-panel", n.innerHTML = e, t.appendChild(n)
                }(a);
            var i = s();
            sendText(a).then(function(e) {
                var t;
                try {
                    t = e
                } catch (e) {
                    t = ""
                }
                l(e), n(t, i)
            }).catch(function(e) {
                l(e), n("Something goes wrong", i)
            })
        }
    }

    function s() {
        var e = document.createElement("div");
        e.className = "bot-msg", resultDiv.appendChild(e);
        var t = document.createElement("img");
        t.className = "bot-img", t.setAttribute("src", "../images/chatbot/botIcon.png"), e.appendChild(t);
        var s = document.createElement("div");
        return s.className = "clearfix left card-panel", s.innerHTML = "...", e.appendChild(s), s
    }

    function n(e, t) {
        console.log("Booklet Request : ", e, e.result.hasOwnProperty("action"));
        var s, n, l, a, i = "",
            r = "";
        if ("BOOKLET_REQUEST" == e.result.action) {
            a = e.result.fulfillment.messages.length, console.log("BOOKLET_REQUEST : ", a, e);
            for (var o = 0; o < a; o++) {
                var u = e.result.fulfillment.messages[o].type,
                    c = "btnSelect" + o,
                    d = "dotSpan" + dynVarCount,
                    p = "moreSpan" + dynVarCount,
                    m = "btnReadMore" + dynVarCount;
                dynVarCount++;
                var g = (w = e.result.fulfillment.messages[o]).title,
                    b = w.subtitle,
                    f = w.imageUrl,
                    v = w.buttons[0].postback,
                    h = w.hasOwnProperty("buttons"),
                    y = "Download";
                if (0 == u && ((i += e.result.fulfillment.messages[o].speech).length > 145 ? (s = i.substring(0, 145), l = i.substring(145, i.length), t.innerHTML = "<p>" + s + "<span id=" + d + ">...</span><span id=" + p + " style=display:none>" + l + "</span></p><button class=btn-link onclick=getFullText(" + d + "," + p + "," + m + ") id=" + m + ">Read more</button>", t.setAttribute("data-actual-response", s)) : (t.innerHTML = "<p>" + i + "</p>", t.setAttribute("data-actual-response", i))), 1 == u)
                    if (r += "<div class='card-1'>", 1 == h && (r += "<img src=" + f + ' class="card-img">'), 1 == a ? "BOOKLET_REQUEST" == e.result.action ? (s = g.length > 28 ? g.substring(0, 28) + "..." : g, r += '<h2 id=orgTitle class="tooltip"  title="' + g + '"> ' + s + "</h2>", r += '<p id=orgSubTitle class="tooltip"  title="' + b + '" >' + b + "</p>", r += "<button id=" + c + ' value="' + y + '"   class="sm btn" onclick="openDownloadDocument(\'' + v + "')\">" + y + " </button>", r += "</div>") : r += "<span class=card-msg>" + (n = b.substring(0, 145)) + " <span id=" + d + ">...</span> <span id=" + p + " style=display:none>" + (l = b.substring(145, b.length)) + "</span><button class=btn-link onclick=getFullText(" + d + "," + p + "," + m + ") id=" + m + ">Read more</button> </span>" : (s = g.length > 28 ? g.substring(0, 28) + "..." : g, r += '<h2 id=orgTitle class="tooltip"  title="' + g + '"> ' + s + "</h2>", r += '<p id=orgSubTitle class="tooltip"  title="' + b + '" >' + b + "</p>", r += "<button id=" + c + ' value="' + y + '"   class="sm btn" onclick="openDownloadDocument(\'' + v + "')\">" + y + " </button>", r += "</div>"), o == a - 1 && 1 != a) {
                        var T = i + "<div class='myc-conversation-bubble myc-text-response myc-is-active myc-text-response card-slider'><div class='carousel'>";
                        T += r, t.innerHTML = T + "</div></div></div>", t.className = "clearfix left", $(".carousel").not(".slick-initialized").slick({
                            infinite: !1,
                            autoplay: !1,
                            adaptiveHeight: !0,
                            prevArrow: "<img class='slick-prev' src='../images/chatbot/arrow-prev.svg'>",
                            nextArrow: "<img class='slick-next' src='../images/chatbot/arrow-next.svg'>"
                        }), $(".tooltip").tipTop()
                    } else 1 == a && (t.innerHTML = r, t.className = "clearfix left card-panel card-1", t.setAttribute("data-actual-response", n))
            }
        } else
            for (a = e.result.fulfillment.messages.length, o = 0; o < a; o++) {
                if (u = e.result.fulfillment.messages[o].type, c = "btnSelect" + o, d = "dotSpan" + dynVarCount, p = "moreSpan" + dynVarCount, m = "btnReadMore" + dynVarCount, dynVarCount++, console.log("msg Type ", o, " : ", u), 0 == u && ((i += e.result.fulfillment.messages[o].speech).length > 145 ? (s = i.substring(0, 145), l = i.substring(145, i.length), t.innerHTML = "<p>" + s + "<span id=" + d + ">...</span><span id=" + p + " style=display:none>" + l + "</span></p><button class=btn-link onclick=getFullText(" + d + "," + p + "," + m + ") id=" + m + ">Read more</button>", t.setAttribute("data-actual-response", s)) : (t.innerHTML = "<p>" + i + "</p>", t.setAttribute("data-actual-response", i))), 1 == u) {
                    g = (w = e.result.fulfillment.messages[o]).title, b = w.subtitle, f = w.imageUrl;
                    var w, k = w.hasOwnProperty("buttons");
                    y = "Select", n = b.length > 58 ? b.substring(0, 58) + "..." : b, r += "<div class='card-1'>", r += 1 == k ? '<iframe  class="card-img" src=https://www.youtube.com/embed/' + getId(w.buttons[0].postback) + "></iframe>" : "<img src=" + f + ' class="card-img">', 1 == a ? r += "<span class=card-msg>" + (n = b.substring(0, 145)) + " <span id=" + d + ">...</span> <span id=" + p + " style=display:none>" + (l = b.substring(145, b.length)) + "</span><button class=btn-link onclick=getFullText(" + d + "," + p + "," + m + ") id=" + m + ">Read more</button> </span>" : (s = g.length > 28 ? g.substring(0, 28) + "..." : g, r += '<h2 id=orgTitle class="tooltip"  title="' + g + '"> ' + s + "</h2>", null == b && "" == b || (r += '<p id=orgSubTitle class="tooltip"  title="' + b + '" >' + n + "</p>"), r += "<button id=" + c + ' value="' + y + '"   class="sm btn" onclick="getButtonValue(\'' + g + "','" + y + "')\">" + y + " </button>", r += "</div>"), o == a - 1 && 1 != a ? (T = i + "<div class='myc-conversation-bubble myc-text-response myc-is-active myc-text-response card-slider'><div class='carousel'>", T += r, t.innerHTML = T + "</div></div></div>", t.className = "clearfix left", console.log("Final msg :", T), $(".carousel").not(".slick-initialized").slick({
                        infinite: !1,
                        autoplay: !1,
                        adaptiveHeight: !0,
                        prevArrow: "<img class='slick-prev' src='../images/chatbot/arrow-prev.svg'>",
                        nextArrow: "<img class='slick-next' src='../images/chatbot/arrow-next.svg'>"
                    }), $(".tooltip").tipTop()) : 1 == a && (t.innerHTML = r, t.className = "clearfix left card-panel card-1", t.setAttribute("data-actual-response", n))
                }
                if (2 == u) {
                    for (var I = 0, C = (r = "", e.result.fulfillment.messages[o].replies), S = (y = "Suggestion", ""), E = ""; I < C.length; I++) {
                        var x = C[I];
                        console.log("ISQUESTION CHECK : ", C[I].includes("IsQuestion"), S, x.includes("IsQuestion")), x.includes("IsQuestion") ? (E = (S = x.split("IsQuestion"))[0], S = S[1]) : (E = C[I], S = C[I]), r += '<input type="button" id=btnSuggestion' + I + ' value="' + E + '"   class="btn" onclick="getButtonValue(\'' + S + "','" + y + "')\"/>"
                    }
                    t.innerHTML = "<p>" + i + "</p>" + r
                }
            }
            document.getElementById("result").scrollTop = 1000000;
    }

    function l(e) {
        document.getElementById("jsonResponse").innerHTML = JSON.stringify(e, null, 2)
    }
    window.onload = function() {
        var e;
        document.getElementById("main-wrapper").style.display = "block", accessTokenInput = "75185ce7fb5645f88d60f89b4db85d4a", queryInput = document.getElementById("userInput"), resultDiv = document.getElementById("result"), queryInput.addEventListener("keydown", t), window.init(accessTokenInput), e = s(), sendText("1 Hi").then(function(t) {
            l(t), n(t, e)
        }).catch(function(t) {
            l(t), n("Something goes wrong", e)
        })
    }
}();
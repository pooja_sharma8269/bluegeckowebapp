$('.carousel').slick({
        infinite: false,
        autoplay: true,
        adaptiveHeight: true,
        prevArrow:"<img class='slick-prev' src='../images/chatbot/arrow-prev.svg'>",
        nextArrow:"<img class='slick-next' src='../images/chatbot/arrow-next.svg'>"
    }); 
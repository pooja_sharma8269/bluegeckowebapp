/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var queryInput, resultDiv, accessTokenInput,dynVarCount=0;
(function() {
  "use strict";

  var ENTER_KEY_CODE = 13,cntGreet=1;    

  window.onload = init;

  function init() {
    document.getElementById("main-wrapper").style.display = "block";
    accessTokenInput="75185ce7fb5645f88d60f89b4db85d4a"; //Set your access token over here... 
    queryInput = document.getElementById("userInput");
    resultDiv = document.getElementById("result");
    queryInput.addEventListener("keydown", queryInputKeyDown);    
    window.init(accessTokenInput);
   // if (event.which !== ENTER_KEY_CODE) {      
      setProactiveWelcomeMessage();
   // }

    
  }

  function queryInputKeyDown(event) {
    if (event.which !== ENTER_KEY_CODE) {      
      return;
    }

    var value = queryInput.value;
    queryInput.value = "";

    createQueryNode(value);
    var responseNode = createResponseNode();

    sendText(value)
      .then(function(response) {
        var result;
        try {
          console.log();
          
          //result = response.result.fulfillment.speech
          result = response;
          console.log("RES in InputKey : ",result,value);
          
        } catch(error) {
          result = "";
        }
        setResponseJSON(response);
        setResponseOnNode(result, responseNode);
      })
      .catch(function(err) {
        setResponseJSON(err);
        setResponseOnNode("Something goes wrong", responseNode);
      });
  }

  function createQueryNode(query) {
    var divQuery = document.createElement('div');
    divQuery.className = "user-msg";   
    resultDiv.appendChild(divQuery);

    var Img = document.createElement("img");
    Img.setAttribute('src', 'http://flowcore.io/wp-content/plugins/flowcore-chatbot/assets/images/mask.png');
    Img.className = "user-img";
    divQuery.appendChild(Img);


    var node = document.createElement('div');
    node.className = "clearfix right card-panel";
    node.innerHTML = query;
    divQuery.appendChild(node);
  }

  function createResponseNode() {    

    var divRes = document.createElement('div');
    divRes.className = "bot-msg";   
    resultDiv.appendChild(divRes);
    
    var Img = document.createElement("img");
    Img.className = "bot-img";
    Img.setAttribute('src', 'http://flowcore.io/wp-content/plugins/flowcore-chatbot/assets/images/request.png');   
    divRes.appendChild(Img);

    var node = document.createElement('div');
    //node.className = "myc-conversation-bubble-container myc-conversation-bubble-container-response xyz";
    node.className = "clearfix left card-panel";
    node.innerHTML = "...";
   

    divRes.appendChild(node);
    return node;
  }

  function setResponseOnNode(response, node) 
  {
        // if(response.result.action=="input.welcome")
        // {
        //       setProactiveWelcomeMessage();
        //       return ;
        // }
        // else
        // {
              var finalRespose="",html="",compTitle,compSubtitle,textAftComp;        
              console.log('Hi response : ',response);
              var msgLength= response.result.fulfillment.messages.length;
              //console.log("msgLength : ",msgLength);
              
              for(var i=0;i<msgLength;i++)
              {      
                  var msgType=response.result.fulfillment.messages[i].type,btnID="btnSelect"+i,dotSpan="dotSpan"+dynVarCount,moreSpan="moreSpan"+dynVarCount,btnReadMore="btnReadMore"+dynVarCount;
                  dynVarCount++;
                  //msgType=3;
                  console.log("msg Type ",i," : ",msgType);
                  
                  //console.log(i ," : ",msgType);
                  if(msgType==0)//text only....
                  {
                      //console.log("1 : ",response.result.fulfillment.messages[i].speech);
                    
                      finalRespose += response.result.fulfillment.messages[i].speech;
                      console.log("lenght : ",finalRespose.length);
                      if(finalRespose.length>145)
                      {
                          compTitle = finalRespose.substring(0,145);
                          textAftComp = finalRespose.substring(145,finalRespose.length);
                          //node.innerHTML="<p>"+compTitle+" <a id='"+btnID+"RM"+"' href=\"javascript:\" class=read_more onclick=\"getFullText('"+btnID+"')\" style=color:white;font-weight:700;>Read More</a><div id="+btnID+"FT"+" style=display:none;>"+textAftComp+"</div></p>";
                          node.innerHTML="<p>"+compTitle+"<span id="+dotSpan+">...</span><span id="+moreSpan+" style=display:none>"+textAftComp+"</span></p><button class=btn-link onclick=getFullText("+dotSpan+","+moreSpan+","+btnReadMore+") id="+btnReadMore+">Read more</button>";
                        // node.innerHTML="<p>"+compTitle+"<span id=dots>...</span><span id=more>"+textAftComp+"</span></p><button onclick=getFullText() id=myBtn>Read more</button>";
                          node.setAttribute('data-actual-response',compTitle);                              
                      }
                      else
                      {
                          node.innerHTML="<p>"+finalRespose+"</p>";
                          node.setAttribute('data-actual-response',finalRespose);                               
                      }                 
                      
                  }
                  if(msgType==1)//For list object type....Card 
                  { 
                        //console.log("imgURL,webURL,vidURL : ", i,response.result.fulfillment.messages[i].buttons[i].postback);   
                        //Start of for multiple card response...
                        var cardArray = response.result.fulfillment.messages[i];
                        var orgTitle = cardArray.title;              
                        var orgSubtitle = cardArray.subtitle;              
                        var imgURL=cardArray.imageUrl;
                        var isVideoUrl=cardArray.hasOwnProperty('buttons');                
                        console.log("vidURL : ",i,isVideoUrl,imgURL);                  
                        var cardButton="Select";
                        if(orgSubtitle.length > 58) 
                        {
                            compSubtitle = orgSubtitle.substring(0,58)+"...";
                        }
                        else
                        {
                            compSubtitle = orgSubtitle;
                        }
                        html += "<div class='card-1'>";
                        if(isVideoUrl==true)
                        {                    
                            var youtubeURL = "https://www.youtube.com/embed/"+getId(cardArray.buttons[0].postback);    
                            console.log("youtubeURL Video Present : ",youtubeURL);                                 
                            html +="<iframe  class=\"card-img\" src="+youtubeURL+"></iframe>";
                            
                        }
                        else
                        {
                            html += "<img src="+imgURL+" class=\"card-img\">";
                        }
      
                        if(msgLength==1)//Response is Video response....
                        {
                              compSubtitle = orgSubtitle.substring(0,145);
                              textAftComp = orgSubtitle.substring(145,orgSubtitle.length); 
                              html += "<span class=card-msg>"+compSubtitle+" <span id="+dotSpan+">...</span> <span id="+moreSpan+" style=display:none>"+textAftComp+"</span><button class=btn-link onclick=getFullText("+dotSpan+","+moreSpan+","+btnReadMore+") id="+btnReadMore+">Read more</button> </span>";                        
                            // html += "</div>"; 
                        }
                        else // Multi select functionality....
                        {
                              if(orgTitle.length > 28) 
                              {
                                  compTitle = orgTitle.substring(0,28)+"...";
                              }
                              else
                              {
                                  compTitle = orgTitle;
                              }
                              console.log('subtitle:',orgSubtitle);
                              
                              html += "<h2 id=orgTitle class=\"tooltip\"  title=\""+orgTitle+"\"> "+compTitle+"</h2>";
                              if(orgSubtitle !=null || orgSubtitle!="")
                              {
                                  html += "<p id=orgSubTitle class=\"tooltip\"  title=\""+orgSubtitle+"\" >"+compSubtitle+"</p>";  
                              }                        
                              html += "<button id="+btnID+" value=\"" + cardButton + "\"   class=\"sm btn\" onclick=\"getButtonValue('"+orgTitle+"')\">"+cardButton+" </button>";
                              html += "</div>"; 
                        }
                      
                        if(i==(msgLength-1) && msgLength!=1) //Multiselect
                        {
                            var htmlcard=finalRespose+"<div class='myc-conversation-bubble myc-text-response myc-is-active myc-text-response card-slider'><div class='carousel'>";
                            htmlcard+=html;
                            node.innerHTML=htmlcard+"</div></div></div>";
                            node.className="clearfix left";
      
                            console.log("Final msg :",htmlcard);
                            
                            //Start of slider and tooltip initialzed in expand collapse function
                            //$('.carousel').slick({
                            $('.carousel').not('.slick-initialized').slick({
                                infinite: false,
                                autoplay: false,
                                adaptiveHeight: true,
                                prevArrow:"<img class='slick-prev' src='../images/chatbot/arrow-prev.svg'>",
                                nextArrow:"<img class='slick-next' src='../images/chatbot/arrow-next.svg'>"
                            });
      
                            // tooltip
                            $('.tooltip').tipTop();
                            //End of slider and tooltip initialization                  
                        }   
                        else if(msgLength==1) //Video card with response....
                        {
                              node.innerHTML=html;
                              node.className="clearfix left card-panel card-1";
                              node.setAttribute('data-actual-response',compSubtitle);
                        }          
                  }
                  if(msgType==2)//Suggestions or replies
                  {
                      var index = 0,html="",replies=response.result.fulfillment.messages[i].replies;
                      
                      for (index; index<replies.length; index++) 
                      {
                          html += "<input type=\"button\" id=btnSuggestion"+index+" value=\"" + replies[index] + "\"   class=\"btn\" onclick=\"getButtonValue('"+replies[index]+"')\"/>";                    
                      }
                      console.log("html : ",html);                
                      //finalRespose += html;
                    
                      node.innerHTML="<p>"+finalRespose+"</p>"+html;
                  }                  
              }
       // }
  }

  function setResponseJSON(response) 
  {    
      var node = document.getElementById("jsonResponse");
      //node.innerHTML = JSON.stringify(response, null, 2);
  }
  function setProactiveWelcomeMessage() 
  {
        var value="1 Hi",channelNUserId="1 Hi";//channelNUserId="JavascriptSnippet and 1  Hi"      
        var responseNode = createResponseNode();

        // const client = new ApiAi.ApiAiClient({accessToken: 'YOUR_ACCESS_TOKEN'});
        // const promise = client.eventRequest("EVENT_NAME", options);
        console.log("channelNUserId : ",channelNUserId)
        sendText(channelNUserId)
        .then(function(handleResponse){
          //console.log("return from event : ",handleResponse);
          console.log("RES from Proactive: ",handleResponse,channelNUserId);
          setResponseJSON(handleResponse);
           setResponseOnNode(handleResponse, responseNode);
          
        })
        .catch(function(err) {
          // setResponseJSON(err);
          // setResponseOnNode("Something goes wrong", responseNode);
        });


        // sendText(value)
        //   .then(function(response) {
        //     var result;
        //     try {          
        //       result = response
        //     } catch(error) {
        //       result = "";
        //     }
        //     setResponseJSON(response);
        //     setResponseOnNode(result, responseNode);
        //   })
        //   .catch(function(err) {
        //     setResponseJSON(err);
        //     setResponseOnNode("Something goes wrong", responseNode);
        //   });
  }
})();

  //function getFullText()
  function getFullText(dotSpan,moreSpan,btnReadMore )
  {
        var dots = document.getElementById(dotSpan.id);
        var moreText = document.getElementById(moreSpan.id);
        var btnText = document.getElementById(btnReadMore.id);


        console.log("dots : ",dots,dotSpan);

        if (dots.style.display === "none") 
        {
          dots.style.display = "inline";
          btnText.innerHTML = "Read more"; 
          moreText.style.display = "none";
        } 
        else 
        {
          dots.style.display = "none";
          btnText.innerHTML = "Read less"; 
          moreText.style.display = "inline";
        }
  }
  function getButtonValue(title)
{
      console.log("button click : ",title);      
      var selectedVal=title;

      createQueryNode(selectedVal);     
      var responseNode = createResponseNode();

      sendText(selectedVal)
       .then(function(response) {
         var result;
         try {     
              console.log("button click : ",response.result.fulfillment.messages.length,response);     
              var lenOfMsgArr= response.result.fulfillment.messages.length;
                  if(lenOfMsgArr>1)
                  {
                    var checkReplies=response.result.fulfillment.messages[1].hasOwnProperty('replies');
                    if(checkReplies)
                    {
                          console.log("replies check  : ",response.result.fulfillment.messages[1].hasOwnProperty('replies'));           
                          console.log("button res : ",response.result,response.result.fulfillment.messages[1].replies.length,response.result.fulfillment.speech,response.result.fulfillment.messages[0].speech);           
                          var lenOfSubMenu= response.result.fulfillment.messages[1].replies.length;
                          if(lenOfSubMenu>1)
                          {
                                result = response;
                          }
                          else
                          {
                                var isCard=response.result.fulfillment.messages[0].hasOwnProperty('subtitle');  
                                console.log("Video card : ",isCard);
                                if(isCard==true)
                                {
                                    result = response.result.fulfillment.messages[0].subtitle; 
                                }
                                else
                                {
                                    result = response.result.fulfillment.messages[0].speech; 
                                }
                          }
                    }
                    else //check for multiple responses or not.....
                    {
                          //console.log("check for multiple responses",response.result.fulfillment.messages.length);
                          var msgLength=response.result.fulfillment.messages.length;
                          result=response;

                          /*
                          var finalRespose="",html="",compTitle,compSubtitle,textAftComp;
                          for(var i=0;i<msgLength;i++)
                          {
                                    var msgType=response.result.fulfillment.messages[i].type,btnID="btnSelect"+i,dotSpan="dotSpan"+dynVarCount,moreSpan="moreSpan"+dynVarCount,btnReadMore="btnReadMore"+dynVarCount;
                                    dynVarCount++;
                                                  //console.log("imgURL,webURL,vidURL : ", i,response.result.fulfillment.messages[i].buttons[i].postback);   
                                    //Start of for multiple card response...
                                    var cardArray = response.result.fulfillment.messages[i];
                                    var orgTitle = cardArray.title;              
                                    var orgSubtitle = cardArray.subtitle;              
                                    var imgURL=cardArray.imageUrl;
                                    var isVideoUrl=cardArray.hasOwnProperty('buttons');                
                                    console.log("vidURL : ",i,isVideoUrl,imgURL);                  
                                    var cardButton="Select";
                                    if(orgSubtitle.length > 58) 
                                    {
                                        compSubtitle = orgSubtitle.substring(0,58)+"...";
                                    }
                                    else
                                    {
                                        compSubtitle = orgSubtitle;
                                    }
                                    html += "<div class='card-1'>";
                                    if(isVideoUrl==true)
                                    {                    
                                        //html +="<iframe  class=\"card-img\" src="+cardArray.buttons[i].postback+"></iframe>";
                                        html +="<iframe  src=https://www.youtube.com/embed/GJ0GqWPjzMc></iframe>";
                                        
                                    }
                                    else
                                    {
                                        html += "<img src="+imgURL+" class=\"card-img\">";
                                    }

                                    if(msgLength==1)//Response is Video response....
                                    {
                                          compSubtitle = orgSubtitle.substring(0,145);
                                          textAftComp = orgSubtitle.substring(145,orgSubtitle.length); 
                                          html += "<span class=card-msg>"+compSubtitle+" <span id="+dotSpan+">...</span> <span id="+moreSpan+" style=display:none>"+textAftComp+"</span><button class=btn-link onclick=getFullText("+dotSpan+","+moreSpan+","+btnReadMore+") id="+btnReadMore+">Read more</button> </span>";                        
                                        // html += "</div>"; 
                                    }
                                    else // Multi select functionality....
                                    {
                                          if(orgTitle.length > 28) 
                                          {
                                              compTitle = orgTitle.substring(0,28)+"...";
                                          }
                                          else
                                          {
                                              compTitle = orgTitle;
                                          }
                                          console.log('subtitle:',orgSubtitle);
                                          
                                          html += "<h2 id=orgTitle class=\"tooltip\"  title=\""+orgTitle+"\"> "+compTitle+"</h2>";
                                          if(orgSubtitle !=null || orgSubtitle!="")
                                          {
                                              html += "<p id=orgSubTitle class=\"tooltip\"  title=\""+orgSubtitle+"\" >"+compSubtitle+"</p>";  
                                          }                        
                                          html += "<button id="+btnID+" value=\"" + cardButton + "\"   class=\"sm btn\" onclick=\"getButtonValue('"+orgTitle+"')\">"+cardButton+" </button>";
                                          html += "</div>"; 
                                    }
                                  
                                    if(i==(msgLength-1) && msgLength!=1) //Multiselect
                                    {
                                        var htmlcard=finalRespose+"<div class='myc-conversation-bubble myc-text-response myc-is-active myc-text-response card-slider'><div class='carousel'>";
                                        htmlcard+=html;
                                        node.innerHTML=htmlcard+"</div></div></div>";
                                        node.className="clearfix left";

                                        console.log("Final msg :",htmlcard);
                                        
                                        //Start of slider and tooltip initialzed in expand collapse function
                                        //$('.carousel').slick({
                                        $('.carousel').not('.slick-initialized').slick({
                                            infinite: false,
                                            autoplay: false,
                                            adaptiveHeight: true,
                                            prevArrow:"<img class='slick-prev' src='assets/arrow-prev.svg'>",
                                            nextArrow:"<img class='slick-next' src='assets/arrow-next.svg'>"
                                        });

                                        // tooltip
                                        $('.tooltip').tipTop();
                                        //End of slider and tooltip initialization                  
                                    }   
                                    else if(msgLength==1) //Video card with response....
                                    {
                                          node.innerHTML=html;
                                          node.className="clearfix left card-panel card-1";
                                          node.setAttribute('data-actual-response',compSubtitle);
                                    }
                          }
                      */
                          // var isCard=response.result.fulfillment.messages[0].hasOwnProperty('subtitle');  
                          // console.log("check for multiple responses : ",isCard);
                          // if(isCard==true)
                          // {
                          //     result = response.result.fulfillment.messages[0].subtitle; 
                          // }
                          // else
                          // {
                          //     result = response.result.fulfillment.messages[0].speech; 
                          // }
                    }
              }
              else
              {
                     // console.log("Single res : ",response.result,response.result.fulfillment.messages[1].replies.length,response.result.fulfillment.speech,response.result.fulfillment.messages[0].speech);           
                      var isCard=response.result.fulfillment.messages[0].hasOwnProperty('subtitle');  
                      console.log("Video card : ",isCard);
                      console.log("Video card 1: ",response.result.fulfillment.messages[0].subtitle);
                      
                      if(isCard==true)
                      {
                          result = response.result.fulfillment.messages[0].subtitle; 
                      }
                      else
                      {
                          result = response.result.fulfillment.messages[0].speech; 
                      }
              }
           
         } catch(error) {
           result = "";
         }
         //console.log("result : ",result);
         setResponseJSON(response);
         setResponseOnNodeCardClick(result, responseNode);
       })
       .catch(function(err) {
        //console.log("inside catch : ",result);
         setResponseJSON(err);
         setResponseOnNodeCardClick("Something goes wrong", responseNode);
       });      
  }
  function createQueryNode(query) 
  {    
        var divQuery = document.createElement('div');
        divQuery.className = "user-msg";   
        resultDiv.appendChild(divQuery);

        var Img = document.createElement("img");
        Img.setAttribute('src', 'http://flowcore.io/wp-content/plugins/flowcore-chatbot/assets/images/mask.png');
        Img.className = "user-img";
        divQuery.appendChild(Img);

        var node = document.createElement('div');
        node.className = "clearfix right card-panel";
        node.innerHTML = query;
        divQuery.appendChild(node);
  }

  function createResponseNode() 
  {    
        var divRes = document.createElement('div');
        divRes.className = "bot-msg";   
        resultDiv.appendChild(divRes);
        
        var Img = document.createElement("img");
        Img.className = "bot-img";
        Img.setAttribute('src', 'http://flowcore.io/wp-content/plugins/flowcore-chatbot/assets/images/request.png');   
        divRes.appendChild(Img);

        var node = document.createElement('div');
        node.className = "clearfix left card-panel";
        node.innerHTML = "..."; 
        divRes.appendChild(node);
        
        return node;
  }
  function setResponseOnNodeCardClick(response,node) 
  {    
        var compTitle="",textAftComp="";
        var dotSpan="dotSpan"+dynVarCount,moreSpan="moreSpan"+dynVarCount,btnReadMore="btnReadMore"+dynVarCount;
        dynVarCount++;
        console.log(" unknown result 1: ",response,response.hasOwnProperty('result'));       
        if(response.hasOwnProperty('result'))
        {
                var checkReplies=response.result.fulfillment.messages[1].hasOwnProperty('replies');
                if(checkReplies)
                {
                  console.log("Title of BUTTON : ",response);
                  
                        console.log(" response lenght for button : ",response.result.fulfillment.messages.length,response,response.result.fulfillment.messages[1].replies.length);
                        var lenOfSubMenu=response.result.fulfillment.messages[1].replies.length;
                        if(lenOfSubMenu>1)//Submenu section.....
                        {
                            //formCardResponseForSubMenu(response);              
                                console.log("formCardResponseForSubMenu res : ",response);
                                var lenOfSubMenu=response.result.fulfillment.messages[1].replies.length;            
                                var replies=response.result.fulfillment.messages[1].replies,index = 0,html="";               
                                for(index=0;index<lenOfSubMenu;index++)
                                {
                                    html += "<input type=\"button\" id=btnSuggestion"+index+" value=\"" + replies[index] + "\"   class=\"btn\" onclick=\"getButtonValue('"+replies[index]+"')\"/>";                                        
                                }
                                node.innerHTML="<p>"+response.result.fulfillment.messages[0].speech+"</p>"+html;             
                        }
                        else
                        {
                              if(response.length>145)
                              {
                                  console.log(" inside response: ",response.length);
                                  compTitle = response.substring(0,145);
                                  textAftComp = response.substring(145,response.length);            
                                  node.innerHTML="<p>"+compTitle+"<span id="+dotSpan+">...</span><span id="+moreSpan+" style=display:none>"+textAftComp+"</span></p><button class=btn-link onclick=getFullText("+dotSpan+","+moreSpan+","+btnReadMore+") id="+btnReadMore+">Read more</button>";          
                                  node.setAttribute('data-actual-response',compTitle);                              
                              }
                              else
                              {
                                  node.innerHTML="<p>"+response+"</p>";
                                  node.setAttribute('data-actual-response',response);                               
                              }
                        }
                }
                else
                {
                        ///START
                        var finalRespose="",html="",compTitle,compSubtitle,textAftComp;
                        var msgLength= response.result.fulfillment.messages.length;
                        for(var i=0;i<msgLength;i++)
                        {
                                  var msgType=response.result.fulfillment.messages[i].type,btnID="btnSelect"+i,dotSpan="dotSpan"+dynVarCount,moreSpan="moreSpan"+dynVarCount,btnReadMore="btnReadMore"+dynVarCount;
                                  dynVarCount++;
                                                //console.log("imgURL,webURL,vidURL : ", i,response.result.fulfillment.messages[i].buttons[i].postback);   
                                  //Start of for multiple card response...
                                  var cardArray = response.result.fulfillment.messages[i];
                                  var orgTitle = cardArray.title;              
                                  var orgSubtitle = cardArray.subtitle;              
                                  var imgURL=cardArray.imageUrl;
                                  var isVideoUrl=cardArray.hasOwnProperty('buttons');                
                                  console.log("vidURL : ",i,isVideoUrl,imgURL);                  
                                  var cardButton="Select";
                                  if(orgSubtitle.length > 58) 
                                  {
                                      compSubtitle = orgSubtitle.substring(0,58)+"...";
                                  }
                                  else
                                  {
                                      compSubtitle = orgSubtitle;
                                  }
                                  html += "<div class='card-1'>";
                                  if(isVideoUrl==true)
                                  {            
                                    var youtubeURL = "https://www.youtube.com/embed/"+getId(cardArray.buttons[0].postback);    
                            console.log("youtubeURL Video Present : ",youtubeURL);                                 
                            html +="<iframe  class=\"card-img\" src="+youtubeURL+"></iframe>";
                            
                                      
                                  }
                                  else
                                  {
                                      html += "<img src="+imgURL+" class=\"card-img\">";
                                  }

                                  if(msgLength==1)//Response is Video response....
                                  {
                                        compSubtitle = orgSubtitle.substring(0,145);
                                        textAftComp = orgSubtitle.substring(145,orgSubtitle.length); 
                                        html += "<span class=card-msg>"+compSubtitle+" <span id="+dotSpan+">...</span> <span id="+moreSpan+" style=display:none>"+textAftComp+"</span><button class=btn-link onclick=getFullText("+dotSpan+","+moreSpan+","+btnReadMore+") id="+btnReadMore+">Read more</button> </span>";                        
                                      // html += "</div>"; 
                                  }
                                  else // Multi select functionality....
                                  {
                                        if(orgTitle.length > 28) 
                                        {
                                            compTitle = orgTitle.substring(0,28)+"...";
                                        }
                                        else
                                        {
                                            compTitle = orgTitle;
                                        }
                                        console.log('subtitle:',orgSubtitle);
                                        
                                        html += "<h2 id=orgTitle class=\"tooltip\"  title=\""+orgTitle+"\"> "+compTitle+"</h2>";
                                        if(orgSubtitle !=null || orgSubtitle!="")
                                        {
                                            html += "<p id=orgSubTitle class=\"tooltip\"  title=\""+orgSubtitle+"\" >"+compSubtitle+"</p>";  
                                        }                        
                                        html += "<button id="+btnID+" value=\"" + cardButton + "\"   class=\"sm btn\" onclick=\"getButtonValue('"+orgTitle+"')\">"+cardButton+" </button>";
                                        html += "</div>"; 
                                  }
                                
                                  if(i==(msgLength-1) && msgLength!=1) //Multiselect
                                  {
                                      var htmlcard=finalRespose+"<div class='myc-conversation-bubble myc-text-response myc-is-active myc-text-response card-slider'><div class='carousel'>";
                                      htmlcard+=html;
                                      node.innerHTML=htmlcard+"</div></div></div>";
                                      node.className="clearfix left";

                                      console.log("Final msg :",htmlcard);
                                      
                                      //Start of slider and tooltip initialzed in expand collapse function
                                      //$('.carousel').slick({
                                      $('.carousel').not('.slick-initialized').slick({
                                          infinite: false,
                                          autoplay: false,
                                          adaptiveHeight: true,
                                          prevArrow:"<img class='slick-prev' src='../images/chatbot/arrow-prev.svg'>",
                                          nextArrow:"<img class='slick-next' src='../images/chatbot/arrow-next.svg'>"
                                      });

                                      // tooltip
                                      $('.tooltip').tipTop();
                                      //End of slider and tooltip initialization                  
                                  }   
                                  else if(msgLength==1) //Video card with response....
                                  {
                                        node.innerHTML=html;
                                        node.className="clearfix left card-panel card-1";
                                        node.setAttribute('data-actual-response',compSubtitle);
                                  }
                        }
                  ///END
                }
        }
        else
        {
              if(response.length>145)
              {
                  console.log(" inside response: ",response.length);
                  compTitle = response.substring(0,145);
                  textAftComp = response.substring(145,response.length);            
                  node.innerHTML="<p>"+compTitle+"<span id="+dotSpan+">...</span><span id="+moreSpan+" style=display:none>"+textAftComp+"</span></p><button class=btn-link onclick=getFullText("+dotSpan+","+moreSpan+","+btnReadMore+") id="+btnReadMore+">Read more</button>";          
                  node.setAttribute('data-actual-response',compTitle);                              
              }
              else
              {
                  node.innerHTML="<p>"+response+"</p>";
                  node.setAttribute('data-actual-response',response);                               
              }
        }



        
        
  }
  function setResponseJSON(response) 
  {    
        var node = document.getElementById("jsonResponse");
       // node.innerHTML = JSON.stringify(response, null, 2);
  }
  function formCardResponseForSubMenu(response) 
  {    
    console.log("formCardResponseForSubMenu res : ",response);
    var lenOfSubMenu=response.result.fulfillment.messages[1].replies.length;
    
    
    
        var replies=response.result.fulfillment.messages[1].replies,index = 0,html="";
        //var lenOfSubMenu=22,replies=response[1].replies,index = 0,html="";
        for(i=0;i<lenOfSubMenu;i++)
        {
          html += "<input type=\"button\" id=btnSuggestion"+index+" value=\"" + replies[index] + "\"   class=\"btn\" onclick=\"getButtonValue('"+replies[index]+"')\"/>";                    
        }

        node.innerHTML="<p>Below response</p>"+html;
        node.setAttribute('data-actual-response',html);         
  }

  function getId(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);
  
    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return 'error';
    }
  }


  
    
const async = require("async");
const dbHelper = require('../../lib/dbHelper');
const logger = require('../../lib/loggers');

/**
 * Function to get Event State Information
 * @param {userID} userID
 * @param {intentID} intentID
 */
const getEventInformation = (userID, intentId) => new Promise((resolve, reject) => {
    dbHelper.getEventInformationStoredProcedure(userID, intentId).then(recordset => {
        if (recordset.recordsets.length > 0) {
            let successObj = {
                code: 200,
                data: recordset.recordsets[0]
            };
            resolve(successObj);
        }else{
          resolve({
            code: 404,
            message: `Record Not Found`,
          });
        }
    }).catch(err =>{
        reject({
            code: 500,
            message: `Internal Server Error`,
        });
    });
  })
  
  /**
   * Function to get intent list for user
   */
  const getIntentList = (type, UserID) => new Promise((resolve, reject) => {
    dbHelper.getIntentListAndLanguageStoredProcedure(type,UserID).then(intentRecordset => {
      if(intentRecordset.recordsets.length > 0) {
        let successObj = {
          code: 200,
          message: `List Exists`,
          intentList: intentRecordset.recordsets
        };
        resolve(successObj);
      }else{
        resolve({
          code: 404,
          message: `Record Not Found`,
        });
      }
    }).catch(err => {
      logger.error('Error in getIntentList ', err);
      reject({
        code: 500,
        message: `Internal Server Error`,
      });
    });
  });

   /**
   * Function to get intent QA for user
   */
  const getIntentQA = (languages) => new Promise((resolve, reject) => {
    let intentQA = [];
    async.forEach(languages, (language, callback) => {
      dbHelper.getIntentQAStoredProcedure(language.LanguageID).then(intentRecordset => {
        if(intentRecordset.recordsets[0].length > 0) {
          intentQA = intentQA.concat(intentRecordset.recordsets[0]);
        }
        callback();
      }).catch(err => {
        logger.error('Error in fetching intent QA  for  database ', err);
        callback();
      });
      
    }, err => {
        if (err) {
            logger.error('Error in fetching intent QA  from database ', err);
            reject({
                code: 500,
                message: `Error in fetching intent QA from database`,
            });
        }
        else {  
          let successObj = {
            code: 200,
            message: `List Exists`,
            intentList: intentQA
          };
          resolve(successObj);
        }
    });
  });
  
  /**
   * Function to update event infornation
   * @param { userId } userId
   * @param { eventInfo} eventInfo
   */
  const updateEventInfo = (userId,eventInfo) => new Promise((resolve, reject) => {
    eventInfo.state = (eventInfo.state== 'State')?"0":eventInfo.state;
    dbHelper.SetEventInformationStoredProcedure(userId,eventInfo).then(updatedIntentRecord => {
      if(updatedIntentRecord.rowsAffected.length > 0) {
        let successObj = {
          code: 200,
          message: `Updated Intent Info`,
        };
        resolve(successObj);
      }else{
        resolve({
          code: 404,
          message: `Record Not Found`,
        });
      }
    }).catch(err => {
      reject({
        code: 500,
        message: `Internal Server Error`,
      });
    });
  });


  /**
   * Function to get event dashbaord infornation
   * @param { userId } userId
   */
  const getEventDashbaordInfo = (userId) => new Promise((resolve, reject) => {
    dbHelper.getEventDashbaordInfoStoredProcedure(userId).then(Record => {
      if(Record.recordset.length > 0) {
        let dashbaordData = formateSliceData(Record.recordset);
        dbHelper.getCriticalResponseTimerStoredProcedure().then(date => {
          let successObj = {
            code: 200,
            message: `Event Dashboard Info`,
            data: dashbaordData,
            date : date.recordsets[0][0]
          };
          resolve(successObj);
        }).catch(error =>{
          let successObj = {
            code: 200,
            message: `Event Dashboard Info`,
            data: dashbaordData
          };
          resolve(successObj);
        });
      }else{
        resolve({
          code: 404,
          message: `Record Not Found`,
        });
      }
    }).catch(err => {
      reject({
        code: 500,
        message: `Internal Server Error`,
      });
    });
  });

  const formateSliceData = (sliceData) => {
    let sliceArray = [];
    sliceData.forEach((element) =>  {
      Object.keys(element).forEach(key =>{
        if(key.indexOf('SLICE') > -1){
          sliceArray.push(element[key]);
          delete element[key];
        }
      });
      element.sliceArray = sliceArray;
    });
   return sliceData;
  }

  /**
   * Function to get last known reset
   * @param { userId } userId
   * @param { type } type
   */
  const getLastKnownReset = (userId,type) => new Promise((resolve, reject) => {
    dbHelper.getLastKnownResetStoredProcedure(userId,type).then(Record => {
      if(Record.recordsets.length > 0) {
        let successObj = {
          code: 200,
          message: `Last Known Reset`,
          data: Record.recordset[0]
        };
        resolve(successObj);
      }else{
        resolve({
          code: 404,
          message: `Record Not Found`,
          data: 0
        });
      }
    }).catch(err => {
      reject({
        code: 500,
        message: `Internal Server Error`,
      });
    });
  });

  /**
   * Function to get message response
   * @param { userId } userId
   */
  const getMessageResponse = (userId , query) => new Promise((resolve, reject) => {
    dbHelper.getMessageResponseStoredProcedure(userId , query).then(Record => {
      if(Record.recordset.length > 0) {
        let successObj = {
          code: 200,
          message: `Message Response Found`,
          data: Record.recordset
        };
        resolve(successObj);
      }else{
        resolve({
          code: 404,
          message: `Record Not Found`,
        });
      }
    }).catch(err => {
      reject({
        code: 500,
        message: `Internal Server Error`,
      });
    });
  });

  module.exports = {
      getEventInformation,
      getIntentList,
      getIntentQA,
      updateEventInfo,
      getEventDashbaordInfo,
      getLastKnownReset,
      getMessageResponse
  }
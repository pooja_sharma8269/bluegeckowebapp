const dbHelper = require('../../lib/dbHelper');
const async = require("async");
const request = require('request');
const cheerio = require('cheerio');
let xmlOutPutData = ``;
const logger = require('../../lib/loggers');

const updateURLResponse = (urlValues) => new Promise((resolve, reject) => {
    async.forEach(urlValues, (data, callback) => {
        getFullData(data).then((xmlData) => {
            xmlOutPutData += xmlData;
            callback();
        }).catch((xmlData) => {
            xmlOutPutData += xmlData;
            callback();
        });
    }, err => {
        if (err) {
            logger.error('Error in fetching URLs  from database ', err);
            reject({
                code: 500,
                message: `Error in fetching URLs from database`,
            });
        }
        else {
            let xmlFinaldata = `<?xml version="1.0"?>\n <GotQuestionsTranslations> \n` + xmlOutPutData.concat(`</GotQuestionsTranslations>`).replace(/'/g, "`");
            dbHelper.updateURLQuestionAnswerStoredProcedure(xmlFinaldata).then((result) => {
                xmlOutPutData = xmlFinaldata = ``;
                let successObj = {
                    code: 200,
                    message: `WebScraping Done`,
                    returnValue: result.returnValue
                };
                resolve(successObj);
            }).catch((error) => {
                logger.error('Error in updating scraped data ', error);
                reject({
                    code: 500,
                    message: `Error in updating scraped data`,
                });
            });
        }
    });
});

const getFullData = (result) => new Promise((resolve, reject) => {
    const DOCUMENTURL = result.DOCUMENTURL;
    const Id = result.GQLIBRARYID;
    try {
        var extension = DOCUMENTURL.split('.').pop();
        if (extension == "xml") {
            var currentDate = getDate();
            let xmlData = inValidURL(Id, currentDate);
            reject(xmlData);
        } else {
            request(DOCUMENTURL, (error, Response, html) => {
                if (!error && Response.statusCode == 200) {
                    const $ = cheerio.load(html);
                    var question, answer, flag = 0, shortDiscription = '', videoURL = '', ResourceURL = '';

                    $('span').each((i, el) => {
                        if ($(el).attr("itemprop") == "articleBody") {
                            if ($(el).text().indexOf('Question:') != -1 && $(el).text().indexOf('Answer:') != -1) {
                                flag = 1;
                                const item = $(el).text().split('Answer:');
                                var title = item[0].split('Question: ');
                                var titleval = title[1].split('"');
                                var quetionlen = titleval.length;

                                if (quetionlen == 1) {
                                    question = titleval[0].replace(/&/g, 'and').replace(/[…,]/g, '').trimLeft();
                                } else question = titleval[1].replace(/&/g, '').replace(/[…,]/g, '').trimLeft();

                                if ($(el).text().indexOf('Related Topics:') != -1) {
                                    if ($(el).find('.hidden')) {
                                        var tempAnswer = item[1].split("Related Topics:");
                                        var webLanguageData = $(el).find('.hidden').text();
                                        answer = tempAnswer[0].replace(webLanguageData, '').replace(/&/g, 'and').replace(/[…,]/g, '').trim();
                                    } else {
                                        var tempAnswer = item[1].split("Related Topics:");
                                        answer = tempAnswer[0].replace(/&/g, 'and').replace(/[…,]/g, '').trim();
                                    }
                                } else {
                                    if ($(el).find('.hidden')) {
                                        var webLanguageData = $(el).find('.hidden').text();
                                        answer = item[1].replace(webLanguageData, '').replace(/&/g, 'and').replace(/[…,]/g, '').trim();
                                    } else {
                                        answer = item[1].replace(/&/g, 'and').replace(/[…,]/g, '').trim();
                                    }
                                }
                            } else {
                                var currentDate = getDate();
                                let xmlData = inValidURL(Id, currentDate);
                                reject(xmlData);
                            }
                        }
                    })
                    // .replace(/[!@#$’%^\n'&\/*()—,."-:{}–;|“”<>…]/g, '') @@@@all special character regex

                    //Feched additional resorces if url contain attr type itemprop
                    if (flag == 1) {
                        let shortDiscSplitMoreinsights, shortDiscSplitRecommendedResource, shortTitle;
                        if ($('.contents').find('span').nextAll().text().indexOf('Recommended Resource:') != -1 || $('.contents').find('span').nextAll().text().indexOf('Recommended Resources:') != -1) {
                            var shortDiscFindSpan = $('.contents').find('span').nextAll().text();
                            if ($('.contents').find('span').nextAll().text().indexOf('Recommended Resource:') != -1) {
                                shortDiscSplitRecommendedResource = shortDiscFindSpan.split("Recommended Resource:");
                            } else if ($('.contents').find('span').nextAll().text().indexOf('Recommended Resources:') != -1) {
                                shortDiscSplitRecommendedResource = shortDiscFindSpan.split("Recommended Resources:");
                            }
                            var shoertdiscSplitRelatedTopics = shortDiscSplitRecommendedResource[1].split("Related Topics:");
                            if (shoertdiscSplitRelatedTopics[0].indexOf("More insights")) {
                                shortDiscSplitMoreinsights = shoertdiscSplitRelatedTopics[0].split("More insights");
                            }
                            if (shortDiscSplitMoreinsights) {
                                shortDiscription = shortDiscSplitMoreinsights[0].replace(/&/g, 'and').replace(/[…,]/g, '').trimLeft();
                            } else {
                                shortDiscription = shoertdiscSplitRelatedTopics[0].replace(/&/g, 'and').replace(/[…,]/g, '').trimLeft();
                            }

                            var resourceURLtemp = $('.contents').find('span').nextAll().first().next().attr('href');
                            if (resourceURLtemp) {
                                ResourceURL = resourceURLtemp.replace(/&/g, 'and').trimLeft()
                            }
                        } else if ($('.content').find('span').nextAll().text().indexOf('Recommended Resource:') != -1 || $('.content').find('span').nextAll().text().indexOf('Recommended Resources:') != -1) {
                            var shortDiscFindSpan = $('.content').find('span').nextAll().text();
                            if ($('.content').find('span').nextAll().text().indexOf('Recommended Resource:') != -1) {
                                shortDiscSplitRecommendedResource = shortDiscFindSpan.split("Recommended Resource:");
                            } else if ($('.content').find('span').nextAll().text().indexOf('Recommended Resources:') != -1) {
                                shortDiscSplitRecommendedResource = shortDiscFindSpan.split("Recommended Resources:");
                            }
                            var shoertdiscSplitRelatedTopics = shortDiscSplitRecommendedResource[1].split("Related Topics:");
                            if (shoertdiscSplitRelatedTopics[0].indexOf("More insights")) {
                                shortDiscSplitMoreinsights = shoertdiscSplitRelatedTopics[0].split("More insights");
                            }
                            if (shortDiscSplitMoreinsights) {
                                $('.hidden').each((i, el) => {
                                    shortTitle = $(el).text();
                                })
                                if (shortTitle) {
                                    shortDiscription = shortDiscSplitMoreinsights[0].replace(shortTitle, '').replace(/&/g, 'and').replace(/[…,]/g, '').trimLeft();
                                } else {
                                    shortDiscription = shortDiscSplitMoreinsights[0].replace(/&/g, 'and').replace(/[…,]/g, '').trimLeft();
                                }
                            } else {
                                $('.hidden').each((i, el) => {
                                    shortTitle = $(el).text();
                                })
                                if (shortTitle) {
                                    shortDiscription = shoertdiscSplitRelatedTopics[0].replace(shortTitle, '').replace(/&/g, 'and').replace(/[…,]/g, '').trimLeft();
                                } else {
                                    shortDiscription = shoertdiscSplitRelatedTopics[0].replace(/&/g, 'and').replace(/[…,]/g, '').trimLeft();
                                }
                            }

                            resourceURLtemp = $('.content').find('span').nextAll().first().next().attr('href');
                            if (resourceURLtemp) {
                                ResourceURL = resourceURLtemp.replace(/&/g, 'and').trimLeft()
                            } else {
                                $('strong').each((i, el) => {
                                    if ($(el).text() == "Recommended Resource: " || $(el).text() == "Recommended Resources: ") {
                                        resourceURLtemp = $(el).nextAll().first().attr('href');
                                    }
                                });
                                if (resourceURLtemp) {
                                    ResourceURL = resourceURLtemp.replace(/&/g, 'and').trimLeft()
                                }
                            }
                        }
                        else {
                            shortDiscription = '';
                            ResourceURL = '';
                        }

                        $('.youtube-player').each((i, el) => {
                            if ($(el).attr("data-id")) {
                                const videoValue = $(el).attr("data-id");
                                videoURL = "https://www.youtube.com/embed/".concat(videoValue).trimLeft();
                            } else {
                                videoURL = null;
                            }
                        });
                        var currentDate = getDate();
                        let xmlData = addXMLData(Id, question, answer, shortDiscription, currentDate, videoURL, ResourceURL);
                        resolve(xmlData);
                    } else {
                        var currentDate = getDate();
                        let xmlData = inValidURL(Id, currentDate);
                        reject(xmlData);
                    }
                } else {
                    var currentDate = getDate();
                    let xmlData = inValidURL(Id, currentDate);
                    reject(xmlData);
                }
            });
        }
    } catch (error) {
        var currentDate = getDate();
        let xmlData = inValidURL(Id, currentDate);
        reject(xmlData);
    }
});

const getDate = () => {
    var currentDate = new Date();
    var dd = String(currentDate.getDate()).padStart(2, '0');
    var mm = String(currentDate.getMonth() + 1).padStart(2, '0');
    var yyyy = currentDate.getFullYear();
    var time = currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds() + ":" + currentDate.getMilliseconds();
    currentDate = yyyy + '-' + mm + '-' + dd + ' ' + time;
    return currentDate
}

const addXMLData = (Id, question, answer, shortDiscription, currentDate, videoURL, ResourceURL) => {
    var output = `
    <GotQuestions>
        <GQLibraryID>${Id}</GQLibraryID>
        <LanguageID>38</LanguageID>
        <Question>${question}</Question>
        <Answer>${answer}</Answer>
        <AddResorces>${shortDiscription}</AddResorces>
        <ResourceURL>${ResourceURL}</ResourceURL>
        <FollowUpVideoURI>${videoURL}</FollowUpVideoURI>
        <IsCompatible>1</IsCompatible>
        <IsScraped>1</IsScraped>
        <IsValid>1</IsValid>
        <IsOverride>1</IsOverride>
        <DateModified>${currentDate}</DateModified>
    </GotQuestions>`
    return output;
}


const inValidURL = (Id, currentDate) => {
    var output = `
         <GotQuestions>
            <GQLibraryID>${Id}</GQLibraryID>
            <LanguageID>38</LanguageID>
            <Question></Question>
            <Answer></Answer>
            <AddResorces></AddResorces>
            <ResourceURL></ResourceURL>
            <FollowUpVideoURI></FollowUpVideoURI>
            <IsCompatible>0</IsCompatible>
            <IsScraped>1</IsScraped>
            <IsValid>0</IsValid>
            <IsOverride>1</IsOverride >
            <DateModified>${currentDate}</DateModified>
         </GotQuestions>`
    return output;
}

const getCompleteWebScrapedResponse = () => new Promise((resolve, reject) => {
    dbHelper.getURLStoredProcedure().then((result) => {
        let successObj = {
            code: 200,
            message: `GotQuestions URL complete data fetched`,
            completeData: result.recordsets[3]
        };
        resolve(successObj);
    }).catch((error) => {
        logger.error('Error in fetching all scraped data ', error);
        reject({
            code: 500,
            message: `Error in fetching all scraped data`,
        });
    });
});

const updateTranslatedResponse = (data) => new Promise((resolve, reject) => {
    var currentDate = getDate();
    var updatedTransaltedData = `<?xml version="1.0"?>
    <GotQuestionsTranslations>
        <GotQuestions>
            <GQLibraryID>${data.gqID}</GQLibraryID>
            <LanguageID>${data.LanguageID}</LanguageID>
            <Question>${data.question}</Question>
            <Answer>${data.answer}</Answer>
            <AddResorces>${data.shortDiscription}</AddResorces>
            <ResourceURL>${data.ResourceURL}</ResourceURL>
            <FollowUpVideoURI>${data.videoURL}</FollowUpVideoURI>
            <IsCompatible>1</IsCompatible>
            <IsValid>1</IsValid>
            <IsScraped>1</IsScraped>
            <IsOverride>1</IsOverride>
            <DateModified>${currentDate}</DateModified>
        </GotQuestions>
    </GotQuestionsTranslations>`
    dbHelper.updateURLQuestionAnswerStoredProcedure(updatedTransaltedData).then((result) => {
        let successObj = {
            code: 200,
            message: `Translation Updation Done`,
        };
        resolve(successObj);
    }).catch((error) => {
        logger.error('Error in updating translated data', error);
        reject({
            code: 500,
            message: `Error in updating translated data`,
        });
        reject(error);
    });
});

module.exports = {
    updateURLResponse,
    updateTranslatedResponse,
    getCompleteWebScrapedResponse,
}
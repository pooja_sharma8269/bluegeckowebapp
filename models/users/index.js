/* eslint-disable no-undef */
require('dotenv').config({ path: '.env' });
const async = require("async");
const dbHelper = require('../../lib/dbHelper');
const logger = require('../../lib/loggers');
var utils = require('../../lib/utils');

/**
 * Function to get user list
 */
const getUserList = (emailID) => new Promise((resolve, reject) => {
  dbHelper.getUserListStoredProcedure(emailID,1).then(recordset => {
    //encrpt email
    if(recordset.recordset.length > 0) {
      encryptEmailAddress(recordset.recordset).then( userListObject =>{
         let successObj = {
          code: 200,
          message: `List Exists.`,
          userList: userListObject.userList
        };
        resolve(successObj);
      }).catch(error => {
        let successObj = {
          code: 404,
          message: `Records Not Found`
        };
        resolve(successObj);
      });
    }else{
      let successObj = {
        code: 404,
        message: `Records Not Found`
      };
      resolve(successObj);
    }
  }).catch(err => {
    logger.error('Error in getUserList', err);
    reject({
      code: 500,
      message: `Internal Server Error`,
    });
  });
});

/**
 * Function to get user data by email
 * @param {user} user
 */
const getUserByEmail = (user) => new Promise((resolve, reject) => {
  decryptEmailAddress(user).then(decryptEmailAddress => {
    dbHelper.getUserListStoredProcedure(decryptEmailAddress.decryptEmailAddress,0).then(UserRecordset => {
      if(UserRecordset.recordsets.length > 0 && UserRecordset.returnValue !== -1) {
        let successObj = {
          code: 200,
          message: `User Exists.`,
          userData: UserRecordset.recordsets[0]
        };
        resolve(successObj);
      }else{
        resolve({
          code: 404,
          message: `Record Not Found`,
        });
      }
    }).catch(err => {
      logger.error('Error in getUserByEmail', err);
      reject({
        code: 500,
        message: `Internal Server Error`,
      });
    });
  }).catch(error =>{
    logger.error('Error in getUserByEmail', error);
    reject({
      code: 500,
      message: `Internal Server Error`,
    });
  });
});

/**
 * Function to get user data by email
 * @param {userEmail} userEmail
 */
const getDialogUserIdByEmail = (userEmail) => new Promise((resolve, reject) => {
  dbHelper.getUserListStoredProcedure(userEmail,0).then(UserRecordset => {        
    if(UserRecordset.recordsets.length > 0 && UserRecordset.returnValue != '-1') {
      let successObj = {
        code: 200,
        message: `User Exists.`,
        userData: UserRecordset.recordsets[0][0].dialogUserId
      };
      resolve(successObj);
    }else{
      resolve({
        code: 404,
        message: `Record Not Found`,
      });
    }
  }).catch(err => {
    logger.error('Error in getDialogUserIdByEmail', err);
    reject({
      code: 500,
      message: `Internal Server Error`,
    });
  });
});


/**
 * Function to update active state of user
 * @param {userEmail} userEmail
 */
const updateUserActiveState = (query) => new Promise((resolve, reject) => {
  dbHelper.updateActiveStateStoredProcedure(query).then(UserRecordset => {
    if(UserRecordset.returnValue == 1) {
        if(query.type == 2){
          let successObj = {
            code: 200,
            message: `User with email "`+ query.EmailAddress + `" has been deleted.`,
          };
          resolve(successObj);
        }else if(query.type == 0){
          let successObj = {
            code: 200,
            message: `User with email "`+ query.EmailAddress + `" deactviated.`,
          };
          resolve(successObj);
        }else if(query.type == 1) {
          let successObj = {
            code: 200,
            message: `User with email "`+ query.EmailAddress + `" activated.`,
          };
          resolve(successObj);
        }
    }else if(UserRecordset.returnValue == -1){
      reject({
        code: 404,
        message: `Something went wrong.`,
      });
    }
  }).catch(err => {
    logger.error('Error in updateUserActiveState', err);
    reject({
      code: 500,
      message: `Internal Server Error`,
    });
  });
});

/**
 * Function to get user role list
 * @param {userID} userID
 */
const getUserDropdownList = (userID) => new Promise((resolve, reject) => {
  dbHelper.getUserDropdownListListStoredProcedure(userID).then(UserRecordset => {        
    if(UserRecordset.recordsets.length > 0) {
      let successObj = {
        code: 200,
        message: `User Role Exists.`,
        userRoleList: UserRecordset.recordsets[0]
      };
      resolve(successObj);
    }else{
      resolve({
        code: 404,
        message: `Record Not Found`,
      });
    }
  }).catch(err => {
    logger.error('Error in getDialogUserIdByEmail', err);
    reject({
      code: 500,
      message: `Internal Server Error`,
    });
  });
});


/**
 * Function to encrypt emailAddress
 */
const encryptEmailAddress = (userList) => new Promise((resolve, reject) => {
  dbHelper.getEncryptionDecrytionStoredProcedure().then(encryptionKey => {
    async.forEach(userList, (user, callback) => {
      utils.getEncryptEmailAddress(user.EmailAddress,encryptionKey.recordset[0].SALTGUID).then((encryptEmailAddress) => {
          user.EncryptedEmailAddress = encryptEmailAddress.EncryptEmailAddress;
          callback();
      }).catch((error) => {
          logger.error('Error in email address encryption for user with email:', user.EmailAddress);
          callback();
      });
    }, err => {
        if (err) {
            logger.error('Error in email address encryption', err);
            reject({
                code: 500,
                message: `Error in email address encryption`,
            });
        }
        else {
          resolve({
            userList
          });
        }  
    });
  }).catch(error=>{
    logger.error('Error in fetching encryption key', err);
    reject({
        code: 500,
        message: `Error in fetching encryption key`,
    });
  });
});

/**
 * Function to decrypt emailAddress
 */
const decryptEmailAddress = (user) => new Promise((resolve, reject) => {
  if( user.user == 'wordpressuser') {
    resolve ({
      decryptEmailAddress : user.email
    });
  }else{
    dbHelper.getEncryptionDecrytionStoredProcedure().then(encryptionKey => {
      utils.getDecryptEmailAddress(user.email,encryptionKey.recordset[0].SALTGUID).then((decryptEmailAddress) => {
        resolve ({
          decryptEmailAddress :decryptEmailAddress.DecryptEmailAddress
        });
      }).catch((error) => {
        logger.error('Error in email address decrytion email:');
        reject({
          message : 'Error in Decryption'
        });
      });
    }).catch(error =>{
      logger.error('Error in fetching decryption key', err);
      reject({
        message : 'Error in fetching decryption key'
      });
    });
  }
});


module.exports = {
    getUserList,
    getUserByEmail,
    getDialogUserIdByEmail,
    updateUserActiveState,
    getUserDropdownList
}
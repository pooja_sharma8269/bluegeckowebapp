/* eslint-disable no-undef */
const dbHelper = require('../../lib/dbHelper');
const logger = require('../../lib/loggers');

/**
 * This function helps to Intent Analytics
 * @param {userID} userID
 */
const getIntentAnalytics = (userID, lastDate, currentDate,type) => new Promise((resolve, reject) => {
  dbHelper.getIntentAnalyticsStoredProcedure(userID, lastDate, currentDate).then(recordset => {
    if(type == 1){
      if (recordset.recordsets.length > 0) {
        let successObj = {
          code: 200,
          data: recordset.recordsets,
        };
        resolve(successObj);
      } else {
        resolve({
          code: 404,
          message: `Record Not Found`,
        });
      }
    }else{
      dbHelper.getGeneratedIntentRanking(lastDate, currentDate).then((result) => {
        if (recordset.recordsets.length > 0) {
          let successObj = {
            code: 200,
            data: recordset.recordsets,
            rankingData: result.recordset
          };
          resolve(successObj);
        } else {
          resolve({
            code: 404,
            message: `Record Not Found`,
          });
        }
      })
    }
}).catch(err => {
    reject({
      code: 500,
      message: `Internal Server Error`,
    });
  });
})

/**
 * This function helps to get Intent Transaction History
 * @param {userEmail} userEmail
 * @param {intentID} intentID
 */
const getIntentTxHistory = (userEmail, intentID, lastDate, currentDate) => new Promise((resolve, reject) => {
  dbHelper.getIntentTxHistoryStoredProcedure(userEmail, intentID, lastDate, currentDate).then(recordset => {
    if (recordset.recordsets.length > 0) {
      let successObj = {
        code: 200,
        intentList: recordset.recordsets
      };
      resolve(successObj);
    } else {
      resolve({
        code: 404,
        message: `Record Not Found`,
      });
    }
  }).catch(err => {
    reject({
      code: 500,
      message: `Internal Server Error`,
    });
  });
})

module.exports = {
  getIntentAnalytics,
  getIntentTxHistory
}
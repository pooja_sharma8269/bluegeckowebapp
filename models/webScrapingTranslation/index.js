const dbHelper = require('../../lib/dbHelper');
var async = require("async");
const translatte = require('../translationCognitiveService');
let translatedXmlOutPutData = ``;
const logger = require('../../lib/loggers');
var finalQuestionResponce = '', finalAnswerResponce = '' ,finalAdditionalResponce = '', finalResourseUrl = '';

const translateCompleteWebScrapedResponse = (scrapedData, languageDetails) => new Promise((resolve, reject) => {
    try {
        async.forEach(scrapedData, (data, callback) => {
            translateData(data, languageDetails).then((translatedXmlData) => {
                translatedXmlOutPutData += translatedXmlData;
                callback();
            }).catch((err) => {
                callback();
            });
        }, err => {
            if (err) {
                logger.error('Error in data translation', err);
                reject({
                    code: 500,
                    message: `Error in data translation`,
                });
            }
            else {
                let translatedXmlDataata = (`<?xml version="1.0"?>\n<GotQuestionsTranslations>\n` + translatedXmlOutPutData.concat(`</GotQuestionsTranslations>`)).trimLeft();
               dbHelper.updateURLQuestionAnswerStoredProcedure(translatedXmlDataata).then((result) => {
                    translatedXmlOutPutData = translatedXmlDataata = ``;
                    let successObj = {
                        code: 200,
                        message: `Translation done for web scraped data`,
                        returnValue: result.returnValue,
                    };
                    resolve(successObj);
                }).catch((error) => {
                    logger.error('Error in data translation', error);
                    reject({
                        code: 500,
                        message: `Error in data translation`,
                    });
                });
            }
        });
    } catch (error) {
        logger.error('Error in data translation', error);
        reject({
            code: 500,
            message: `Error in data translation`,
        });
    }
});

const translateData = (resultData, languageDetails) => new Promise((resolve, reject) => {
    translatte.translate(resultData.QUESTION, languageDetails.LanguageTag)
        .then(questionResponse => {
            if (resultData.ANSWER.length < 4990) {
                translatte.translate(resultData.ANSWER, languageDetails.LanguageTag)
                    .then(answerResponse => {
                        translatte.translate(resultData.ADDITIONALRESOURCES, languageDetails.LanguageTag)
                            .then(urlResponse => {
                                var currentDate = getDate();
                                let translatedXmlData = addtranslateXML(resultData.GQLIBRARYID, questionResponse, answerResponse, urlResponse, currentDate, resultData.FOLLOWUPVIDEOURI, resultData.RESOURCEURL, languageDetails.LanguageID);
                                resolve(translatedXmlData);
                            }).catch(err => {
                                logger.error('Error in data translation', err);
                                reject({
                                    code: 500,
                                    message: `Error in data translation`,
                                });
                            })
                    }).catch(err => {
                        logger.error('Error in data translation', err);
                reject({
                    code: 500,
                    message: `Error in data translation`,
                });
                    })
            } else {
                translateAnswerChunk(resultData.ANSWER).then(arrayAnswer => {
                    if (arrayAnswer.chunkStatus) {
                        multipleTranslator(arrayAnswer.chunk, languageDetails.LanguageTag).then(lastTranslatedAnser => {
                            translatte.translate(resultData.ADDITIONALRESOURCES, languageDetails.LanguageTag)
                                .then(urlResponse => {
                                    var currentDate = getDate();
                                    let translatedXmlData = addtranslateXML(resultData.GQLIBRARYID, questionResponse, lastTranslatedAnser, urlResponse, currentDate, resultData.FOLLOWUPVIDEOURI, resultData.RESOURCEURL, languageDetails.LanguageID);
                                    resolve(translatedXmlData);
                                }).catch(err => {
                                    logger.error('Error in data translation', err);
                                    reject({
                                        code: 500,
                                        message: `Error in data translation`,
                                    });
                                })
                        }).catch(err => {
                            translatte.translate(resultData.ADDITIONALRESOURCES, languageDetails.LanguageTag)
                                .then(urlResponse => {
                                    var currentDate = getDate();
                                    let translatedXmlData = addtranslateXML(resultData.GQLIBRARYID, questionResponse, "Error in answer traslation", urlResponse, currentDate, resultData.FOLLOWUPVIDEOURI, resultData.RESOURCEURL, languageDetails.LanguageID);
                                    resolve(translatedXmlData);
                                }).catch(err => {
                                    logger.error('Error in answer traslation', err);
                                    reject({
                                        code: 500,
                                        message: `Error in answer traslation`,
                                    });
                                })
                        });
                    } else {
                        translatte.translate(resultData.ADDITIONALRESOURCES, languageDetails.LanguageTag)
                            .then(urlResponse => {
                                var currentDate = getDate();
                                let translatedXmlData = addtranslateXML(resultData.GQLIBRARYID, questionResponse, arrayAnswer.chunk, urlResponse, currentDate, resultData.FOLLOWUPVIDEOURI, resultData.RESOURCEURL, languageDetails.LanguageID);
                                resolve(translatedXmlData);
                            }).catch(err => {
                                logger.error('Error in data translation', err);
                                reject({
                                    code: 500,
                                    message: `Error in data translation`,
                                });
                            })
                    }
                }).catch(err => {
                    translatte.translate(resultData.ADDITIONALRESOURCES, languageDetails.LanguageTag)
                        .then(urlResponse => {
                            var currentDate = getDate();
                            let translatedXmlData = addtranslateXML(resultData.GQLIBRARYID, questionResponse, "Error in answer traslation", urlResponse, currentDate, resultData.FOLLOWUPVIDEOURI, resultData.RESOURCEURL, languageDetails.LanguageID);
                            resolve(translatedXmlData);
                        }).catch(err => {
                            logger.error('Error in answer traslation', err);
                                    reject({
                                        code: 500,
                                        message: `Error in answer traslation`,
                                    });
                        })
                })
            }
        }).catch(err => {
            logger.error('Error in data translation', err);
            reject({
            code: 500,
            message: `Error in data translation`,
            });
        });
});

const translateAnswerChunk = (ANSWER) => new Promise((resolve, reject) => {
    try {
        var anslenght = [];
        for (var i = 0; i < ANSWER.length; i++) {
            if (ANSWER.length < 3500) {
                anslenght.push(ANSWER);
                resolve({
                    chunkStatus: true,
                    chunk: anslenght
                });
                break;
            } else {
                var answer = ANSWER.substring(0, 3500);
                var lastDot = answer.lastIndexOf('.') + 1;
                if (lastDot) {
                    var finaldata = ANSWER.substring(0, lastDot);
                    anslenght.push(finaldata);
                    ANSWER = ANSWER.substring(lastDot);
                } else {
                    var answer = ANSWER.substring(0, 3500);
                    var lastDot = answer.lastIndexOf('\n') + 1;
                    if (lastDot) {
                        var finaldata = ANSWER.substring(0, lastDot);
                        anslenght.push(finaldata);
                        ANSWER = ANSWER.substring(lastDot);
                    } else {
                        resolve({
                            chunkStatus: false,
                            chunk: "Error in Translation. Please do it manually"
                        });
                    }
                }
            }
        }
    } catch (error) {
        logger.error('Error in data translation', err);
        reject({
        code: 500,
        message: `Error in data translation`,
        });
    }
});

const getDate = () => {
    var currentDate = new Date();
    var dd = String(currentDate.getDate()).padStart(2, '0');
    var mm = String(currentDate.getMonth() + 1).padStart(2, '0');
    var yyyy = currentDate.getFullYear();
    var time = currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds() + ":" + currentDate.getMilliseconds();
    currentDate = yyyy + '-' + mm + '-' + dd + ' ' + time;
    return currentDate
}

const addtranslateXML = (Id, question, answer, shortDiscription, currentDate, videoURL, ResourceURL, LanguageID) => {
    finalQuestionResponce = question.replace(/'/g, "`").replace(/&/g, 'and').replace(/[,…]/g, '').trimLeft();
    finalAnswerResponce = answer.replace(/'/g, "`").replace(/&/g, 'and').replace(/[,…]/g, '').trimLeft();
    finalAdditionalResponce = shortDiscription.replace(/'/g, "`").replace(/&/g, 'and').replace(/[,…]/g, '').trimLeft();
   finalResourseUrl = ResourceURL.replace(/'/g, "`").replace(/&/g, 'and').replace(/[,…]/g, '').trimLeft();
    var output = `
    <GotQuestions>
        <GQLibraryID>${Id}</GQLibraryID>
        <LanguageID>${LanguageID}</LanguageID>
        <Question>${finalQuestionResponce}</Question>
        <Answer>${finalAnswerResponce}</Answer>
        <AddResorces>${finalAdditionalResponce}</AddResorces>
        <ResourceURL>${finalResourseUrl}</ResourceURL>
        <FollowUpVideoURI>${videoURL}</FollowUpVideoURI>
        <IsCompatible>1</IsCompatible>
        <IsValid>1</IsValid>
        <IsScraped>1</IsScraped>
        <IsOverride>0</IsOverride>
        <DateModified>${currentDate}</DateModified>
    </GotQuestions>`
    return output;
}

let asyncForEach = async (array, callback) => { for (let index = 0; index < array.length; index++) { await callback(array[index], index, array) } }


const multipleTranslator = async (arrayAnswer, LanguageTag) => new Promise((resolve, reject) => {
    try {
        let lastTranslatedAnser = '';
        let index = 0;
        asyncForEach(arrayAnswer, async (data) => {
            await translatte.translate(data, LanguageTag).then((answerResponse) => {
                lastTranslatedAnser += answerResponse;
                index++;
                if (index == (arrayAnswer.length)) {
                    resolve(lastTranslatedAnser)
                }
            }).catch((answerResponse) => {
                lastTranslatedAnser += answerResponse;
                index++;
                if (index == (arrayAnswer.length)) {
                    resolve(lastTranslatedAnser)
                }
            });
        });
    } catch (error) {
        logger.error('Error in answer translation', err);
            reject({
            code: 500,
            message: `Error in answer translation`,
        });
    }
});

module.exports = {
    translateCompleteWebScrapedResponse
}
/* eslint-disable no-undef */
require('dotenv').config({
  path: '.env'
});

const dbHelper = require('../../lib/dbHelper');
const logger = require('../../lib/loggers');

const _ = require('underscore');

/**
 * Function to get chatbot locations
 */
const getChatbotLocations = () => new Promise((resolve, reject) => {
  dbHelper.getChatbotInformationStoredProcedure().then(recordset => {
    if (recordset.recordsets[0].length > 0) {
      var locations = _.map(recordset.recordsets[0], function (element) {
        if (element.isActive) {
          return ({
            ChatbotLocationID: element.ChatbotLocationID,
            ChatbotLocation: element.ChatbotLocation
          });
        }
      });
      let successObj = {
        code: 200,
        message: `Locations Exists.`,
        locationList: locations
      };
      resolve(successObj);
    } else {
      resolve({
        code: 404,
        message: `Not records found`,
        locationList: []
      });
    }
  }).catch(err => {
    logger.error('Error in getChatbotLocations ', err);
    reject({
      code: 500,
      message: `Internal Server Error`,
    });
  });
});

/**
 * Function to get visibility mode
 */
const getVisibilityMode = () => new Promise((resolve, reject) => {
  dbHelper.getChatbotInformationStoredProcedure().then(recordset => {
    if (recordset.recordsets[1].length > 0) {
      var visibilityList = _.map(recordset.recordsets[1], function (element) {
        if (element.isActive) {
          return ({
            VisibilityModeID: element.VisibilityModeID,
            VisibilityMode: element.VisibilityMode
          });
        }
      });
      let successObj = {
        code: 200,
        message: `Visibility Mode Exists.`,
        visibilityMode: visibilityList
      };
      resolve(successObj);
    } else {
      resolve({
        code: 404,
        message: `Not records found`,
        visibilityMode: []
      });
    }
  }).catch(err => {
    logger.error('Error in getVisibilityMode ', err);
    reject({
      code: 500,
      message: `Internal Server Error`,
    });
  });
});

/**
 * Function to get chatbot ID 
 */
const getChatBotId = (wordpressdomain) => new Promise((resolve, reject) => {
  dbHelper.getChatbotInformationStoredProcedure(wordpressdomain).then(recordset => {   
    if (recordset.recordsets[0][0] !== '') {
      let chatBotID = recordset.recordsets[0][0].ChatbotLocationID;
      let visibiltyModeID = recordset.recordsets[0][0].VisibilityModeID;
      let ApiKey = recordset.recordsets[0][0].ApiKey;
      let successObj = {
        code: 200,
        message: `Success`,
        chatBotID: chatBotID,
        visibiltyModeID: visibiltyModeID,
        ApiKey: ApiKey       
      };
      resolve(successObj);
    } else {
      resolve({
        code: 404,
        message: `Not found`,
      });
    }
  }).catch(err => {
    logger.error('Error in getChatBotId ', err);
    reject({
      code: 500,
      message: `Internal Server Error`,
    });
  });
});

/**
 * Function to get chatbot locations and visibility mode
 */
const getChatbotLocationAndVisibilityMode = () => new Promise((resolve, reject) => {
  Promise.all([getChatbotLocations(),getVisibilityMode()]).then((chatbotInfoResponse) => {
    let successObj = {
      code: 200,
      message: `Success`,
      locationList: chatbotInfoResponse[0].locationList,
      visibilityMode: chatbotInfoResponse[1].visibilityMode
    };
    resolve(successObj);
  }).catch((error) => {
      reject(error)
    });
  }).catch(err => {
    logger.error('Error in getChatbotLocationAndVisibilityMode ', err);
    reject({
      code: 500,
      message: `Internal Server Error`,
    });
  });

module.exports = {
  getChatbotLocations,
  getVisibilityMode,
  getChatBotId,
  getChatbotLocationAndVisibilityMode
}
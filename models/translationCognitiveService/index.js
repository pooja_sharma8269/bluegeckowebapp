const request = require('request');
const uuidv4 = require('uuid/v4');
const logger = require('../../lib/loggers');


const translate = (text, language) => new Promise((resolve, reject) => {
    try {
        let options = {
        method: 'POST',
        baseUrl: process.env.TRANSLATE_ENDPOINT,
        url: 'translate',
        qs: {
            'api-version': '3.0',
            'to': [ language ]
        },
        headers: {
            'Ocp-Apim-Subscription-Key': process.env.TRANSLATE_SECRET_KEY,
            'Content-type': 'application/json',
            'X-ClientTraceId': uuidv4().toString()
        },
        body: [{
                'text': text
        }],
        json: true,
    };

    request(options, function(err, res, body){
        if(err){
            console.log(err)
            logger.error('Error in cognitive service translation api ', err);
            reject({
                code: 500,
                message: `Error in cognitive service translation api`,
            });
        }
        else{
            try {
                let result = body[0].translations[0].text;
                resolve(result);
            } catch (error) {
                console.log(error)
                logger.error('Error in cognitive service translation api ', error);
                reject({
                    code: 500,
                    message: `Error in cognitive service translation api`,
                });
            }
        }
    });    
    } catch (error) {
        console.log(error)
        reject(error) 
    }
    
});

module.exports = {
    translate
}
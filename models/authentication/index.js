/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
require('dotenv').config({ path: '.env' });
const dbHelper = require('../../lib/dbHelper');
const logger = require('../../lib/loggers');

/**
 * Function for user login
 * @param {*email,password,role} body 
 */
const login = (body) => new Promise((resolve, reject) => {
    dbHelper.validateLogonStoredProcedure(body.email, body.password).then(result => {
        if (result.output.ret_isUserFound === false) {
            resolve({
                code: 202,
                message: `User not found with email ${body.email}`,
            });
        } else if (result.output.ret_isUserFound === true && result.output.ret_isUserDisabled === true) {
            resolve({
                code: 201,
                message: `Your account is disabled.`,
            });
        }else if (result.output.ret_isUserVerified === false){
            resolve({
                code: 201,
                message: `Your account is not activated yet. Please activate your account to login.`,
            });
        }else if(result.output.ret_isUserFound === true && result.output.ret_isPasswordValidated === false && result.output.ret_isUserDisabled === false) {
            resolve({
                code: 201,
                message: `Invalid Password, only 3 attempts allowed.`,
            });
        } else {
            dbHelper.query(`SELECT * FROM  [user].[userProfile] WHERE UserID = '${result.output.ret_UserId}' AND UserRoleID = '${result.output.ret_UserRoleID}'`).then(recordset => {
                if (recordset.recordsets[0][0] && recordset.recordsets[0][0].EmailAddress && recordset.recordsets[0][0].EmailAddress !== '') {
                    resolve({
                        code: 200,
                        message: `User Found with email ${body.email} and password.`,
                        data: recordset.recordsets[0][0]
                    });
                }else{
                    resolve({
                        code: 404,
                        message: `User not found with email ${body.email} and password.`
                    });
                }
            }).catch((error) => {
                logger.error("Error in User Login ", error);
                reject({
                    code: 500,
                    message: `Internal Server Error`,
                });
            });
        }
    }).catch((err) => {
        logger.error("Error in User Login ", err);
        reject({
            code: 500,
            message: `Internal Server Error`,
        });
    })
});

/**
 * Function to check email already exists in database
 * @param {*} email 
 */
const checkEmailExists = (email) => new Promise((resolve, reject) => {
    dbHelper.query(`SELECT * FROM  [user].[userProfile] WHERE EmailAddress = '${email}'`).then(recordset => {
        if (recordset.recordsets[0][0] && recordset.recordsets[0][0].EmailAddress && recordset.recordsets[0][0].EmailAddress !== '') {
            let successObj = {
                code: 200,
                message: `${email} Exists.`,
                data: recordset.recordsets[0][0]
            };
            resolve(successObj);
        } else {
            let successObj = {
                code: 404,
                message: `${email} not Exists.`
            };
            resolve(successObj);
        }
    }).catch((err) => {
        logger.error('Error in checkEmailExists ', err);
        reject({
            code: 500,
            message: `Internal Server Error`,
        });
    })
});

/**
 * Function used to get forget password token
 * @param {*} token 
 * @param {*} tokenName 
 */
const getUserByToken = (token, tokenName) => new Promise((resolve, reject) => {
    dbHelper.query(`SELECT * FROM  [user].[userProfile] WHERE ${tokenName} = '${token}'`).then(result => {
        if (result.recordsets[0] == '') {
            resolve('null');
        }
        resolve(result.recordsets[0]);
    });
}).catch(err => {
    reject({
        code: 500,
        message: `Internal Server Error`,
    });
});

/**
 * get user details by passing register token
 * @param {*} token 
 * @param {*} isVerified 
 */
const getUserByRegisterToken = (token,isVerified) => new Promise((resolve,reject) => {
    dbHelper.getUserTokenStoredProcedure(token,isVerified).then((result) =>{
        resolve(result);
    }).catch((err) => {
        reject(err);
    })
}).catch((error) => {
    reject(error);
});

/**
 * get user by email
 * @param {*} email 
 */
const getUserByEmail = (email) => new Promise((resolve, reject) => {
    dbHelper.query(`SELECT * FROM  [user].[userProfile] WHERE EmailAddress = '${email}'`).then(recordset => {
        if (recordset.recordsets[0][0] && recordset.recordsets[0][0].EmailAddress && recordset.recordsets[0][0].EmailAddress !== '') {
            let successObj = {
                code: 200,
                message: `User fetched successfully.`,
                data: recordset.recordsets[0][0]
            };
            resolve(successObj);
    }else{
        let successObj = {
            code: 404,
            message: `${email} not Exists.`
        };
        resolve(successObj);
    }
});
}).catch(err => {
    reject({
        code: 500,
        message: `Internal Server Error`,
    });
});

/**
 * Function to reset user password
 * @param {*user entered password} password1 
 * @param {*password in DB } password2 
 */
const setUserPassword = (data) => new Promise((resolve, reject) => {
    dbHelper.forgotPasswordStoredProcedure(data).then(result => {
        if (result.returnValue == 1) {
            let successObj = {
                code: 200,
                message: `Password Updated Successfully.`,
                data: {}
            };
            resolve(successObj);
        }else{
            let errObject = {
                code: 400,
                message: 'Password update failed.',
                data: {}
            }
            resolve(errObject);
        }
    });
}).catch(err => {
    reject({
        code: 500,
        message: `Internal Server Error`,
    });
});

module.exports = {
    login,
    checkEmailExists,
    setUserPassword,
    getUserByToken,
    getUserByEmail,
    getUserByRegisterToken
}
const dbHelper = require('../../lib/dbHelper');
const async = require("async");
const request = require('request');
const cheerio = require('cheerio');
let siteMapXmlOutPutData = ``;
const logger = require('../../lib/loggers');
var siteMapScrapeddata = [], gotQuestionResponse = [];
var siteMapXmlFinaldata = '';

let updateSiteMapScrapedResponse = (type) => new Promise((resolve, reject) => {
    dbHelper.updateSitemapResponseStoredProcedure(type, siteMapXmlFinaldata).then((result) => {
        gotQuestionResponse = result.recordsets[0];
        scrapSiteMapScrapedResponse(type, gotQuestionResponse).then((result) => {
            resolve(result)
        });
    }).catch((error) => {
        logger.error('Error in fetching all URLs from database ', error);
        reject({
            code: 500,
            message: `Error in fetching all URLs from database`,
        });
    });
});

let scrapSiteMapScrapedResponse = (type, gotQuestionResponse) => new Promise((resolve, reject) => {
    request('https://www.gotquestions.org/sitemap.xml', (error, Response, html) => {
        if (!error && Response.statusCode == 200) {
            const $ = cheerio.load(html);
            var urlName = '', Inputs_Intent = '', changefreq = '', lastmod = '', priority = '';
            $('url').each((i, el) => {
                let tempUrlName = $(el).find('loc').text();
                var urlEXT = tempUrlName.split('.').pop();
                if (urlEXT == 'html' || urlEXT == 'xml') {
                    urlName = tempUrlName;
                    Inputs_Intent = getIntent_Name(urlName);
                    changefreq = $(el).find('changefreq').text();
                    lastmod = $(el).find('lastmod').text();
                    var tempPriority = $(el).find('priority').text();
                    priority = parseFloat(tempPriority).toFixed(2);
                    siteMapScrapeddata.push({ urlName: urlName, Inputs_Intent: Inputs_Intent, changefreq: changefreq, lastmod: lastmod, priority: priority });
                }
            })
     siteMapScrapeddata = siteMapScrapeddata.reduce((unique, o) => {
                if(!unique.some(obj => obj.Inputs_Intent === o.Inputs_Intent)) {
                  unique.push(o);
                }
        return unique;
    },[]);
 removeDuplicateIntents(type, siteMapScrapeddata, gotQuestionResponse).then((result) => {
                resolve(result)
            })
        }
    });
});

let getIntent_Name = (urlName) => {
    var arr = urlName.split('/');
    var val = arr[arr.length - 1].split('.')[0];
    var result = val.replace(/-/g, '_');
    return (result.toUpperCase());
}

let removeDuplicateIntents = (type, siteMapScrapeddata, gotQuestionResponse) => new Promise((resolve, reject) => {
    for (var i = 0, responselength = gotQuestionResponse.length; i < responselength; i++) {
        for (var j = 0, scraplength = siteMapScrapeddata.length; j < scraplength; j++) {
            if (gotQuestionResponse[i].Inputs_Intent === (siteMapScrapeddata[j].Inputs_Intent).trim()) {
                siteMapScrapeddata.splice(j, 1);
                scraplength = siteMapScrapeddata.length;
            }
        }
    }

    checkDuplicateIntents(type, siteMapScrapeddata, gotQuestionResponse).then((result) => {
        resolve(result)
    })
})

let checkDuplicateIntents = (type, siteMapScrapeddata, gotQuestionResponse) => new Promise((resolve, reject) => {
    var siteMapScrapeddataTemp = [];
    siteMapScrapeddata.forEach(function (arrayItem) {
        siteMapScrapeddataTemp.push(arrayItem.Inputs_Intent);
    });

    var gotQuestionScrapeddataTemp = []
    gotQuestionResponse.forEach(function (arrayItem) {
        gotQuestionScrapeddataTemp.push(arrayItem.Inputs_Intent);
    });
    siteMapScrapeddataTemp = siteMapScrapeddataTemp.filter(n => !gotQuestionScrapeddataTemp.includes(n));
  if (siteMapScrapeddataTemp.length > 0) {
       formSiteMapScrapedResponse(type, siteMapScrapeddata).then((result) => {
            resolve(result);
        })
    } else {
        let successObj = {
            code: 200,
            message: `Sitemap scraping done`,
        };
        resolve(successObj);
    }
})

let formSiteMapScrapedResponse = (type, siteMapScrapeddata) => new Promise((resolve, reject) => {
    async.forEach(siteMapScrapeddata, (data, callback) => {
        getSiteMapXMLData(data, type).then((siteMapData) => {
            siteMapXmlOutPutData += siteMapData;
            callback();
        }).catch((siteMapData) => {
            siteMapXmlOutPutData += siteMapData;
            callback();
        });
    }, err => {
        if (err) {
            logger.error('Error in fetching URLs from intent managment screen ', err);
            reject({
                code: 500,
                message: `Error in fetching URLs from intent managment screen`,
            });
        }
        else {
            siteMapXmlFinaldata = `<?xml version="1.0"?>\n <GotQuestionsSiteMapXML> \n` + siteMapXmlOutPutData.concat(`</GotQuestionsSiteMapXML>`).replace(/'/g, "`");
            dbHelper.updateSitemapResponseStoredProcedure(type = 0, siteMapXmlFinaldata).then((result) => {
                siteMapXmlOutPutData = siteMapXmlFinaldata = ``;
                let successObj = {
                    code: 200,
                    message: `Sitemap scraping done`,
                    returnValue: result.returnValue
                };
                resolve(successObj);
            }).catch((error) => {
                logger.error('Error in updating sitemap scraped data ', error);
                reject({
                    code: 500,
                    message: `Error in updating sitemap scraped data`,
                });
            });
        }
    });

});

let getSiteMapXMLData = (result) => new Promise((resolve, reject) => {
    if (result) {
        var currentDate = getDate();
        let siteMapData = addSiteMapXMLData(result.urlName, result.Inputs_Intent, result.changefreq, result.lastmod, result.priority, currentDate);
        resolve(siteMapData);
    } else {
        reject(result);
    }
});

let getDate = () => {
    var currentDate = new Date();
    var dd = String(currentDate.getDate()).padStart(2, '0');
    var mm = String(currentDate.getMonth() + 1).padStart(2, '0');
    var yyyy = currentDate.getFullYear();
    var time = currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds() + ":" + currentDate.getMilliseconds();
    currentDate = yyyy + '-' + mm + '-' + dd + ' ' + time;
    return currentDate
}

let addSiteMapXMLData = (UrlName, Inputs_Intent, ChangeFreq, Lastmod, Priority, currentDate) => {
    var output = `
    <GotQuestions>
    <Inputs_Intent>${Inputs_Intent}</Inputs_Intent>
    <DocumentURL>${UrlName}</DocumentURL>
    <DateLastChanged>${Lastmod}</DateLastChanged>
    <ChangeFrequency>${ChangeFreq}</ChangeFrequency>
    <Priority>${Priority}</Priority>
    <IsActive>1</IsActive>
    <DateCreated>${currentDate}</DateCreated>
    <DateModified>${currentDate}</DateModified>
    <IsCompatible>1</IsCompatible>
    <IsTranslated>0</IsTranslated>
    <IsUpdateNeeded>0</IsUpdateNeeded>
      <FollowUpVideoURI></FollowUpVideoURI>
    <IsScraped>0</IsScraped>
  </GotQuestions>`
    return output;
}

module.exports = {
    updateSiteMapScrapedResponse
}

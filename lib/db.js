/* eslint-disable no-console */
/* eslint-disable no-undef */
const sql = require('mssql')
const config = require('../config');

const pool = new sql.ConnectionPool(config.dbConfig)
  .connect()
  .then(pool => {
    console.log('Connected to MSSQL')
    return pool
  })
  .catch(err => console.log('Database Connection Failed! Bad Config: ', err))

module.exports = {
  sql, pool
}
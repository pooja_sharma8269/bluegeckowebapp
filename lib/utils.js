/* eslint-disable no-undef */
const CryptoJS = require("crypto-js");
var dbHelper = require('../lib/dbHelper');

/* GET demographic details. */
const getDemographicDetails = (zipCode) => {
    return dbHelper.get(`api.vGetDemographicMaster`, `ZipCode = ${zipCode}`)
        .then((result) => {
            if (result.recordset.length == 0) {
                throw new Error;
            }
            return (result.recordset);
        })
        .catch((error) => {
            throw new Error('Invalid postal code.');
        })
}

/**
 * Function to encrypt emailAddress
 */
const getEncryptEmailAddress = (emailAddress,key) => new Promise((resolve, reject) => {
    try {
        var ciphertext = CryptoJS.AES.encrypt(emailAddress, key);
        let EncryptEmailAddress = ciphertext.toString().replace(/\//g,"-");
        resolve({
            EncryptEmailAddress : EncryptEmailAddress
        });
    } catch (error) {
        reject({
            error: 'Error in Encryption'
        });
    }   
});

/**
 * Function to decrypt emailAddress
 */
const getDecryptEmailAddress = (emailAddress,key) => new Promise((resolve, reject) => {
    try {
        let encryptEailAddress = emailAddress.replace(/-/g,"/");
        var bytes  = CryptoJS.AES.decrypt(encryptEailAddress.toString(),key);
        var plaintext = bytes.toString(CryptoJS.enc.Utf8);
        resolve({
            DecryptEmailAddress : plaintext
        });
    } catch (error) {
        reject({
            error: 'Error in Decryption'
        });
    }   
});

module.exports = {
    getDemographicDetails,
    getEncryptEmailAddress,
    getDecryptEmailAddress
}

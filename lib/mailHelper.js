/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const sgMail = require('@sendgrid/mail');
const expressHandlebars = require('express-handlebars');
const loggers = require('./loggers');
const config = require('../config');
require('dotenv').config();



const hbs = expressHandlebars.create({
  partialsDir: path.join(__dirname, '../views/emailTemplates/'),
  extname: '.hbs',
});

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

/**
 * Fumction for sending mails
 * @param {*} mailOptions
 * @param {*} callback
 */
const sendMail = (mailOptions, callback) => {
  sgMail.send(mailOptions, (error, result) => {
    callback(error, result);
  });
};

/**
 * This function creates mail options
 * @param {array} receipientList
 * @param {string} subject
 * @param {html} html
 */
const createMailOptions = (recipientList, subject, html, attachment) => {
  const mailOptions = {
    from: config.sourceEmail,
    to: recipientList,
    subject,
    html,
  };
  if (attachment) {
    const data = fs.readFileSync(attachment[0].path);
    const base64data = Buffer.from(data).toString('base64');
    mailOptions.attachments = [{
      filename: path.basename(attachment[0].path),
      content: base64data,
      type: 'application/pdf',
    }];
  }
  return mailOptions;
};

const testMail = (recipientList, subject, data) => new Promise((resolve, reject) => {
  const mailOptions = createMailOptions(recipientList, subject, html);
  sendMail(mailOptions, (err, result) => {
    if (err) reject(err);
    else resolve(result);
  });
});

/**
 * creates email template to send in mails
 * @param {*} templateName
 * @param {*} data
 */
const createTemplate = (templateName, data) => new Promise((resolve, reject) => {
  hbs.render(`views/emailTemplates/${templateName}.hbs`, data)
    .then((res) => {
      resolve(res);
    })
    .catch((err) => {
      reject(err);
    });
});

/**
 * Function to send forgot password mail
 * @param {*} req
 */
const sendForgotPasswordMail = (data,type) => new Promise((resolve, reject) => {
 const user = data.EmailAddress;
  const name = data.FirstName + ' ' + data.LastName;
  const host = `${process.env.PROD_PROTOCOL}://${process.env.PROD_HOST}.${process.env.PROD_PORT ? process.env.PROD_PORT : "azurewebsites.net"}`;
  const token = data.ForgetPasswordToken;
  const url = `${host}/changePassword?token=${token}`;
  if(type == 1){
    createTemplate('forgotPasswordEmail', { data: name, host, user, url})
    .then((html) => {
      const mailOptions = createMailOptions([user], 'FlowCore.io: Forgot Password', html);
      sendMail(mailOptions, (err, result) => {
        if (err) {
          loggers.error('error while sendForgotPasswordMail===> ', err);
          reject(err);
        } else {
          loggers.info('In send Contact Mail ', `${user}`);
          resolve({
            message : 'Forget password mail sent for emailaddress ' + user
          });
        }
      });
    });
  }else if(type == 2){
    createTemplate('activateUserEmail', { name, user, host, url})
      .then((html) => {
        const mailOptions = createMailOptions([user], 'FlowCore.io: New User Registration', html);
        sendMail(mailOptions, (err, result) => {
          if (err) {
            loggers.error('error while activateUserEmail===> ', err);
            reject(err);
          } else {
            loggers.info('In send Contact Mail ', `${user}`);
            resolve({
              message : 'User created with emailaddress ' + user
            });
          }
        });
    });
  }
});


module.exports = {
  testMail,
  sendForgotPasswordMail,
};
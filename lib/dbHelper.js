/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
require('dotenv').config();
const config = require('../config');
const { pool, sql } = require('./db')

/**
 * This SP is used to register/update user.
 * @param {*} data
 */
const getSetUserProfileStoredProcedure = (data) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('PublicScreenTag', sql.VarChar(20), data.PublicScreenTag);
        request.input('UTCTimeZone', sql.VarChar(50), data.UTCTimeZone);
        request.input('FirstName', sql.VarChar(50), data.FirstName);
        request.input('LastName', sql.VarChar(50), data.LastName);
        request.input('EmailAddress', sql.VarChar(200), data.EmailAddress ? data.EmailAddress : data.editEmailAddress);
        request.input('UserPassword', sql.VarChar(200), data.UserPassword ? data.UserPassword : data.editUserPassword);
        request.input('OrganizationName', sql.VarChar(100), data.OrganizationName);
        request.input('AddressLine1', sql.VarChar(100), data.AddressLine1);
        request.input('AddressLine2', sql.VarChar(100), data.AddressLine2);
        request.input('City', sql.VarChar(100), data.City);
        request.input('Region', sql.VarChar(100), data.Region);
        request.input('State', sql.VarChar(100), data.State);
        request.input('Country', sql.VarChar(100), data.Country);
        request.input('PostalCode', sql.VarChar(100), data.PostalCode);
        request.input('UserRoleID', sql.TinyInt, data.createUserRole ? data.createUserRole : data.UserRoleID);
        request.input('ChatbotLocationID', sql.TinyInt, data.ChatbotLocationID);
        request.input('VisibilityModeID', sql.TinyInt, data.VisibilityModeID ? data.VisibilityModeID : 1);
        request.input('APIKey', sql.VarChar(100), data.WordPressAPIKey ? data.WordPressAPIKey : '');
        request.input('DomainName', sql.VarChar(100), data.WordPressDomainName ? data.WordPressDomainName : '');
        request.input('ForgetPasswordToken', sql.VarChar(100), data.ForgetPasswordToken ? data.ForgetPasswordToken : '');
        request.input('isVerified', sql.Bit, data.isVerified == 0 ? data.isVerified : 1);
        request.execute('apiGetSetUserProfile').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch((err) => {
        reject({ message: err });
    })
});

/**
 * This SP is used to get and verify registered user token.
 * @param {*} token
 * @param {*} isVerified
 */
const getUserTokenStoredProcedure = (token, isVerified) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('UserToken', sql.VarChar(200), token);
        request.input('IsVerified', sql.Bit, isVerified);
        request.execute('apiValidateUserToken').then((result) => {
            if (result.recordsets.length > 0) {
                resolve(result.recordsets[0]);
            } else {
                reject('No data found.');
            }
        }).catch((err) => {
            reject(err);
        })
    }).catch(() => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

/**
 * This method is used to execute select queries
 * @param {*} tableName
 * @param {*} condition
 */
const get = (tableName, condition) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.query(`SELECT * FROM ${tableName} ${condition ? 'Where ' + condition : ''}`).then((recordset) => {

            resolve(recordset);
        }).catch((err) => {

            reject(err);
        });
    })
});

/**
 * This methos is used to execute queries
 * @param {*} query
 */
const query = (query) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.query(query).then((recordset) => {

            resolve(recordset);
        }).catch((err) => {

            reject(err);
        });
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

/**
 * This SP is used for user login
 * @param {*} email
 * @param {*} password
 */
const validateLogonStoredProcedure = (email, password) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.output('ret_UserId', sql.Int);
        request.output('ret_isUserFound', sql.Bit);
        request.output('ret_isUserDisabled', sql.Bit);
        request.output('ret_isBadAttemptsExceeded', sql.Bit);
        request.output('ret_isPermissionAccessWindowExceeded', sql.Bit);
        request.output('ret_isPasswordValidated', sql.Bit);
        request.output('ret_UserRoleID', sql.TinyInt);
        request.output('ret_isUserVerified', sql.Bit);
        request.input('EmailAddress', sql.VarChar(200), email);
        request.input('UserPassword', sql.VarChar(200), password);


        request.execute('apiValidateLogon').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

const getEventInformationStoredProcedure = (userID, intentId) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('UserId', sql.Int(200), userID);
        request.input('IntentID', sql.Int(200), intentId);
        request.execute('apiGetEventInformation').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

const getIntentAnalyticsStoredProcedure = (userID, lastDate, currentDate) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('UserId', sql.Int(200), userID);
        request.input('FromDate', sql.Date, lastDate);
        request.input('ToDate', sql.Date, currentDate)
        request.execute('[dbo].[apiGetIntentAnalytics]').then((result) => {
            resolve(result);
        }).catch((err) => {

            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

const getGeneratedIntentRanking = (lastDate, currentDate) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('FromDate', sql.Date, lastDate);
        request.input('ToDate', sql.Date, currentDate)
        request.execute('[dbo].[GenerateIntentRanking]').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

/**
 * This SP is used to reset user password.
 * @param {*} data
 */
const forgotPasswordStoredProcedure = (data) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('EmailAddress', sql.VarChar(200), data.EmailAddress);
        request.input('UserPassword', sql.VarChar(200), data.UserPassword);
        request.input('ForgetPasswordToken', sql.VarChar(200), data.ForgetPasswordToken);
        request.input('isVerified', sql.VarChar(200), data.isVerified);
        request.execute('apiSetForgotPassword').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

const getChatbotInformationStoredProcedure = (WordPressDomainName) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('DomainName', sql.VarChar(200), WordPressDomainName ? WordPressDomainName : '');
        request.execute('[apiGetChatBotInformation]').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

const getUserListStoredProcedure = (userEmail, userListType) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('UserEmailAddress', sql.VarChar(200), userEmail);
        request.input('ToggleList', sql.Int(1), userListType);
        request.execute('[apiGetUserList]').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

const SetEventInformationStoredProcedure = (userId, eventInfo) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('UserId', sql.VarChar(200), userId);
        request.input('DefaultResponse', sql.VarChar(300), eventInfo.message);
        request.input('ValidFromDate', sql.VarChar(300), eventInfo.fromValue);
        request.input('ValidToDate', sql.VarChar(300), eventInfo.toValue);
        request.input('IntentID', sql.Int(200), eventInfo.intent);
        request.input('EventState', sql.Int(200), eventInfo.state);
        request.input('UserLocale', sql.VarChar(300), eventInfo.language);
        request.execute('[apiSetEventInformation]').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

const getIntentTxHistoryStoredProcedure = (userEmail, intentID, lastDate, currentDate) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('UserEmailAddress', sql.VarChar(200), userEmail);
        request.input('InputIntentID', sql.Int(200), intentID);
        request.input('FromDate', sql.Date, lastDate);
        request.input('ToDate', sql.Date, currentDate)
        request.execute('[apiGetIntentTransaction]').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

const getEventDashbaordInfoStoredProcedure = (userID) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('UserID', sql.Int(200), userID);
        request.execute('[APIGETSPARKLINE]').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

const getLastKnownResetStoredProcedure = (userID, type) => new Promise((resolve, reject) => {
    let ScreenTag = parseInt(type);
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('UserID', sql.Int(200), userID);
        request.input('IntentID', sql.Int(200), null);
        request.input('ScreenTagID', sql.Int(200), ScreenTag);
        request.execute('[dbo].[apiResetLastKnownDateTime]').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

const getIntentListAndLanguageStoredProcedure = (type, UserID) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('ScreenTag', sql.Int(200), type);
        request.input('UserID', sql.Int(200), UserID);
        request.execute('[dbo].[apiGetIntentLibrary]').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});


const getIntentQAStoredProcedure = (languageID) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('LanguageID', sql.Int(200), languageID);
        request.execute('[apiGetIntentQA]').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

const getMessageResponseStoredProcedure = (userid, query) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('UserID', sql.Int(200), userid);
        request.input('InputIntent', sql.VarChar(200), query.IntentID);
        request.input('EventState', sql.VarChar(200), query.state);
        request.input('UserLocale', sql.VarChar(200), query.LanguageTag);
        request.execute('[apiGetLanguageResponse]').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

const updateActiveStateStoredProcedure = (query) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('EmailAddress', sql.VarChar(200), query.EmailAddress);
        request.input('IsActiveDeactive', sql.Int(200), query.type);
        request.execute('[apiSetActiveDeActiveUser]').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});


const getCriticalResponseTimerStoredProcedure = () => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.execute('[dbo].[apiGetCriticalResponseTimer]').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

const updateURLQuestionAnswerStoredProcedure = (xmFinaldata) => new Promise((resolve, reject) => {  //parameters for fetching the urls
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('GotQuestionsXML', sql.Xml(xmFinaldata.length), xmFinaldata);
        request.execute('[apiSetGotQuestionsKB]').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});


const updateSitemapResponseStoredProcedure = (type, siteMapXmlFinaldata) => new Promise((resolve, reject) => {  //parameters for fetching the urls
    pool.then((connection) => {
        const request = new sql.Request(connection);
        if (type == 1) {
            request.input('GetIntentList', sql.Bit, 1);
            request.input('GotQuestionsXML', sql.Xml, siteMapXmlFinaldata);
        } else {
            request.input('GetIntentList', sql.Bit, 0);
            request.input('GotQuestionsXML', sql.Xml(siteMapXmlFinaldata.length), siteMapXmlFinaldata);
        }
        request.execute('[apiGetSetSiteMapXML]').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

const getUserDropdownListListStoredProcedure = (userID) => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.input('UserID', sql.Int(200), userID);
        request.execute('[apiGetUserRoleList]').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

const getEncryptionDecrytionStoredProcedure = () => new Promise((resolve, reject) => {
    pool.then((connection) => {
        const request = new sql.Request(connection);
        request.execute('[apiGetEncryptDecryptKey]').then((result) => {
            resolve(result);
        }).catch((err) => {
            reject(err);
        })
    }).catch(err => {
        reject({
            message: `Unable to connect on ${config.dbConfig}`
        })
    })
});

module.exports = {
    get,
    query,
    validateLogonStoredProcedure,
    getSetUserProfileStoredProcedure,
    forgotPasswordStoredProcedure,
    getEventInformationStoredProcedure,
    getIntentAnalyticsStoredProcedure,
    getChatbotInformationStoredProcedure,
    getUserListStoredProcedure,
    SetEventInformationStoredProcedure,
    getIntentTxHistoryStoredProcedure,
    getEventDashbaordInfoStoredProcedure,
    getLastKnownResetStoredProcedure,
    getIntentListAndLanguageStoredProcedure,
    getIntentQAStoredProcedure,
    getMessageResponseStoredProcedure,
    updateActiveStateStoredProcedure,
    getCriticalResponseTimerStoredProcedure,
    updateURLQuestionAnswerStoredProcedure,
    getUserTokenStoredProcedure,
    getUserDropdownListListStoredProcedure,
    getEncryptionDecrytionStoredProcedure,
    updateSitemapResponseStoredProcedure,
    getGeneratedIntentRanking
}
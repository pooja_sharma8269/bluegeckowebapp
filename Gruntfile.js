module.exports = function (grunt) {
    grunt.initConfig({
        eslint: {
            target: ["*.js",
                "./modules/**/*.js"],
            options: {
                configFile: "./eslint.json",
                // rulePaths: ['./rules']
            }
        },
    });



    // Load the plugin that provides the "jshint" task.
    grunt.loadNpmTasks("grunt-eslint");
    // Default task(s).
    grunt.registerTask("default", ["eslint"]);
};
/* eslint-disable no-undef */
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const hbs = require('express-handlebars');
const redis = require('redis');
const cors = require('cors');
const helmet = require('helmet');
const session = require('express-session');
//const redisClient = redis.createClient(process.env.REDIS_URL);
const RedisStore = require('connect-redis')(session);
const logger = require('./lib/loggers');
const helperpath = require('./views/helpers/index');
require('dotenv').config({ path: '.env' });
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users/index');
const chatbotRouter = require('./routes/chatbot/index');
const demographicRouter = require('./routes/demographic/index');
const authentication = require('./routes/authentication/index');
var redisClient = redis.createClient(6380, process.env.REDISCACHEHOSTNAME,
   {auth_pass: process.env.REDISCACHEKEY, tls: {servername: process.env.REDISCACHEHOSTNAME}});

logger.init();


var app = express();

app.use(cors());

/**
 * For Security Purpose using Helmet
 */
app.use(helmet());
app.use(helmet.noCache());
app.use(helmet.noSniff());
app.use(helmet.hidePoweredBy());
app.use(helmet.xssFilter());
app.use(helmet.frameguard())

/**
 * view engine setup
 */
app.engine('hbs', hbs({
  extname: 'hbs',
  //defaultLayout: 'main',
  layoutsDir: path.join(__dirname, '/views/layouts/'),
  partialsDir: path.join(__dirname, '/views/partials/'),
  helpers: helperpath,
}));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

/**
 * Redis Configuration
 */
app.use(session({
  secret: process.env.SECRET,
  resave: false,
  saveUninitialized: false,
  store: new RedisStore({
    client: redisClient,
  }),
}));

app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb', extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use((req, res, next) => {
  res.set('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
  next();
});

app.use('/',authentication);
app.use('/', indexRouter);
app.use('/users', usersRouter);
//app.use('/admin', admin);
app.use('/chatbot', chatbotRouter);
//app.use('/witness',witness);
app.use('/demographics', demographicRouter);

app.get('/*', (req, res) => {
  res.redirect('/');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// eslint-disable-next-line no-undef
module.exports = app;

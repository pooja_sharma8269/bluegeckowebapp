/* eslint-disable no-undef */
const config = {
    dbConfig: {
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        server: process.env.DB_SERVER, // You can use 'localhost\\instance' to connect to named instance
        database: process.env.DB_NAME,
        requestTimeout: 800000,
        connectionTimeout: 800000,
        pool: {
            idleTimeoutMillis: 800000,
            max: 100
        },

     options: {
            encrypt: true ,
            packetSize: 32768
    }
    },
    apiMaxLength: '50mb',
    sourceEmail : process.env.SOURCEEMAIL

};
module.exports=config;
